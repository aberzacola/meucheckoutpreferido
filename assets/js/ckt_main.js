var doSubmit = false;
var ticket = false;
var addPayment = false;
var desconto_cartao = parseInt($("#desconto_cartao").val());
var trigger_validation;

$(document).ready(function () {

    trigger_validation = $("#trigger_validation").val();

    $(".prdct_qty").inputSpinner();

    $("#btn-comprar").on("click", function () {
        $('html, body').animate({
            scrollTop: $("#pay").offset().top
        }, 500);
    });

    var inicio = parseInt($("#escassez").val());
    var contador = $("#etapas .numero");
    if (inicio > 0) {
        var inicio = Number(inicio);
        setInterval(function () {
            $(contador[0]).html(Math.floor(inicio / 3600))
            $(contador[1]).html(Math.floor(inicio % 3600 / 60));
            $(contador[2]).html(Math.floor(inicio % 3600 % 60));

            inicio--;
            if (inicio == 0) {
                window.location = "https://" + $("#shop_url").val();
            }
        }, 1000)
    }

    $("body").on("click", ".btn-decrement", function () {
        var qtd = parseInt($(this).parent().siblings(".prdct_qty").val());
        if (qtd <= 1) {
            return;
        } else {
            qtd--;
        }
        var variant_id = $(this).parents('.prdcts_cart').get(0);
        variant_id = $(variant_id).attr("id");

        jQuery.post('/cart/change.js', {
            quantity: qtd,
            id: variant_id
        }, null, 'json').done(function () {
            refreshCart("movimentaCarrinho");
            guarda_carrinho()
        });
    });

    $("body").on("click", ".btn-increment", function () {
        var qtd = parseInt($(this).parent().siblings(".prdct_qty").val());
        qtd++;
        var variant_id = $(this).parents('.prdcts_cart').get(0);
        variant_id = $(variant_id).attr("id");

        jQuery.post('/cart/change.js', {
            quantity: qtd,
            id: variant_id
        }, null, 'json').done(function (e) {
            refreshCart("movimentaCarrinho", variant_id);
            guarda_carrinho()
        });
    });

    $("body").on("change", ".prdct_qty", function () {
        var qtd = parseInt($(this).val());

        var variant_id = $(this).parents('.prdcts_cart').get(0);
        variant_id = $(variant_id).attr("id");

        jQuery.post('/cart/change.js', {
            quantity: qtd,
            id: variant_id
        }, null, 'json').done(function (e) {
            refreshCart("movimentaCarrinho", variant_id);
            guarda_carrinho()
        });

    });

    $("body").on("click", ".notify-badge", function () {
        var id = parseInt($(this).parent().parent().attr("id"));
        jQuery.post('/cart/change.js', {
            quantity: 0,
            id: id
        }, null, 'json').done(function (e) {
            refreshCart("movimentaCarrinho");
            guarda_carrinho()
        });

    });

    // mascara telefone
    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };
    var spOptions = {
        onKeyPress: function (val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };
    $('#celularComprador').mask(SPMaskBehavior, spOptions);

    // mascara cpf
    $("#documentoComprador").mask("000.000.000-00");

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var camposCartao = $("#nav-home input");
        if (e.target.id == "nav-profile-tab") {
            for (var i = 0; i < camposCartao.length; i++) {
                $(camposCartao[i]).removeAttr("required");
            }
            const paymentMethodElement = document.querySelector('input[name=paymentMethodId]');
            paymentMethodElement.value = 'bolbradesco';
            ticket = true;
            addPaymentPixel();
            desconto_cartao = 0;
            calcularTotal();
        } else {
            desconto_cartao = parseInt($("#desconto_cartao").val());
            calcularTotal();
            for (var i = 0; i < camposCartao.length; i++) {
                $(camposCartao[i]).attr("required", "required");
            }
            ticket = false;
        }
    })

    var options = {
        onComplete: function (cep) {
            cepPreenchido(cep);
            guarda_carrinho();
        }
    };

    $('#cepComprador').mask('00000-000', options);
    $('#expiry').mask('00 / 00');

    var card = new Card({
        form: 'form',
        container: '.card-wrapper',
        formSelectors: {
            numberInput: 'input#cardNumber', // optional — default input[name="number"]
            expiryInput: 'input#expiry', // optional — default input[name="expiry"]
            cvcInput: 'input#securityCode', // optional — default input[name="cvc"]
            nameInput: 'input#cardholderName' // optional - defaults input[name="name"]
        },
        placeholders: {
            name: 'Nome completo'
        },
        messages: {
            validDate: 'expire\ndate',
            monthYear: 'mm/yy'
        }
    })

    $("body").on("click", "input[name=metodoEnvio]", function () {
        $("#entrega").html(numberToReal(parseInt($(this).val()) / 100));
        $("#entrega").attr("data-value", $(this).val())
        calcularTotal()
    });

    $("#aplicar_desconto").on("click", function () {
        $.ajax({
            type: "POST",
            url: "/apps/checkout/check_discount/",
            cache: false,
            dataType: 'json',
            data: {
                cod_desconto: $("#cupomDesconto").val()
            },
            success: function (e) {
                $(".cupom_resposta").removeClass("d-none");
                if (e == false) {
                    $(".cupom_resposta .alert-primary").html("Cumpom inválido");
                    $("#cupom_id").val(-1);
                } else {
                    $(".desconto_cartao_alerta").addClass("d-none");
                    $(".area_cupom").addClass("d-none");
                    $(".cupom").removeClass("d-none");
                    $(".cupom").addClass("list-group-item-success");
                    $("#cupom_val").val(e.valor);
                    $("#cupom_metodo").val(e.modalidade);
                    $("#cupom_id").val(e.id);
                    $("#cupom_tipo").val(e.tipo);
                    $(".cupom_resposta .alert-primary").html("Cumpom aplicado");
                    var texto_cupom = "";
                    if (e.modalidade == "fixo") {
                        texto_cupom += numberToReal(e.valor / 100) + " OFF";
                    } else {
                        texto_cupom += e.valor + "% OFF"
                    }
                    if (e.tipo == "frete") {
                        texto_cupom += " no Frete";
                    } else if (e.tipo == "produtos") {
                        texto_cupom += " nos Produtos";
                    } else {
                        texto_cupom += " na Compra";
                    }
                    $("#cupom").html(texto_cupom);
                }
                calcularTotal();
            }
        });
    });


    if (trigger_validation > 0) {
        var celular = $("#celularComprador");
        var emailComprador = $("#emailComprador");
        var nomeComprador = $("#nomeComprador");
        var cepComprador = $("#cepComprador");
        var items_id = $("#items_id");
        var items_qtd = $("#items_qtd");

        if (celular.val() != "") {
            if ($("#celularComprador").cleanVal().length < 11 || !validPhone($("#celularComprador").val())) {
                $("#celularComprador").addClass('is-invalid');
                $("#celularComprador").removeClass('is-valid');
            } else {
                $("#celularComprador").addClass('is-valid');
                $("#celularComprador").removeClass('is-invalid');
                $(".finalizar-pagamento").attr("disabled", false);
            }
        }
        if (emailComprador.val() != "") {
            if (!validateEmail($("#emailComprador").val())) {
                $("#emailComprador").addClass('is-invalid');
                $("#emailComprador").removeClass('is-valid');
            } else {
                $("#emailComprador").addClass('is-valid');
                $("#emailComprador").removeClass('is-invalid');
                $(".finalizar-pagamento").attr("disabled", false);
            }
        }
        if (nomeComprador.val() != "") {
            nomeComprador.trigger("focusout");
        }

        items_id = items_id.val().split("|");
        items_qtd = items_qtd.val().split("|");
        jQuery.post('/cart/clear.js', function () {
            gerar_carrinho(items_id, items_qtd).then(v => {
                refreshCart("criaCarrinho");
                if (cepComprador.val() != "") {
                    cepPreenchido(cepComprador.val());
                }
            });
        }, 'json');


    } else {
        refreshCart("iniciaCarrinho");
    }
})

async function gerar_carrinho(items_id, items_qtd) {
    for (var i = 0; i < items_id.length; i++) {
        await addInCartFromToken(items_qtd[i], items_id[i]);
    }
}

function addInCartFromToken(items_qtd, items_id) {
    return new Promise(resolve => {
        jQuery.post('/cart/add.js', {
            quantity: items_qtd,
            id: items_id
        }, function () {
            resolve(true);
        }, 'json')
    });
}

async function cepPreenchido(cep) {
    $.get("https://viacep.com.br/ws/" + cep + "/json/", async function (data, status) {
        if (data.erro) {
            $('#cepComprador').addClass("is-invalid");
            $('#cepComprador').removeClass("is-valid");
            return;
        } else {
            $('#cepComprador').addClass("is-valid");
            $('#cepComprador').removeClass("is-invalid");
        }
        $("#enderecoComprador").val(data.logradouro);
        $("#enderecoComprador").addClass("is-valid");
        $("#bairroComprador").val(data.bairro);
        $("#bairroComprador").addClass("is-valid");
        $("#cidadeComprador").val(data.localidade);
        $("#cidadeComprador").addClass("is-valid");
        $("#estadoComprador").val(data.uf);
        $("#estadoComprador").addClass("is-valid");
        $(".enderecoCompleto").css("display", "inline");

        shippingJson = await getShippingRates(cep, data.uf, 'Brazil');
        $(".alertaEndereco").css("display", 'none');
        var metodoEnvio = $(".metodoEnvio");
        metodoEnvio.html('');
        metodoEnvio.css("display", "inline");

        if (typeof shippingJson.error != "undefined") {
            metodoEnvio.append(
                '<div class="form-check">' +
                '<input class="form-check-input" type="radio" name="metodoEnvio" value="0#|#-1#|#-1" required checked>' +
                '<label class="form-check-label" for="metodoEnvio">' +
                'Frete grátis' +
                '</label>' +
                '</div>')
        } else {
            for (i = 0; i < shippingJson.shipping_rates.length; i++) {
                var checked = ""
                if (shippingJson.shipping_rates.length == 1) {
                    checked = "checked"
                }
                metodoEnvio.append(
                    '<div class="form-check">' +
                    '<input class="form-check-input" type="radio" name="metodoEnvio" value="' + parseFloat(shippingJson.shipping_rates[i].price) * 100 + '#|#' + shippingJson.shipping_rates[i].code + '#|#' + shippingJson.shipping_rates[i].presentment_name + '" required ' + checked + '>' +
                    '<label class="form-check-label" for="metodoEnvio">' +
                    shippingJson.shipping_rates[i].presentment_name + ' ( R$' + shippingJson.shipping_rates[i].price + ' )' +
                    '</label>' +
                    '</div>')
            }
        }
        $($("input[name=metodoEnvio]")[0]).trigger("click");
        $(".finalizar-pagamento").attr("disabled", false);
    });
}

function guarda_carrinho() {
    var nome_comprador = $("#nomeComprador").val();
    var email = $("#emailComprador").val();
    var celular = $("#celularComprador").val();
    var cep = $("#cepComprador").val();

    $.get("/cart.js", function (cart) {
        var cart_json = JSON.stringify(cart);
        $.ajax({
            type: "POST",
            url: "/apps/checkout/carrinho",
            cache: false,
            data: {
                nome_comprador: nome_comprador,
                email: email,
                celular: celular,
                cep: cep,
                cart_json: cart_json
            }
        });
    }, "json");
}

function calc_soma() {
    var desconto_id = parseInt($("#cupom_id").val());
    var desconto_val = parseInt($("#cupom_val").val());
    var desconto_metodo = $("#cupom_metodo").val();
    var desconto_tipo = $("#cupom_tipo").val();

    var subtotal = parseInt($("#subtotal").attr("data-value"));
    var entrega = parseInt($("#entrega").attr("data-value"));
    var desconto = parseInt($("#desconto").attr("data-value"));

    if (desconto_id > 0) {
        if (desconto_tipo == "frete") {
            if (desconto_metodo == "fixo") {
                entrega = entrega - parseInt(desconto_val);
                if (entrega < 0) {
                    entrega = 0;
                }
            } else {
                entrega = entrega - (entrega * (desconto_val / 100));
                if (entrega < 0) {
                    entrega = 0;
                }
            }
        } else if (desconto_tipo == "produtos") {
            if (desconto_metodo == "fixo") {
                subtotal = subtotal - parseInt(desconto_val);
                if (subtotal < 0) {
                    subtotal = 0;
                }
            } else {
                subtotal = subtotal - (subtotal * (desconto_val / 100));
                if (subtotal < 0) {
                    subtotal = 0;
                }
            }
        }
    }

    var soma = subtotal + entrega;

    if (desconto_id > 0) {
        if (desconto_tipo == "total") {
            if (desconto_metodo == "fixo") {
                soma = soma - parseInt(desconto_val);
                if (soma < 0) {
                    soma = 0;
                }
            } else {
                soma = soma - (soma * (desconto_val / 100));
                if (soma < 0) {
                    soma = 0;
                }
            }
        }
    }

    if (desconto_cartao > 0 && desconto_id <= 0) {
        desconto = soma * (desconto_cartao / 100)
        soma = soma - desconto;
        $("#desconto").html(numberToReal(desconto / 100));
        $(".desconto_cartao_alerta").removeClass("d-none");
        $($("#desconto").parents("li")[0]).addClass("list-group-item-success");
        $($("#desconto").parents("li")[0]).removeClass("d-none");
    } else {
        $("#desconto").html(numberToReal(0));
        $($("#desconto").parents("li")[0]).addClass("d-none");
        $($("#desconto").parents("li")[0]).removeClass("list-group-item-success");
    }
    return soma;
}

function calcularTotal() {
    var soma = calc_soma();
    calculo_parcelas();
    $("#total").attr("data-value", soma);
    $("#total").html(numberToReal(soma / 100));
    $(".checkoutTotal").html(numberToReal(soma / 100));
}


async function getShippingRates(zip, state, country) {
    const shippingJson = '{"shipping_rates":[{"name":"Super","presentment_name":"Super","code":"Super","price":"1.00","markup":null,"source":"shopify","delivery_date":null,"delivery_range":null,"delivery_days":[],"compare_price":null,"phone_required":null,"currency":"BRL","carrier_identifier":null,"delivery_category":null,"using_merchant_account":null,"carrier_service_id":null,"description":null,"api_client_id":null,"requested_fulfillment_service_id":null,"shipment_options":null,"charge_items":null,"has_restrictions":null,"rating_classification":null},{"name":"Mega","presentment_name":"Mega","code":"Mega","price":"10.00","markup":null,"source":"shopify","delivery_date":null,"delivery_range":null,"delivery_days":[],"compare_price":null,"phone_required":null,"currency":"BRL","carrier_identifier":null,"delivery_category":null,"using_merchant_account":null,"carrier_service_id":null,"description":null,"api_client_id":null,"requested_fulfillment_service_id":null,"shipment_options":null,"charge_items":null,"has_restrictions":null,"rating_classification":null}]}';
    const url = encodeURI(`shipping_address[zip]=${zip}&shipping_address[country]=${country}&shipping_address[province]=${state}`);
    const result = await fetch(`/cart/shipping_rates.json?${url}`, {
        method: "GET",
        headers: {
            "Accept": "application/json"
        }
    });

    return result.json();
    // return JSON.parse(shippingJson);
}

function addPaymentPixel() {
    if (addPayment == false) {
        fbq('track', 'AddPaymentInfo');
        addPayment = true;
    }
}

window.Mercadopago.setPublishableKey($("#mp_public_key").val());

function getBin() {
    const cardnumber = document.getElementById("cardNumber").value;
    return cardnumber.replace(/ /g, '').substring(0, 6);
}

function guessingPaymentMethod(event) {
    var bin = getBin();
    if (event.type == "keyup") {
        if (bin.length >= 6) {
            window.Mercadopago.getPaymentMethod({
                "bin": bin
            }, setPaymentMethodInfo);
        }
    } else {
        setTimeout(function () {
            if (bin.length >= 6) {
                window.Mercadopago.getPaymentMethod({
                    "bin": bin
                }, setPaymentMethodInfo);
            }
        }, 100);
    }
};

function setPaymentMethodInfo(status, response) {
    var form = document.querySelector('#pay');
    if (status == 200) {
        const paymentMethodElement = document.querySelector('input[name=paymentMethodId]');
        if (paymentMethodElement) {
            paymentMethodElement.value = response[0].id;
        } else {
            const input = document.createElement('input');
            input.setattribute('name', 'paymentMethodId');
            input.setAttribute('type', 'hidden');
            input.setAttribute('value', response[0].id);

            form.appendChild(input);
        }
    } else {
        alert(`payment method info error: ${response}`);
    }
};

function validPhone(phone) {
    var regex = new RegExp('^\\([0-9]{2}\\) (9[0-9]{4}-[0-9]{4})$');
    return regex.test(phone);
}


$("#documentoComprador").on("focusout", function () {
    if (validarCPF($("#documentoComprador").cleanVal()) === true) {
        $("#documentoComprador").addClass('is-valid');
        $("#documentoComprador").removeClass('is-invalid');
        $(".finalizar-pagamento").attr("disabled", false);
    } else {
        $("#documentoComprador").addClass('is-invalid');
        $("#documentoComprador").removeClass('is-valid');
    }
});

$("#celularComprador").on("focusout", function () {
    if ($("#celularComprador").cleanVal().length < 11 || !validPhone($("#celularComprador").val())) {
        $("#celularComprador").addClass('is-invalid');
        $("#celularComprador").removeClass('is-valid');
    } else {
        $("#celularComprador").addClass('is-valid');
        $("#celularComprador").removeClass('is-invalid');
        $(".finalizar-pagamento").attr("disabled", false);
        guarda_carrinho();
    }
})

$("#nomeComprador").on("focusout", function () {
    if (!/\w+\s+\w+/.test($("#nomeComprador").val())) {
        $("#nomeComprador").addClass('is-invalid');
        $("#nomeComprador").removeClass('is-valid');
    } else {
        $("#nomeComprador").addClass('is-valid');
        $("#nomeComprador").removeClass('is-invalid');
        $(".finalizar-pagamento").attr("disabled", false);
        guarda_carrinho();
    }
})

$("#emailComprador").on("focusout", function () {
    if (!validateEmail($("#emailComprador").val())) {
        $("#emailComprador").addClass('is-invalid');
        $("#emailComprador").removeClass('is-valid');
    } else {
        $("#emailComprador").addClass('is-valid');
        $("#emailComprador").removeClass('is-invalid');
        $(".finalizar-pagamento").attr("disabled", false);
        guarda_carrinho();
    }
})

$("#numeroEnderecoComprador").on("focusout", function () {
    if ($("#numeroEnderecoComprador").val() == "") {
        $("#numeroEnderecoComprador").addClass('is-invalid');
        $("#numeroEnderecoComprador").removeClass('is-valid');
    } else {
        $("#numeroEnderecoComprador").addClass('is-valid');
        $("#numeroEnderecoComprador").removeClass('is-invalid');
        $(".finalizar-pagamento").attr("disabled", false);
    }
})

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

$(window).keydown(function (event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        return false;
    }
});



$("#pay").on("submit", function (e) {
    e.preventDefault();
    $(".finalizar-pagamento").attr("disabled", true);

    //Validando CPF
    if (validarCPF($("#documentoComprador").cleanVal()) === false) {
        $("#documentoComprador").addClass('is-invalid');
        $('html, body').animate({
            scrollTop: $("#documentoComprador").offset().top
        }, 500);
        return false;
    } else {
        $("#documentoComprador").removeClass('is-invalid');
    }
    //Validando celular
    if ($("#celularComprador").cleanVal().length < 11 || !validPhone($("#celularComprador").val())) {
        $("#celularComprador").addClass('is-invalid');
        $('html, body').animate({
            scrollTop: $("#celularComprador").offset().top
        }, 500);
        return false;
    } else {
        $("#celularComprador").removeClass('is-invalid');
    }

    //validando nome
    if (!/\w+\s+\w+/.test($("#nomeComprador").val())) {
        $("#nomeComprador").addClass('is-invalid');
        alert("Digite seu nome completo");
        $('html, body').animate({
            scrollTop: $("#nomeComprador").offset().top
        }, 500);
        return false;
    } else {
        $("#nomeComprador").removeClass('is-invalid');
    }

    //validando email
    if (!validateEmail($("#emailComprador").val())) {
        $("#emailComprador").addClass('is-invalid');
        alert("Digite seu email corretamente");
        $('html, body').animate({
            scrollTop: $("#emailComprador").offset().top
        }, 500);
        return false;
    } else {
        $("#emailComprador").removeClass('is-invalid');
    }

    //validando cep
    if ($("#cepComprador").val().length < 9) {
        $("#cepComprador").addClass('is-invalid');
        alert("Digite seu cep corretamente");
        $('html, body').animate({
            scrollTop: $("#cepComprador").offset().top
        }, 500);
        return false;
    } else {
        $("#cepComprador").removeClass('is-invalid');
    }


    $("#docNumber").val($("#documentoComprador").cleanVal());

    if (!doSubmit && !ticket) {
        if (jQuery("input[id='cardNumber']").is(':visible')) {
            if (jQuery("#expiry").length) {
                var cardExpiryDate = jQuery("#expiry").val().replace(/ /g, '');
                if (typeof cardExpiryDate != "undefined" && cardExpiryDate != "" && cardExpiryDate != null) {
                    var cardExpiryDateArray = cardExpiryDate.split('/');

                    if (typeof cardExpiryDateArray != "undefined" && cardExpiryDateArray != "" && cardExpiryDateArray != null && cardExpiryDateArray.length == 2) {
                        if (jQuery("#cardExpirationMonth").length) {
                            jQuery("#cardExpirationMonth").attr('value', cardExpiryDateArray[0]);
                            jQuery("#cardExpirationMonth").val(cardExpiryDateArray[0]);
                        }
                        if (jQuery("#cardExpirationYear").length) {
                            jQuery("#cardExpirationYear").attr('value', "20" + cardExpiryDateArray[1]);
                            jQuery("#cardExpirationYear").val("20" + cardExpiryDateArray[1]);
                        }
                    }
                }
            }
            guessingPaymentMethod(e);
            if (!doSubmit) {
                var $form = document.querySelector('#pay');
                Mercadopago.createToken($form, sdkResponseHandler); // The function "sdkResponseHandler" is defined below
                return false;
            }
        }
    } else {
        var form = document.querySelector('#pay');
        doSubmit = true;
        $.get("/cart.js", function (cart) {
            $("#cart").val(JSON.stringify(cart));
            jQuery.post('/cart/clear.js');
            bloqueia("Processando, aguarde...");
            form.submit();
        }, "json");
    }
});

function refreshCart(e, variant_id) {
    variant_id = variant_id || false;
    var ids = [];
    var titles = [];
    $.get("/cart.js", function (cart) {
        if (cart.item_count <= 0) {
            //none
        }
        var carrinho = $(".prdcts_list");
        carrinho.html("");
        var total = 0;
        for (i = 0; i < cart.items.length; i++) {
            ids.push(cart.items[i].product_id);
            titles.push(cart.items[i].title);
            total += cart.items[i].price * cart.items[i].quantity;
            var model = $("#prdcts_model").clone();
            $(model).removeClass("d-none");
            $(model).attr("id", cart.items[i].variant_id);
            $(model).find(".prdct-thumb").find("img").attr("src", cart.items[i].featured_image.url);
            $(model).find(".prdct-price").html(numberToReal((cart.items[i].price * cart.items[i].quantity) / 100));
            $(model).find(".prdct_qty").val(cart.items[i].quantity);
            $(model).find(".prdct_qty_badge").html(cart.items[i].quantity);
            $(model).find(".prdct-name").find("strong").html(cart.items[i].product_title);
            if (cart.items[i].variant_title)
                $(model).find("ul").append("<li>" + cart.items[i].variant_title + "</li>");
            carrinho.append(model);
            if (e == "movimentaCarrinho" && variant_id == cart.items[i].variant_id) {
                ids = []
                ids.push(cart.items[i].product_id);
                fbq('track', 'AddToCart', {
                    value: cart.items[i].price / 100,
                    currency: 'BRL',
                    content_ids: ids,
                    content_type: "product_group",
                    content_name: cart.items[i].product_title,
                    num_items: 1
                });
            }
        }
        $("#subtotal").html(numberToReal(total / 100))
        $("#subtotal").attr("data-value", total);
        calcularTotal();
        if (e == "iniciaCarrinho") {
            fbq('track', 'InitiateCheckout', {
                content_type: 'product_group',
                content_ids: ids,
                value: calc_soma() / 100,
                num_items: cart.item_count,
                currency: 'BRL',
                content_name: titles
            });
        }
        desbloqueia();
    }, "json");
}

$("#cardNumber").on("change", function () {
    calculo_parcelas()
})

function calculo_parcelas() {
    var bin = getBin();
    var soma = calc_soma();
    var selectParcelas = $("#parcelas");
    var alertaParcelas = $(".alertParcelas");
    selectParcelas.html("");
    if (bin.length >= 6) {
        window.Mercadopago.getInstallments({
            "bin": bin,
            "amount": soma / 100
        }, function (status, response) {
            if (status == 200) {
                addPaymentPixel();
                selectParcelas.prop('disabled', false);
                var parcela_cartao_1 = response[0];
                selectParcelas.html("");
                selectParcelas.removeClass("d-none");
                alertaParcelas.addClass("d-none");
                for (var i = 0; i < parcela_cartao_1.payer_costs.length; i++) {
                    if (i == 0) {
                        selectParcelas.append('<option value="' + parcela_cartao_1.payer_costs[i].installments + '" selected>' + parcela_cartao_1.payer_costs[i].recommended_message + '</option>');
                    } else {
                        selectParcelas.append('<option value="' + parcela_cartao_1.payer_costs[i].installments + '">' + parcela_cartao_1.payer_costs[i].recommended_message + '</option>');
                    }
                }
            }
        });
    } else {
        alertaParcelas.removeClass("d-none");
        selectParcelas.addClass("d-none");
        selectParcelas.html("");
        selectParcelas.prop('disabled', true);
    }
}

function sdkResponseHandler(status, response) {
    if (status != 200 && status != 201) {
        alert("Confira os dados do cartão");
        $('html, body').animate({
            scrollTop: $(".card-wrapper").offset().top
        }, 500);
    } else {
        var form = document.querySelector('#pay');
        var card = document.createElement('input');
        card.setAttribute('name', 'token');
        card.setAttribute('type', 'hidden');
        card.setAttribute('value', response.id);
        form.appendChild(card);
        doSubmit = true;
        $.get("/cart.js", function (cart) {
            $("#cart").val(JSON.stringify(cart));
            jQuery.post('/cart/clear.js');
            bloqueia("Processando, aguarde...");
            form.submit();
        }, "json");
    }
};

function numberToReal(numero) {
    var numero = numero.toFixed(2).split('.');
    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
}

function validarCPF(cpf) {
    cpf = cpf.replace(/[^\d]+/g, '');
    if (cpf == '') {
        return false;
    }

    // Elimina CPFs invalidos conhecidos
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999") {
        return false;
    }
    // Valida 1o digito
    add = 0;
    for (i = 0; i < 9; i++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(9))) {
        return false;
    }
    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10))) {
        return false;
    } else {
        return true;
    }
}

function bloqueia(msg, spin = true) {
    $(".outerLoader").removeClass("d-none");
    $(".outerLoader p").html(msg);
}

function desbloqueia() {
    setTimeout(function () {
        $(".outerLoader").addClass("d-none");
    }, 1500);
}