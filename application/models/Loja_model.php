<?php

class Loja_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //cadastrado o nome da cerca no banco
    public function inserir_loja($shopify_nome, $token)
    {
        $data = array(
            'shopify_nome' => $shopify_nome,
            'token' => $token,
        );

        $this->db->insert('lojas', $data);
        return $this->db->insert_id();
    }

    public function update_loja($id_loja, $val)
    {
        $this->db->where("id", $id_loja);
        $this->db->update('lojas', $val);
    }

    public function inserir_ordem($id_loja, $shopify_order_id, $payment_status = 0, $name, $cart_token, $order_status_url)
    {
        $data = array(
            'loja_id' => $id_loja,
            'shopify_order_id' => $shopify_order_id,
            'payment_status' => $payment_status,
            'order_name' => $name,
            "cart_token" => $cart_token,
            "order_status_url" => $order_status_url
        );

        $this->db->insert('orders', $data);
    }

    public function get_60days_orders($id_loja)
    {

        $this->db->where("loja_id", $id_loja);
        $this->db->order_by('id', 'DESC');
        $resultado = $this->db->get("orders");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row;
        }
    }

    public function get_auto_fail($date)
    {

        $resultado = $this->db->query("SELECT * FROM orders WHERE (status IS NULL OR status != 'approved') AND date_updated >= '$date' AND mp_id IS NOT NULL");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row;
        }
    }

    public function get_loja_id_by_mp_id($mp_id)
    {

        $this->db->select("loja_id");
        $this->db->where("mp_id", $mp_id);
        $resultado = $this->db->get("orders");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0]['loja_id'];
        }
    }

    public function get_loja_id_by_order_id($order_id)
    {

        $this->db->select("loja_id");
        $this->db->where("shopify_order_id", $order_id);
        $resultado = $this->db->get("orders");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0]['loja_id'];
        }
    }

    public function get_order_by_id($shopify_order_id)
    {

        $this->db->where('shopify_order_id', $shopify_order_id);
        $resultado = $this->db->get("orders");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function get_order_by_local_id($order_id)
    {
        $this->db->where('id', $order_id);
        $resultado = $this->db->get("orders");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function update_order($shopify_order_id, $val)
    {
        $this->db->where("shopify_order_id", $shopify_order_id);
        $this->db->update('orders', $val);
    }

    public function get_loja_shopify_by_id($id)
    {

        $this->db->where('id', $id);
        $resultado = $this->db->get("lojas");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function get_loja_by_cod_afiliado($cod_afiliado)
    {

        $this->db->where('cod_afiliado', $cod_afiliado);
        $resultado = $this->db->get("lojas");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function get_loja_shopify_by_name($shopify_nome)
    {

        $this->db->where('shopify_nome', $shopify_nome);

        $resultado = $this->db->get("lojas");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function get_all_lojas()
    {

        $resultado = $this->db->get("lojas");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row;
        }
    }
}
