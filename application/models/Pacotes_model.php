<?php

class Pacotes_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_pacotes()
    {

        $resultado = $this->db->get("pacotes");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row;
        }
    }

    public function get_id_by_mp_id($mp_id)
    {
        $this->db->select("id");
        $this->db->where("mp_id", $mp_id);
        $resultado = $this->db->get("pacotes_compras");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0]['id'];
        }
    }

    public function get_pacote_by_id($id_pacote)
    {

        $this->db->where("id", $id_pacote);
        $resultado = $this->db->get("pacotes");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }
    public function get_pacotes_compras_by_id($id)
    {

        $this->db->where("id", $id);
        $resultado = $this->db->get("pacotes_compras");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function get_pacotes_compras_by_cod_afiliado($cod_afiliado, $payment_status = 1)
    {
        $this->db->where("afiliado_de", $cod_afiliado);
        $this->db->where("payment_status", $payment_status);
        $resultado = $this->db->get("pacotes_compras");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            return $resultado->result_array();
        }
    }

    public function update_pagamento($id_compra, $val)
    {
        $this->db->where("id", $id_compra);
        $this->db->update('pacotes_compras', $val);
    }

    public function set_compra_pacote($dados)
    {

        $this->db->insert('pacotes_compras', $dados);
        return $this->db->insert_id();
    }
}
