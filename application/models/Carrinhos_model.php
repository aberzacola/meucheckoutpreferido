<?php

class Carrinhos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //cadastrado o nome da cerca no banco
    public function inserir_carrinho($dados)
    {
        $this->db->insert('carrinhos_abandonados', $dados);
        return $this->db->insert_id();
    }

    public function update_carrinho($id_carrinho, $val)
    {
        $this->db->reset_query();
        $this->db->where("id", $id_carrinho);
        $this->db->update('carrinhos_abandonados', $val);
    }


    public function get_carrinho_by_token($loja_id, $token, $email = false)
    {

        $query = "SELECT * FROM `carrinhos_abandonados` 
        WHERE `loja_id` = '$loja_id'";



        if ($email == false) {
            $query .= "AND `cart_token` = '$token'";
        } else {
            $query .= "AND `email` = '$email'";
        }

        $query .= "AND (`estagio1` < 3 OR `estagio1` = 4) 
        AND (`estagio2` < 3 OR `estagio2` = 4) 
        AND (`estagio3` < 3 OR `estagio3` = 4)
        AND !(`estagio1` = 4 AND `estagio2` = 4 AND `estagio3` = 4)";

        $resultado = $this->db->query($query);


        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $result = $resultado->result_array();
            return $result[0];
        }
    }

    public function set_estagios_recuperacao($id, $val)
    {
        $this->db->where("id", $id);
        $this->db->update('estagios_rec_lojas', $val);
    }

    public function inserir_estagios_recuperacao($dados)
    {
        $this->db->insert('estagios_rec_lojas', $dados);
        return $this->db->insert_id();
    }

    public function get_estagios_recuperacao($loja_id)
    {
        $this->db->where("loja_id", $loja_id);
        $resultado = $this->db->get("estagios_rec_lojas");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $result = $resultado->result_array();
            return $result[0];
        }
    }

    public function get_lojas_ativas_recuperacao()
    {
        $this->db->where("estagio_1_ativo", 1);
        $this->db->or_where("estagio_2_ativo", 1);
        $this->db->or_where("estagio_3_ativo", 1);
        $resultado = $this->db->get("estagios_rec_lojas");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $result = $resultado->result_array();
            return $result;
        }
    }

    public function get_carrinhos_elegiveis($loja_id, $estagio, $timer)
    {

        $resultado = $this->db->query("
        SELECT * FROM `carrinhos_abandonados` 
        WHERE `loja_id` = '$loja_id' 
        AND `estagio$estagio` = 0 
        AND (`estagio1` < 3 OR `estagio1` = 4) 
        AND (`estagio2` < 3 OR `estagio2` = 4) 
        AND (`estagio3` < 3 OR `estagio3` = 4) 
        AND `date_created` < '$timer'");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            return $resultado->result_array();
        }
    }

    public function get_carrinhos($loja_id, $excluir_estagio = false)
    {
        $this->db->where("loja_id", $loja_id);
        if ($excluir_estagio !== false) {
            $this->db->where("estagio1 !=", $excluir_estagio);
            $this->db->where("estagio2 !=", $excluir_estagio);
            $this->db->where("estagio3 !=", $excluir_estagio);
        }
        $resultado = $this->db->get("carrinhos_abandonados");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            return $resultado->result_array();
        }
    }

    public function deleta_carrinho($id_carrinho)
    {
        $this->db->reset_query();
        $this->db->where('id', $id_carrinho);
        $this->db->delete('carrinhos_abandonados');
    }

    public function deleta_carrinho_by_email($id_loja, $email)
    {
        $this->db->query("
        DELETE FROM carrinhos_abandonados 
        WHERE loja_id = '$id_loja' 
        AND email = '$email' 
        AND (`estagio1` < 3 OR `estagio1` = 4) 
        AND (`estagio2` < 3 OR `estagio2` = 4) 
        AND (`estagio3` < 3 OR `estagio3` = 4)
        AND !(`estagio1` = 4 AND `estagio2` = 4 AND `estagio3` = 4)");
    }
}
