<?php

class Descontos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //cadastrado o nome da cerca no banco
    public function inserir_desconto($loja_id, $dados)
    {
        $dados['loja_id'] = $loja_id;
        $this->db->insert('descontos', $dados);
        return $this->db->insert_id();
    }

    public function get_desconto_by_id($loja_id, $id)
    {
        $this->db->where("loja_id", $loja_id);
        $this->db->where("id", $id);
        $resultado = $this->db->get("descontos");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $result = $resultado->result_array();
            return $result[0];
        }
    }

    public function get_desconto_by_name($loja_id, $nome)
    {
        $this->db->where("loja_id", $loja_id);
        $this->db->where("nome", $nome);
        $resultado = $this->db->get("descontos");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $result = $resultado->result_array();
            return $result[0];
        }
    }

    public function get_descontos($loja_id)
    {
        $this->db->where("loja_id", $loja_id);
        $resultado = $this->db->get("descontos");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            return $resultado->result_array();
        }
    }

    public function deleta_desconto($id_desconto)
    {
        $this->db->where('id', $id_desconto);
        $this->db->delete('descontos');
    }
}
