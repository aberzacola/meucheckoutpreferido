<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Cocur\Slugify\Slugify;

class Dashboard extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function old()
	{
		$this->load->model("Loja_model", "", true);

		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		if ($loja['permitida'] == 0) {
			echo "loja não está em dia, contate o suporte: angeloghiotto@gmail.com";
		}

		$this->load->view('dashboard/old', $loja);
	}

	public function afiliados()
	{
		$this->load->model("Loja_model", "", true);

		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);
		$dados['loja'] = $loja;

		$this->load->view('dashboard/afiliados', $dados);
	}

	public function get_all_pacotes_compras()
	{
		$this->load->model("Loja_model", "", true);
		$this->load->model("Pacotes_model", "", true);

		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$tabela = array();
		if (!empty($loja['cod_afiliado'])) {
			$pacotes_vendidos = $this->Pacotes_model->get_pacotes_compras_by_cod_afiliado($loja['cod_afiliado'], 1);
			if ($pacotes_vendidos != false) {
				foreach ($pacotes_vendidos as $key => $pacote_vendido) {
					$pacote = $this->Pacotes_model->get_pacote_by_id($pacote_vendido['id_pacote']);
					$loja_compradora = $this->Loja_model->get_loja_shopify_by_name($pacote_vendido['loja']);
					$tabela[$key]['nome_loja'] = $loja_compradora['nome_loja'];

					$phpdate = strtotime($pacote_vendido['date_updated']);
					$phpdate -= 60 * 60 * 3;
					$tabela[$key]['date_updated'] = date('d/m/Y H:i', $phpdate);

					$tabela[$key]['pacote_comprado'] = $pacote['nome'];
					$tabela[$key]['id'] = $pacote['id'];
					$tabela[$key]['valor'] = 'R$' . number_format($pacote['valor'] / 100, 2, ',', '.');

					$tabela[$key]['creditado'] = $pacote_vendido['creditado'] == 0 ? "Não" : "Sim";
				}
			}
		}


		echo json_encode($tabela);
	}

	public function gerar_codigo_afiliado()
	{

		$this->load->model("Loja_model", "", true);

		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		if (!$loja['cod_afiliado']) {
			$existe = true;
			while ($existe == true) {
				$cod = strtoupper(substr(md5(mt_rand()), 0, 12));
				$existe = $this->Loja_model->get_loja_by_cod_afiliado($cod);
			}
		} else {
			$cod = $loja['cod_afiliado'];
		}

		$update['cod_afiliado'] = $cod;

		$this->Loja_model->update_loja($loja['id'], $update);

		echo json_encode(array("cod_afiliado" => $cod));
	}

	public function index()
	{
		$this->load->model("Loja_model", "", true);


		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$orders = $this->Loja_model->get_60days_orders($loja['id']);

		$dados['orders_pagas'] = 0;
		$dados['orders_nao_pagas'] = 0;

		if ($orders != false) {
			foreach ($orders as $order) {
				if ($order['payment_status'] == 1) {
					$dados['orders_pagas']++;
				} else {
					$dados['orders_nao_pagas']++;
				}
			}
		}


		$dados['orders_total'] = $dados['orders_pagas'] + $dados['orders_nao_pagas'];

		$dados['loja'] = $loja;

		$this->load->view('dashboard/index', $dados);
	}

	public function carrinho()
	{
		$this->load->model("Loja_model", "", true);
		$this->load->model("Carrinhos_model", "", true);


		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);
		$rec_carrinho = $this->Carrinhos_model->get_estagios_recuperacao($loja['id']);
		$carrinhos = $this->Carrinhos_model->get_carrinhos($loja['id']);

		$dados['loja'] = $loja;
		$dados['rec_carrinho'] = $rec_carrinho;

		$dados['estagio1']['aguardando'] = 0;
		$dados['estagio1']['enviado'] = 0;
		$dados['estagio1']['voltaram'] = 0;
		$dados['estagio1']['convertido'] = 0;

		$dados['estagio2']['aguardando'] = 0;
		$dados['estagio2']['enviado'] = 0;
		$dados['estagio2']['voltaram'] = 0;
		$dados['estagio2']['convertido'] = 0;

		$dados['estagio3']['aguardando'] = 0;
		$dados['estagio3']['enviado'] = 0;
		$dados['estagio3']['voltaram'] = 0;
		$dados['estagio3']['convertido'] = 0;

		if ($carrinhos != false) {

			foreach ($carrinhos as $carrinho) {

				switch ($carrinho['estagio1']) {
					case 0:
						$dados['estagio1']['aguardando']++;
						break;
					case 1:
						$dados['estagio1']['enviado']++;
						break;
					case 2:
						$dados['estagio1']['voltaram']++;
						break;
					case 3:
						$dados['estagio1']['convertido']++;
						continue 2;
					default:
						break;
				}


				switch ($carrinho['estagio2']) {
					case 0:
						$dados['estagio2']['aguardando']++;
						break;
					case 1:
						$dados['estagio2']['enviado']++;
						break;
					case 2:
						$dados['estagio2']['voltaram']++;
						break;
					case 3:
						$dados['estagio2']['convertido']++;
						continue 2;
					default:
						break;
				}

				switch ($carrinho['estagio3']) {
					case 0:
						$dados['estagio3']['aguardando']++;
						break;
					case 1:
						$dados['estagio3']['enviado']++;
						break;
					case 2:
						$dados['estagio3']['voltaram']++;
						break;
					case 3:
						$dados['estagio3']['convertido']++;
						continue 2;
					default:
						break;
				}
			}
		}

		$this->load->view('dashboard/carrinhos', $dados);
	}

	public function salvar_conf_carrinho()
	{
		$this->load->model("Loja_model", "", true);
		$this->load->model("Carrinhos_model", "", true);

		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$tempo_estagio_1 = $this->input->post("tempo_estagio_1");

		if ($tempo_estagio_1) {
			$tempo = explode(":", $tempo_estagio_1);
			$update['estagio_1_timer'] = ($tempo[0] * 60 * 60) + ($tempo[1] * 60) + $tempo[2];
		}

		$update['estagio_1_cupom'] = $this->input->post("cumpon_estagio_1");
		$update['estagio_1_email'] = $this->input->post("email_estagio_1");
		$update['estagio_1_assunto'] = $this->input->post("estagio_1_assunto");
		$update['estagio_1_ativo'] = $this->input->post("ativo_estagio_1") == null ? 0 : 1;

		$tempo_estagio_2 = $this->input->post("tempo_estagio_2");

		if ($tempo_estagio_2) {
			$tempo = explode(":", $tempo_estagio_2);
			$update['estagio_2_timer'] = ($tempo[0] * 60 * 60) + ($tempo[1] * 60) + $tempo[2];
		}

		$update['estagio_2_cupom'] = $this->input->post("cumpon_estagio_2");
		$update['estagio_2_email'] = $this->input->post("email_estagio_2");
		$update['estagio_2_assunto'] = $this->input->post("estagio_2_assunto");
		$update['estagio_2_ativo'] = $this->input->post("ativo_estagio_2") == null ? 0 : 1;

		$tempo_estagio_3 = $this->input->post("tempo_estagio_3");

		if ($tempo_estagio_3) {
			$tempo = explode(":", $tempo_estagio_3);
			$update['estagio_3_timer'] = ($tempo[0] * 60 * 60) + ($tempo[1] * 60) + $tempo[2];
		}

		$update['estagio_3_cupom'] = $this->input->post("cumpon_estagio_3");
		$update['estagio_3_email'] = $this->input->post("email_estagio_3");
		$update['estagio_3_assunto'] = $this->input->post("estagio_3_assunto");
		$update['estagio_3_ativo'] = $this->input->post("ativo_estagio_3") == null ? 0 : 1;


		$existe = $this->Carrinhos_model->get_estagios_recuperacao($loja['id']);

		if ($existe) {
			$this->Carrinhos_model->set_estagios_recuperacao($existe['id'], $update);
		} else {
			$update['loja_id'] = $loja['id'];
			$this->Carrinhos_model->inserir_estagios_recuperacao($update);
		}
	}

	public function auto()
	{
		$this->load->library("mercadopago");
		$this->load->model("Pacotes_model", "", true);
		$this->load->model("Loja_model", "", true);

		$pacote_id = $this->Pacotes_model->get_id_by_mp_id($this->input->get("data_id"));
		$pacotes_compras = $this->Pacotes_model->get_pacotes_compras_by_id($pacote_id);
		$pacote = $this->Pacotes_model->get_pacote_by_id($pacotes_compras['id_pacote']);

		$resposta = $this->mercadopago->acessarPagamento($this->input->get("data_id"), $this->config->item('mcp_mp_access_token'));
		$resposta_parsed = json_decode($resposta, true);

		if ($resposta_parsed['status'] == "approved" && $pacotes_compras['payment_status'] == 0) {
			$loja = $this->Loja_model->get_loja_shopify_by_name($pacotes_compras['loja']);
			$loja['mcps'] =  floatval($loja['mcps']) + intval($pacote['valor']);
			$loja['limite'] = floatval($loja['limite']) + intval($pacote['bonus']);
			$this->Loja_model->update_loja($loja['id'], $loja);
		}

		$update = array(
			"status" => $resposta_parsed['status'],
			"status_detail" => $resposta_parsed['status_detail'],
			"payment_status" => $resposta_parsed['status'] == "approved" ? 1 : 0,
			"mp_id" =>  $this->input->get("data_id")
		);

		$this->Pacotes_model->update_pagamento($pacote_id, $update);
	}

	public function processar_pagamento()
	{

		$this->load->model("Loja_model", "", true);
		$this->load->model("Pacotes_model", "", true);
		$this->load->helper("url");
		$this->load->library("mercadopago");


		$shop = $this->input->get("shop");

		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$pacote_compra['loja'] = $shop;
		$pacote_compra['afiliado_de'] = $loja['afiliado_de'];
		$pacote_compra['id_pacote'] = $this->input->post("id_pacote");


		$nome_comprador = $this->input->post("nomeComprador");
		$nome_comprador_exploded = explode(" ", trim($nome_comprador));


		$pacote = $this->Pacotes_model->get_pacote_by_id($pacote_compra['id_pacote']);

		$id_compra = $this->Pacotes_model->set_compra_pacote($pacote_compra);

		$information_mp = $this->mercadopago->informationObject();

		//payer info mp
		$information_mp->payer->first_name = $nome_comprador_exploded[0];
		$information_mp->payer->last_name = end($nome_comprador_exploded);

		$this->mercadopago->__set("additional_info", $information_mp);


		$slugify = new Slugify(['separator' => ' ', 'lowercase' => false]);
		$this->mercadopago->__set('external_reference', $id_compra);
		$this->mercadopago->__set('description', "MCP - Pacote:" . $pacote['nome']);
		$this->mercadopago->__set('statement_descriptor', "MCP|PACOTE " . $slugify->slugify($pacote['nome']));
		$this->mercadopago->__set('installments', $this->input->post('parcelas') ? (int) $this->input->post('parcelas') : 1);
		$this->mercadopago->__set('transaction_amount', (float) $pacote['valor'] / 100);
		$this->mercadopago->__set('payment_method_id', $this->input->post("paymentMethodId"));

		$payer = $this->mercadopago->payerObject();
		$payer->email = $this->input->post("emailComprador");
		$payer->first_name = $nome_comprador_exploded[0];
		$payer->last_name = end($nome_comprador_exploded);

		if ($this->input->post('token')) {
			$this->mercadopago->__set('token', $this->input->post('token'));
		} else {
			$payer->identification->type = "cpf";
			$payer->identification->number = $this->input->post("documentoComprador");
		}


		$this->mercadopago->__set("payer", $payer);
		$this->mercadopago->__set("notification_url", $this->config->item("base_url") . "dashboard/auto");

		$mp_retorno = $this->mercadopago->criaPagamento($this->config->item("mcp_mp_access_token"));

		for ($x = 0; isset($mp_retorno['message']) && $x < 2; $x++) {
			sleep(1);
			$mp_retorno = $this->mercadopago->criaPagamento($this->config->item("mp_access_token"));
		}

		$dados = [
			"mp_id" => $mp_retorno['id'],
			"tipo_pagamento" => $mp_retorno['payment_type_id'],
			"url_boleto" => $mp_retorno["transaction_details"]["external_resource_url"],
			"codigo_boleto" => isset($mp_retorno["barcode"]["content"]) ? $mp_retorno["barcode"]["content"] : "",
			"valor_total" => (int) ($mp_retorno['transaction_amount'] * 100),
			"moeda" => $mp_retorno['currency_id']
		];

		$this->Pacotes_model->update_pagamento($id_compra, $dados);

		redirect($this->config->item("base_url") . "dashboard/loja/1/?" . $_SERVER['QUERY_STRING']);
	}

	public function reemitir_boleto()
	{
		$this->load->model("Loja_model", "", true);
		$this->load->model("Descontos_model", "", true);
		$this->load->helper("url");
		$this->load->library("mercadopago");
		$this->load->library("shopify");

		$order_id = $this->input->post("order_id");

		$shop = $this->input->get("shop");

		$estadosBrasileiros = array(
			'AC' => 'Acre',
			'AL' => 'Alagoas',
			'AP' => 'Amapá',
			'AM' => 'Amazonas',
			'BA' => 'Bahia',
			'CE' => 'Ceará',
			'DF' => 'Distrito Federal',
			'ES' => 'Espírito Santo',
			'GO' => 'Goiás',
			'MA' => 'Maranhão',
			'MT' => 'Mato Grosso',
			'MS' => 'Mato Grosso do Sul',
			'MG' => 'Minas Gerais',
			'PA' => 'Pará',
			'PB' => 'Paraíba',
			'PR' => 'Paraná',
			'PE' => 'Pernambuco',
			'PI' => 'Piauí',
			'RJ' => 'Rio de Janeiro',
			'RN' => 'Rio Grande do Norte',
			'RS' => 'Rio Grande do Sul',
			'RO' => 'Rondônia',
			'RR' => 'Roraima',
			'SC' => 'Santa Catarina',
			'SP' => 'São Paulo',
			'SE' => 'Sergipe',
			'TO' => 'Tocantins',
		);


		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$order = $this->Loja_model->get_order_by_local_id($order_id);

		$order_details = $this->shopify->get_order_detail($shop, $loja['token'], $order['shopify_order_id']);
		$order_details = json_decode($order_details, true);


		if (count($order_details['order']["shipping_lines"]) > 0) {
			$metodoEnvio_price = $order_details['order']["shipping_lines"][0]['price'];
			$metodoEnvio_code = $order_details['order']["shipping_lines"][0]['code'];
			$metodoEnvio_title = $order_details['order']["shipping_lines"][0]['title'];
		} else {
			$metodoEnvio_price = 0;
			$metodoEnvio_code = -1;
			$metodoEnvio_title = "Frete Grátis";
		}

		$nome_comprador = $order['nome_comprador'];
		$nome_comprador_exploded = explode(" ", trim($nome_comprador));

		$celular_comprador = trim($order['celular_comprador']);
		$celular_comprador_exploded = explode(" ", $celular_comprador);
		$ddd = str_ireplace("(", "", $celular_comprador_exploded[0]);
		$ddd = str_ireplace(")", "", $ddd);

		$total_order = $order_details['order']['total_price'] + $metodoEnvio_price;
		$total_order_produtos = $order_details['order']['total_line_items_price'];
		$total_envio = $metodoEnvio_price;

		$order_update = array(
			"order" => array(
				"line_items" => array(),
				"email" => $order_details['order']['email'],
				"phone" => $celular_comprador,
				"shipping_address" => array(
					"address1" => $order_details['order']['shipping_address']['address1'],
					"address2" => $order_details['order']['shipping_address']['address2'],
					"city" => $order_details['order']['shipping_address']['city'],
					"company" => null,
					"country" => "Brazil",
					"first_name" => $order_details['order']['shipping_address']['first_name'],
					"last_name" => $order_details['order']['shipping_address']['last_name'],
					"phone" => $order_details['order']['shipping_address']['phone'],
					"province" => $order_details['order']['shipping_address']['province'],
					"zip" =>  $order_details['order']['shipping_address']['zip'],
					"name" => $nome_comprador,
					"country_code" => "BR",
					"province_code" => $order_details['order']['shipping_address']['province_code'],
				),
				"buyer_accepts_marketing" => true,
				"financial_status" => "pending",
				"tags" => "meu_checkout_preferido",
			),
		);

		$order_update['order']['discount_codes'] = array();

		$desconto_soma = 0;
		$cupom = $this->Descontos_model->get_desconto_by_id($loja['id'], $this->input->post("cupom_id"));

		if ($cupom != false) {
			if ($cupom['tipo'] == "total") {
				if ($cupom['modalidade'] == "porcentagem") {
					$desconto = $total_order * ($cupom['valor'] / 100);
					$desconto_soma += $desconto;

					$order_update['order']['discount_codes'][] = array(
						"code" => $cupom['nome'],
						"amount" => $desconto,
						"type" => "fixed_amount"
					);
				} elseif ($cupom['modalidade'] == "fixo") {
					$desconto = ($cupom['valor'] / 100);
					$desconto_soma += $desconto;
					$order_update['order']['discount_codes'][] = array(
						"code" => $cupom['nome'],
						"amount" => $desconto,
						"type" => "fixed_amount"
					);
				}
			}

			if ($cupom['tipo'] == "produtos") {
				if ($cupom['modalidade'] == "porcentagem") {
					$desconto = $total_order_produtos * ($cupom['valor'] / 100);
					$desconto_soma += $desconto;

					$order_update['order']['discount_codes'][] = array(
						"code" => $cupom['nome'],
						"amount" => $desconto,
						"type" => "fixed_amount"
					);
				} elseif ($cupom['modalidade'] == "fixo") {
					$desconto = ($cupom['valor'] / 100);
					$desconto_soma += $desconto;
					$order_update['order']['discount_codes'][] = array(
						"code" => $cupom['nome'],
						"amount" => $desconto,
						"type" => "fixed_amount"
					);
				}
			}

			if ($cupom['tipo'] == "frete") {
				if ($cupom['modalidade'] == "porcentagem") {
					$desconto = $total_envio * ($cupom['valor'] / 100);
					$desconto_soma += $desconto;

					$order_update['order']['discount_codes'][] = array(
						"code" => $cupom['nome'],
						"amount" => $desconto,
						"type" => "fixed_amount"
					);
				} elseif ($cupom['modalidade'] == "fixo") {
					$desconto = ($cupom['valor'] / 100);
					$desconto_soma += $desconto;
					$order_update['order']['discount_codes'][] = array(
						"code" => $cupom['nome'],
						"amount" => $desconto,
						"type" => "fixed_amount"
					);
				}
			}
		}

		if ($metodoEnvio_code != -1 && $metodoEnvio_title != -1) {
			$order_update['order']["shipping_lines"] = array(
				array(
					"code" => $metodoEnvio_code,
					"price" => $metodoEnvio_price,
					"title" => $metodoEnvio_title,
					"tax_lines" => array(),
				)
			);
		}

		$slugify = new Slugify(['separator' => ' ', 'lowercase' => false]);
		foreach ($order_details['order']['line_items'] as $key => $item) {
			$order_update['order']['line_items'][$key]['variant_id'] = $item['variant_id'];
			$order_update['order']['line_items'][$key]['quantity'] = $item['quantity'];

			$produto = $this->shopify->get_product_info($shop, $loja['token'], $item['product_id']);
			$produto = json_decode($produto, true);

			$items_mp[$key] = $this->mercadopago->itemObject();
			$items_mp[$key]->id = $item['variant_id'];
			$items_mp[$key]->title = $slugify->slugify($item['title']);
			$items_mp[$key]->description = $slugify->slugify($produto["product"]['body_html']);
			$items_mp[$key]->picture_url = $produto["product"]["image"]["src"];
			$items_mp[$key]->category_id = $item['product_id'];
			$items_mp[$key]->quantity = (int) $item['quantity'];
			$items_mp[$key]->unit_price = (float) $item['price'];
		}

		$information_mp = $this->mercadopago->informationObject();
		$information_mp->items = $items_mp;

		//payer info mp
		$information_mp->payer->first_name = $nome_comprador_exploded[0];
		$information_mp->payer->last_name = end($nome_comprador_exploded);
		$information_mp->payer->phone->area_code = $ddd;
		$information_mp->payer->phone->number = end($celular_comprador_exploded);
		$information_mp->payer->address->zip_code = $order_details['order']["shipping_address"]['zip'];
		$information_mp->payer->address->street_name = $order_details['order']["shipping_address"]['address1'];
		$information_mp->payer->address->street_number = "with the street name";


		$information_mp->shipments->receiver_address->zip_code = $order_details['order']["shipping_address"]['zip'];
		$information_mp->shipments->receiver_address->state_name = $order_details['order']["shipping_address"]['province'];
		$information_mp->shipments->receiver_address->city_name = $order_details['order']["shipping_address"]['city'];
		$information_mp->shipments->receiver_address->street_name = $order_details['order']["shipping_address"]['address1'];
		$information_mp->shipments->receiver_address->street_number = "with the street name";
		$information_mp->shipments->receiver_address->floor = $order_details['order']["shipping_address"]['address2'];
		$information_mp->shipments->receiver_address->apartment = $order_details['order']["shipping_address"]['address2'];

		$this->mercadopago->__set("additional_info", $information_mp);

		$order_update['order']['transactions'] = array(
			array(
				"amount" => $total_order - $desconto_soma,
				"kind" => "authorization",
				"status" => "success",
				"gateway" => "mercado_pago"
			)
		);


		$order_done = $this->shopify->create_order($shop, $loja["token"], $order_update);
		$order_done = json_decode($order_done, true);

		$this->Loja_model->inserir_ordem($loja["id"], $order_done["order"]['id'], 0, $order_done["order"]['name'], $order_done['order']['token'], $order_done['order']['order_status_url']);

		$this->shopify->delete_order($shop, $loja["token"], $order_details['order']['id']);
		$this->Loja_model->update_order($order_details['order']['id'], array("excluida" => 1, "nova_order_name" => $order_done["order"]['name'], "nova_shopify_order_id" => $order_done["order"]['id']));


		$this->mercadopago->__set('external_reference',  $order_done["order"]['id']);
		$this->mercadopago->__set('description', $loja["nome_loja"] . " - pedido: " . $order_done["order"]['name']);
		$this->mercadopago->__set('statement_descriptor', $order_done["order"]['name'] . "|" . $loja["nome_loja"]);
		$this->mercadopago->__set('installments', 1);
		$this->mercadopago->__set('transaction_amount', (float) $order_done["order"]['total_price']);
		$this->mercadopago->__set('payment_method_id', 'bolbradesco');

		$payer = $this->mercadopago->payerObject();
		$payer->email = $order['email_comprador'];
		$payer->first_name = $nome_comprador_exploded[0];
		$payer->last_name = end($nome_comprador_exploded);


		$payer->identification->type = "cpf";
		$payer->identification->number = $this->input->post("cpf_comprador");

		$this->mercadopago->__set("payer", $payer);
		$this->mercadopago->__set("notification_url", $this->config->item("base_url") . "checkout/auto");

		$mp_retorno = $this->mercadopago->criaPagamento($loja["mp_access_token"]);

		$s3 = new Aws\S3\S3Client([
			'region'  => $this->config->item('aws_region'),
			'version' => 'latest',
			'credentials' => [
				'key'    => $this->config->item('aws_key_id'),
				'secret' => $this->config->item('aws_secret'),
			]
		]);

		for ($x = 0; isset($mp_retorno['message']) && $x < 2; $x++) {
			sleep(1);
			$result = $s3->putObject([
				"ACL" => 'public-read',
				'Bucket' => $this->config->item('aws_s3_logs_bucket_name'),
				'Key'    => $shop . "/orders/recuperacao_" . $order_done["order"]['id'] . "_$x.txt",
				'Body' => json_encode($mp_retorno)
			]);
			$mp_retorno = $this->mercadopago->criaPagamento($loja["mp_access_token"]);
		}

		$result = $s3->putObject([
			"ACL" => 'public-read',
			'Bucket' => $this->config->item('aws_s3_logs_bucket_name'),
			'Key'    => $shop . "/orders/recuperacao_" . $order_done["order"]['id'] . "_final.txt",
			'Body' => json_encode($mp_retorno)
		]);

		$dados = [
			"nome_comprador" => $nome_comprador_exploded[0] . " " . end($nome_comprador_exploded),
			"email_comprador" =>  $order_details['order']['email'],
			"endereco_comprador" => $order_details['order']['shipping_address']['address1'] . " " . $order_details['order']['shipping_address']['address2'] . " " . $order_details['order']['shipping_address']['city'] . " " . $order_details['order']['shipping_address']['province'],
			"celular_comprador" => $celular_comprador,
			"mp_id" => $mp_retorno['id'],
			"mp_access_token" => $loja["mp_access_token"],
			"tipo_pagamento" => $mp_retorno['payment_type_id'],
			"url_boleto" => $mp_retorno["transaction_details"]["external_resource_url"],
			"codigo_boleto" => isset($mp_retorno["barcode"]["content"]) ? $mp_retorno["barcode"]["content"] : "",
			"valor_total" => (int) ($mp_retorno['transaction_amount'] * 100),
			"moeda" => $mp_retorno['currency_id']
		];

		$this->Loja_model->update_order($order_done["order"]['id'], $dados);
	}

	public function deletar_desconto()
	{

		$this->load->model("Descontos_model", "", true);

		$id_desconto = $this->input->post("id_desconto");

		$this->Descontos_model->deleta_desconto($id_desconto);
	}

	public function descontos()
	{
		$this->load->model("Loja_model", "", true);
		$this->load->model("Descontos_model", "", true);


		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$dados['descontos'] = $this->Descontos_model->get_descontos($loja['id']);
		$dados['loja'] = $loja;

		$this->load->view('dashboard/descontos', $dados);
	}

	public function salva_desconto()
	{
		$this->load->model("Descontos_model", "", true);
		$this->load->model("Loja_model", "", true);

		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$nome_desconto = $this->input->post("desconto_nome");
		$desconto = $this->Descontos_model->get_desconto_by_name($loja['id'], $nome_desconto);

		if ($desconto == false) {
			$dados['nome'] = $nome_desconto;
			$dados['tipo'] = $this->input->post("tipo_desconto");
			$dados['modalidade'] = $this->input->post("modalidade_desconto");
			$dados['valor'] = $this->input->post("desconto_valor_puro");
			$this->Descontos_model->inserir_desconto($loja['id'], $dados);

			echo json_encode(array("resposta" => true));
		} else {
			echo json_encode(array("resposta" => false));
		}
	}

	public function configuracoes()
	{
		$this->load->model("Loja_model", "", true);


		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$dados['loja'] = $loja;

		$this->load->view('dashboard/configuracoes', $dados);
	}

	public function loja($comprou = 0)
	{
		$this->load->model("Loja_model", "", true);
		$this->load->model("Pacotes_model", "", true);


		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$dados['pacotes'] = $this->Pacotes_model->get_pacotes();
		$dados['comprou'] = $comprou;

		$dados['loja'] = $loja;

		$this->load->view('dashboard/loja', $dados);
	}

	public function suporte()
	{
		$this->load->model("Loja_model", "", true);


		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$dados['loja'] = $loja;

		$this->load->view('dashboard/suporte', $dados);
	}

	public function update_loja_config()
	{
		$this->load->model("Loja_model", "", true);
		$this->load->library("shopify");
		$this->load->helper('url');
		$this->load->helper('file');

		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		if ($loja['limite'] <= 0 && $loja['mcps'] <= 0) {
			return false;
		}

		$mp_public_key = trim($this->input->post("mp_public_key"));
		$mp_access_token = trim($this->input->post("mp_access_token"));
		$fb_pixel = trim($this->input->post("fb_pixel"));
		$desconto_cartao = trim($this->input->post("desconto_cartao"));
		$status_checkout = $this->input->post("status_checkout");
		$pagina_obrigado = $this->input->post("pagina_obrigado");
		$pixel_boleto = $this->input->post("pixel_boleto");
		$escassez = $this->input->post("escassez");
		$texto_escassez = $this->input->post("texto_escassez");
		$remover_banner_lateral = $this->input->post("remover_banner_lateral");
		$remover_banner_superior = $this->input->post("remover_banner_superior");
		$remover_pagina_obrigado = $this->input->post("remover_pagina_obrigado");


		if (isset($_FILES["banner_lateral"]) && $remover_banner_lateral != "on") {
			$file_name = strtolower($_FILES['banner_lateral']['name']);
			$temp_file_location = $_FILES['banner_lateral']['tmp_name'];

			$ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

			$supported_image = array(
				'gif',
				'jpg',
				'jpeg',
				'png'
			);

			if (in_array($ext, $supported_image)) {

				$slugify = new Slugify();
				$file_name = str_ireplace("." . $ext, "", $file_name);
				$banner_lateral =  $slugify->slugify($file_name) . "_banner_lateral";
				$banner_lateral .= "." . $ext;

				$s3 = new Aws\S3\S3Client([
					'region'  => $this->config->item('aws_region'),
					'version' => 'latest',
					'credentials' => [
						'key'    => $this->config->item('aws_key_id'),
						'secret' => $this->config->item('aws_secret'),
					]
				]);

				$result = $s3->putObject([
					"ACL" => 'public-read',
					'Bucket' => $this->config->item('aws_s3_bucket_name'),
					'Key'    => $shop . "/" . $banner_lateral,
					'SourceFile' => $temp_file_location
				]);
			}
		}

		if (isset($_FILES["banner_superior"]) && $remover_banner_superior != "on") {
			$file_name = strtolower($_FILES['banner_superior']['name']);
			$temp_file_location = $_FILES['banner_superior']['tmp_name'];

			$ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

			$supported_image = array(
				'gif',
				'jpg',
				'jpeg',
				'png'
			);

			if (in_array($ext, $supported_image)) {

				$slugify = new Slugify();
				$file_name = str_ireplace("." . $ext, "", $file_name);
				$banner_superior =  $slugify->slugify($file_name) . "_banner_superior";;
				$banner_superior .= "." . $ext;

				$s3 = new Aws\S3\S3Client([
					'region'  => $this->config->item('aws_region'),
					'version' => 'latest',
					'credentials' => [
						'key'    => $this->config->item('aws_key_id'),
						'secret' => $this->config->item('aws_secret'),
					]
				]);

				$result = $s3->putObject([
					"ACL" => 'public-read',
					'Bucket' => $this->config->item('aws_s3_bucket_name'),
					'Key'    => $shop . "/" . $banner_superior,
					'SourceFile' => $temp_file_location
				]);
			}
		}

		if (isset($_FILES["pagina_obrigado"]) && $remover_pagina_obrigado != "on") {
			$file_name = strtolower($_FILES['pagina_obrigado']['name']);
			$temp_file_location = $_FILES['pagina_obrigado']['tmp_name'];

			$ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

			$supported_files = array(
				'html',
				'htm'
			);

			if (in_array($ext, $supported_files)) {

				$slugify = new Slugify();
				$file_name = str_ireplace("." . $ext, "", $file_name);
				$pagina_obrigado =  $slugify->slugify($file_name) . "_pagina_obrigado";;
				$pagina_obrigado .= "." . $ext;

				$s3 = new Aws\S3\S3Client([
					'region'  => $this->config->item('aws_region'),
					'version' => 'latest',
					'credentials' => [
						'key'    => $this->config->item('aws_key_id'),
						'secret' => $this->config->item('aws_secret'),
					]
				]);

				$result = $s3->putObject([
					"ACL" => 'public-read',
					'Bucket' => $this->config->item('aws_s3_pages_bucket_name'),
					'Key'    => $shop . "/" . $pagina_obrigado,
					'Body' => file_get_contents($temp_file_location),
					'ContentType' => 'text/html',
				]);
			}
		}


		$update = array();

		if (isset($banner_lateral)) {
			$update['banner_lateral'] = $banner_lateral;
		} elseif ($remover_banner_lateral == "on") {
			$update['banner_lateral'] = "";
		}
		if (isset($banner_superior)) {
			$update['banner_superior'] = $banner_superior;
		} elseif ($remover_banner_superior == "on") {
			$update['banner_superior'] = "";
		}

		if (isset($mp_public_key)) {
			$update['mp_public_key'] = $mp_public_key;
		}

		if (isset($mp_access_token)) {
			$update['mp_access_token'] = $mp_access_token;
		}

		if (isset($fb_pixel)) {
			$update['fb_pixel'] = $fb_pixel;
		}

		if (isset($fb_pixel)) {
			$update['desconto_cartao'] = $desconto_cartao;
		}

		if (isset($pagina_obrigado)) {
			$update['pagina_obrigado'] = $pagina_obrigado;
		} elseif ($remover_pagina_obrigado == "on") {
			$update['pagina_obrigado'] = "";
		}

		if (isset($pixel_boleto)) {
			$update['pixel_boleto'] = $pixel_boleto;
		}

		if (isset($escassez)) {
			$tempo = explode(":", $escassez);
			$update['escassez'] = ($tempo[0] * 60 * 60) + ($tempo[1] * 60) + $tempo[2];
		}

		if (isset($texto_escassez)) {
			$update['texto_escassez'] = $texto_escassez;
		}



		$status = $this->shopify->get_checkout_info($shop, $loja['token']);
		$status = json_decode($status, true);

		// formulario		
		$existe = false;
		foreach ($status['script_tags'] as $tag) {
			if ($tag['id'] == $loja['script_tag']) {
				$existe = true;
				break;
			}
		}

		switch ($status_checkout) {
			case 1:
				if ($existe == false) {
					$resultado = $this->shopify->turn_on_checkout($shop, $loja['token']);
					$resultado = json_decode($resultado, true);
					$update['script_tag'] = $resultado['script_tag']["id"];
				}
				break;
			case 0:
				if ($existe == true) {
					$resultado = $this->shopify->turn_off_checkout($shop, $loja['token'], $loja['script_tag']);
				}
				$update['script_tag'] = 0;
				break;
		}

		// formulario		
		$existe = false;
		foreach ($status['script_tags'] as $tag) {
			if ($tag['id'] == $loja['form_tag']) {
				$existe = true;
				break;
			}
		}

		switch ($status_checkout) {
			case 1:
				if ($existe == false) {
					$resultado = $this->shopify->turn_on_compre_ja($shop, $loja['token']);
					$resultado = json_decode($resultado, true);
					$update['form_tag'] = $resultado['script_tag']["id"];
				}
				break;
			case 0:
				if ($existe == true) {
					$resultado = $this->shopify->turn_off_checkout($shop, $loja['token'], $loja['form_tag']);
				}
				$update['form_tag'] = 0;
				break;
		}

		// vacina
		$existe_vacina = false;
		foreach ($status['script_tags'] as $tag) {
			if ($tag['id'] == $loja['vacina_cartx']) {
				$existe_vacina = true;
				break;
			}
		}

		switch ($status_checkout) {
			case 1:
				if ($existe_vacina == false) {
					$resultado = $this->shopify->aplica_vacina($shop, $loja['token']);
					$resultado = json_decode($resultado, true);
					$update['vacina_cartx'] = $resultado['script_tag']["id"];
				}
				break;
			case 0:
				if ($existe_vacina == true) {
					$resultado = $this->shopify->turn_off_checkout($shop, $loja['token'], $loja['vacina_cartx']);
				}
				$update['vacina_cartx'] = 0;
				break;
		}


		$this->Loja_model->update_loja($loja['id'], $update);
	}

	public function gerar_script_output()
	{
		$this->load->model("Loja_model", "", true);
		$this->load->library("shopify");

		$todas_lojas = $this->Loja_model->get_all_lojas();
		foreach ($todas_lojas as $loja) {
			if ($loja['script_tag'] > 0 && $loja['vacina_cartx'] > 0 && $loja['form_tag'] > 0) {
				$this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['script_tag']);
				$this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['vacina_cartx']);
				$this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['form_tag']);

				$resultado = $this->shopify->turn_on_checkout($loja['shopify_nome'], $loja['token']);
				$resultado = json_decode($resultado, true);
				$update['script_tag'] = $resultado['script_tag']["id"];

				$resultado_vacina = $this->shopify->turn_on_compre_ja($loja['shopify_nome'], $loja['token']);
				$resultado_vacina = json_decode($resultado_vacina, true);
				$update['form_tag'] = $resultado_vacina['script_tag']["id"];

				$resultado_vacina = $this->shopify->aplica_vacina($loja['shopify_nome'], $loja['token']);
				$resultado_vacina = json_decode($resultado_vacina, true);
				$update['vacina_cartx'] = $resultado_vacina['script_tag']["id"];

				$this->Loja_model->update_loja($loja['id'], $update);
			}
		}
	}

	public function order_detail()
	{
		$this->load->model("Loja_model", "", true);
		$this->load->library("shopify");
		$this->load->library("mercadopago");
		$this->load->library('encryption');

		$shop = $this->input->get("shop");
		$loja = $this->Loja_model->get_loja_shopify_by_name($shop);

		$order_id = $this->input->post("order_id");
		$order = $this->Loja_model->get_order_by_local_id($order_id);
		$order['cpf_comprador'] = $this->encryption->decrypt($order['cpf_comprador']);
		$order['cpf_comprador'] = $order['cpf_comprador'] == false ? "" : $order['cpf_comprador'];

		$order_details = $this->shopify->get_order_detail($shop, $loja['token'], $order['shopify_order_id']);
		$order_details = json_decode($order_details, true);
		if (isset($order_details['order'])) {
			$order_details = $order_details['order'];

			$order_details['total_discounts_formated'] = "R$ " . number_format($order_details['total_discounts'], 2, ',', '.');

			$order['order_detail_shopify'] = $order_details;
			$order = $this->formata_order($order);
		}

		echo json_encode($order);
	}

	public function get_all_orders()
	{

		$this->load->model("Loja_model", "", true);

		$orders = $this->Loja_model->get_60days_orders($this->config->item("id_loja_shopify"));

		if ($orders != false) {
			foreach ($orders as $order) {
				$orders_formated[] = $this->formata_order($order);
			}
		} else {
			$orders_formated = array();
		}

		echo json_encode($orders_formated);
	}

	private function formata_order($order)
	{

		$phpdate = strtotime($order['date_updated']);
		$phpdate -= 60 * 60 * 3;
		$order['date_updated'] = date('d/m/Y H:i', $phpdate);

		$phpdate = strtotime($order['date_created']);
		$phpdate -= 60 * 60 * 3;
		$order['date_created'] = date('d/m/Y H:i', $phpdate);


		switch ($order['status']) {
			case "approved":
				$order['status'] = "Pago";
				break;
			case "in_process":
				$order['status'] = "Em Processo";
				break;
			case "canceled":
				$order['status'] = "Cancelado";
				break;
			case "pending":
				$order['status'] = "Pendente";
				break;
			case "refunded":
				$order['status'] = "Estornado";
				break;
			case "rejected":
				$order['status'] = "Rejeitado";
				break;
		}

		switch ($order['tipo_pagamento']) {
			case "ticket":
				$order['tipo_pagamento'] = "Boleto";
				break;
			case "credit_card":
				$order['tipo_pagamento'] = "Cartão de Crédito";
				break;
		}

		switch ($order['excluida']) {
			case 0:
				$order['excluida'] = "Não";
				break;
			case 1:
				$order['excluida'] = "Sim substituta -> " . $order['nova_order_name'];
				$phpdate = strtotime($order['date_created']);
				$phpdate -= 60 * 60 * 3;
				$order['date_updated'] = date('d/m/Y H:i', $phpdate);
				break;
		}

		switch ($order['status_detail']) {
			case "accredited":
				$order['status_detail'] = "Confirmado";
				break;
			case "by_payer":
				$order['status_detail'] = "Pelo comprador";
				break;
			case "cc_rejected_bad_filled_card_number":
				$order['status_detail'] = "Num. incorreto";
				break;
			case "cc_rejected_bad_filled_date":
				$order['status_detail'] = "Data de validade incorreta";
				break;
			case "cc_rejected_bad_filled_security_code":
				$order['status_detail'] = "Código segurança incorreto";
				break;
			case "cc_rejected_blacklist":
				$order['status_detail'] = "Cartão na blacklist";
				break;
			case "cc_rejected_call_for_authorize":
				$order['status_detail'] = "Ligar para autorizar";
				break;
			case "cc_rejected_card_disabled":
				$order['status_detail'] = "Cartão desabilitado";
				break;
			case "cc_rejected_high_risk":
				$order['status_detail'] = "Alto risco de fraude";
				break;
			case "cc_rejected_insufficient_amount":
				$order['status_detail'] = "Sem fundos";
				break;
			case "cc_rejected_other_reason":
				$order['status_detail'] = "Razão desconhecida";
				break;
			case "pending_review_manual":
				$order['status_detail'] = "Aguardando revisão manual";
				break;
			case "pending_waiting_payment":
				$order['status_detail'] = "Aguardando pagamento";
				break;
			case "refunded":
				$order['status_detail'] = "Estornado";
				break;
		}

		$order['valor_total'] = "R$ " . number_format($order['valor_total'] / 100, 2, ',', '.');

		return $order;
	}
}
