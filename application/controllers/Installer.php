<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Installer extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
  public function index()
  {
    $this->load->view("install");
  }

  public function redirect()
  {
    // Set variables for our request
    $shop = $this->input->get('shop');
    $afiliado = $this->input->get('afiliado');
    if(!empty($afiliado)){
      session_start();
      $_SESSION["afiliado_id"] = $afiliado;
    }
    $shop = str_ireplace(".myshopify.com", "", strtolower($shop));
    $shop = str_ireplace("https://", "", $shop);
    $api_key = $this->config->item('shopify_api_key');
    $scopes = "read_orders,read_customers,read_discounts, write_discounts,read_price_rules,write_products,read_script_tags, write_script_tags,read_content, write_content,read_themes, write_themes,read_orders, write_orders,read_draft_orders, write_draft_orders,read_inventory, write_inventory,read_fulfillments, write_fulfillments,read_shipping, write_shipping,read_checkouts, write_checkouts";
    $redirect_uri = $this->config->item('base_url') . "installer/generate_token";

    // Build install/approval URL to redirect to
    $install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

    // Redirect
    header("Location: " . $install_url);
    die();
  }

  public function generate_token()
  {

    $this->load->helper("url");

    $afiliado = -1;
    session_start();
    if(isset($_SESSION['afiliado_id'])){
      $afiliado = $_SESSION['afiliado_id'];
      unset($_SESSION['afiliado_id']);
    }

    // Set variables for our request
    $api_key = $this->config->item('shopify_api_key');
    $shared_secret =  $this->config->item('shopify_shared_secret');
    $params = $this->input->get(); // Retrieve all request parameters

    // Set variables for our request
    $query = array(
      "client_id" => $api_key, // Your API key
      "client_secret" => $shared_secret, // Your app credentials (secret key)
      "code" => $params['code'] // Grab the access key from the URL
    );

    // Generate access token URL
    $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

    // Configure curl client and execute request
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $access_token_url);
    curl_setopt($ch, CURLOPT_POST, count($query));
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
    $result = curl_exec($ch);
    curl_close($ch);

    // Store the access token
    $result = json_decode($result, true);
    $access_token = $result['access_token'];

    //Deu tudo certo, vamos gravar a loja e o token
    $this->load->model('Loja_model', '', TRUE);
    $this->load->library('shopify');
    $loja_ja_existe =  $this->Loja_model->get_loja_shopify_by_name($params['shop']);

    if ($loja_ja_existe == false) {
      $id_loja = $this->Loja_model->inserir_loja($params['shop'], $access_token);
      $afiliado_existe = $this->Loja_model->get_loja_by_cod_afiliado($afiliado);
      $update["mcps"] = 0;
      if (!$afiliado_existe) {
        $update["limite"] = 2500;
      } else {
        $update["limite"] = 5000;
        $update['afiliado_de'] = $afiliado;
      }
    } else {
      $id_loja = $loja_ja_existe['id'];
      $update = array(
        "token" => $access_token
      );
    }

    $detalhes = $this->shopify->get_shop_info($params['shop'], $access_token);
    $detalhes = json_decode($detalhes, true);

    $update["dominio_proprio"] = $detalhes['shop']['domain'];
    $update["pagina_obrigado"] = "";
    $update["nome_loja"] = $detalhes['shop']['name'];
    $update["pixel_boleto"] = "desligado";
    $update["script_tag"] = 0;
    $update["form_tag"] = 0;
    $update["vacina_cartx"] = 0;
    $update["email_contato"] = empty($detalhes['shop']['customer_email']) ? $detalhes['shop']['email'] : $detalhes['shop']['customer_email'];

    $this->Loja_model->update_loja($id_loja, $update);

    redirect("https://{$params['shop']}/admin");
  }

  public function teste()
  {

    $this->load->library('shopify');
    $resultado = $this->shopify->get_shop_info("side-store-2.myshopify.com", "40543199d4f2951ea8aba33974aef29d");
    var_dump($resultado);
  }
}
