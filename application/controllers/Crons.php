<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Crons extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function processar_mp()
    {
        $this->load->library("mercadopago");
        $this->load->library("shopify");
        $this->load->model("Loja_model", "", true);

        $client = new Aws\Sqs\SqsClient([
            'region' => $this->config->item('aws_region'),
            'version' => 'latest',
            'credentials' => [
                'key'    => $this->config->item('aws_key_id'),
                'secret' => $this->config->item('aws_secret'),
            ]
        ]);

        try {
            $result = $client->receiveMessage(array(
                'AttributeNames' => ['SentTimestamp'],
                'MaxNumberOfMessages' => 10,
                'MessageAttributeNames' => ['Body'],
                'QueueUrl' => $this->config->item('sqs_pagamentos_mp_url'), // REQUIRED
                'WaitTimeSeconds' => 20,
            ));

            if ($result->get('Messages') !== null && count($result->get('Messages')) > 0) {

                foreach ($result->get('Messages') as $msg) {

                    $dados = json_decode($msg['Body'], true);

                    if (isset($dados['type']) && $dados['type'] == "payment") {
                        $id_mp = $dados["data"]['id'];

                        $this->load->library("mercadopago");
                        $this->load->library("shopify");
                        $this->load->model("Loja_model", "", true);

                        $loja_id = $this->Loja_model->get_loja_id_by_mp_id($id_mp);

                        if ($loja_id == false) {
                            continue;
                        }

                        $loja = $this->Loja_model->get_loja_shopify_by_id($loja_id);

                        $resposta = $this->mercadopago->acessarPagamento($id_mp, $loja['mp_access_token']);
                        $resposta_parsed = json_decode($resposta, true);

                        $referencia_externa = $resposta_parsed['external_reference'];
                        $update = array(
                            "status" => $resposta_parsed['status'],
                            "status_detail" => $resposta_parsed['status_detail'],
                            "payment_status" => $resposta_parsed['status'] == "approved" ? 1 : 0,
                            "mp_id" =>  $id_mp
                        );

                        $this->Loja_model->update_order($referencia_externa, $update);

                        if ($resposta_parsed['status'] == "approved") {

                            $transactions = json_decode($this->shopify->get_transactions($loja['shopify_nome'], $loja['token'], $referencia_externa), true);
                            $ultima_transaction = end($transactions["transactions"]);
                            $id_ultima_transaction = $ultima_transaction['id'];

                            $transaction = array(
                                "transaction" => array(
                                    "parent_id" => $id_ultima_transaction,
                                    "kind" => "capture",
                                    "gateway" => "mercado_pago",
                                    "amount" => $resposta_parsed['transaction_amount'],
                                    "status" => "success",
                                    "currency" => "BRL"
                                )
                            );

                            $cobranca = $resposta_parsed['transaction_amount'] * 0.01;
                            if ($cobranca > 1) {
                                $cobranca = 100;
                            } else {
                                $cobranca *= 100;
                            }

                            $resto = $loja['limite'] - $cobranca;

                            if ($resto  < 0) {
                                $loja['limite'] = 0;
                                $loja['mcps'] += $resto;
                            } else {
                                $loja['limite'] -= $cobranca;
                            }



                            if ($loja['mcps'] <= 0 && $loja['limite'] <= 0) {
                                $status = $this->shopify->get_checkout_info($loja['shopify_nome'], $loja['token']);
                                $status = json_decode($status, true);

                                //checkout script
                                $existe = false;
                                foreach ($status['script_tags'] as $tag) {
                                    if ($tag['id'] == $loja['script_tag']) {
                                        $existe = true;
                                        break;
                                    }
                                }

                                // vacina
                                $existe_vacina = false;
                                foreach ($status['script_tags'] as $tag) {
                                    if ($tag['id'] == $loja['vacina_cartx']) {
                                        $existe_vacina = true;
                                        break;
                                    }
                                }

                                // form
                                $existe_form = false;
                                foreach ($status['script_tags'] as $tag) {
                                    if ($tag['id'] == $loja['form_tag']) {
                                        $existe_form = true;
                                        break;
                                    }
                                }

                                if ($existe) {
                                    $this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['script_tag']);
                                    $loja['script_tag'] = 0;
                                }

                                if ($existe_vacina) {
                                    $this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['vacina_cartx']);
                                    $loja['vacina_cartx'] = 0;
                                }

                                if ($existe_form) {
                                    $this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['form_tag']);
                                    $loja['form_tag'] = 0;
                                }
                            }

                            $this->Loja_model->update_loja($loja['id'], $loja);

                            $this->shopify->update_order_transaction($loja['shopify_nome'], $loja['token'], $transaction, $referencia_externa);

                            $this->load->library("sendemail");

                            $order = $this->Loja_model->get_order_by_id($resposta_parsed['external_reference']);

                            $dados['nome'] = $order['nome_comprador']; /**/
                            $dados['email'] = $order['email_comprador'];/**/
                            $dados['pedido'] = $order['order_name']; /**/
                            $dados['nome_loja'] = $loja['nome_loja']; /**/
                            $dados['order_details_link'] = $order['order_status_url']; /**/

                            $this->sendemail->pagamento_aprovado($dados, true);
                        }
                    }

                    $result = $client->deleteMessage([
                        'QueueUrl' => $this->config->item('sqs_pagamentos_mp_url'), // REQUIRED
                        'ReceiptHandle' => $msg['ReceiptHandle'] // REQUIRED
                    ]);
                }
            }
        } catch (AwsException $e) {
            // output error message if fails
            error_log($e->getMessage());
        }
    }

    public function recuperar_carrinho_estagio_1()
    {

        $this->load->model("Carrinhos_model", "", true);
        $this->load->model("Loja_model", "", true);
        $this->load->model("Descontos_model", "", true);
        $this->load->library("sendemail");
        $this->load->helper('date');

        $lojas_recuperacao = $this->Carrinhos_model->get_lojas_ativas_recuperacao();


        foreach ($lojas_recuperacao as $loja) {
            if ($loja['estagio_1_ativo'] == 0) {
                continue;
            }

            $cupom = $this->Descontos_model->get_desconto_by_name($loja['loja_id'], $loja['estagio_1_cupom']);
            $dados_loja = $this->Loja_model->get_loja_shopify_by_id($loja['loja_id']);

            if ($dados_loja['mcps'] < 1 && $dados_loja['limite'] < 1 ) {
                continue;
            }

            $enviar_email['contato_loja'] = $dados_loja['email_contato'];
            $enviar_email['nome_loja'] = $dados_loja['nome_loja'];
            $enviar_email['assunto'] = $loja['estagio_1_assunto'];

            $timer = date('Y-m-d H:i:s', now("UCT") - $loja['estagio_1_timer']);

            $carrinhos_elegiveis = $this->Carrinhos_model->get_carrinhos_elegiveis($loja['loja_id'], 1, $timer);

            if ($carrinhos_elegiveis != false) {
                foreach ($carrinhos_elegiveis as $carrinho) {
                    if (empty($carrinho['email'])) {
                        $this->Carrinhos_model->deleta_carrinho($carrinho['id']);
                        continue;
                    }

                    $cart = json_decode($carrinho['cart_json'], true);

                    if (count($cart['items']) > 0) {

                        $lista_produtos = "<ul>";
                        foreach ($cart['items'] as $items) {
                            $lista_produtos .= "<li>" . $items['title'] . "</li>";
                        }
                        $lista_produtos .= "</ul>";

                        $valor_total = $cart['original_total_price'] / 100;
                        $valor_total =  "R$ " . number_format($valor_total, 2, ',', '.');

                        $valor_cupom = "Sem cupom";
                        if ($cupom != false) {
                            switch ($cupom['tipo']) {
                                case "produtos":
                                    if ($cupom['modalidade'] == "porcentagem") {
                                        $valor_cupom = $cupom['valor'] . "%";
                                    } else {
                                        $valor_cupom = ($cupom['valor'] / 100);
                                        $valor_cupom = "R$ " . number_format($valor_cupom, 2, ',', '.');
                                    }
                                    $valor_cupom .= " off nos produtos";
                                    break;
                                case "frete":
                                    if ($cupom['modalidade'] == "porcentagem") {
                                        $valor_cupom = $cupom['valor'] . "%";
                                    } else {
                                        $valor_cupom = ($cupom['valor'] / 100);
                                        $valor_cupom = "R$ " . number_format($valor_cupom, 2, ',', '.');
                                    }
                                    $valor_cupom .= " off no frete";
                                    break;
                                case "total":
                                    if ($cupom['modalidade'] == "porcentagem") {
                                        $valor_cupom = $cupom['valor'] . "%";
                                    } else {
                                        $valor_cupom = ($cupom['valor'] / 100);
                                        $valor_cupom = "R$ " . number_format($valor_cupom, 2, ',', '.');
                                    }
                                    $valor_cupom .= " off";
                                    break;
                            }
                        }

                        $nome = explode(" ", $carrinho['nome']);
                        $primeiro_nome = $nome[0];
                        $ultimo_nome = end($nome);

                        $link_checkout = "<a target='_blank' href='https://" . $dados_loja['dominio_proprio'] . "/apps/checkout/" . $carrinho['cart_token'] . "/1' >Seu carrinho</a> ";

                        $corpo_email = $loja['estagio_1_email'];
                        $corpo_email = str_ireplace("{{PRODUTOS}}", $lista_produtos, $corpo_email);
                        $corpo_email = str_ireplace("{{VALOR_ATUAL_TOTAL}}", $valor_total, $corpo_email);
                        $corpo_email = str_ireplace("{{VALOR_CUPOM}}", $valor_cupom, $corpo_email);
                        $corpo_email = str_ireplace("{{PRIMEIRO_NOME}}", $primeiro_nome, $corpo_email);
                        $corpo_email = str_ireplace("{{ULTIMO_NOME}}", $ultimo_nome, $corpo_email);
                        $corpo_email = str_ireplace("{{LINK_CHECKOUT}}", $link_checkout, $corpo_email);

                        $enviar_email['corpo'] = $corpo_email;
                        $enviar_email['email_cliente'] = $carrinho['email'];
                        $enviar_email['nome_cliente'] = $primeiro_nome . " " . $ultimo_nome;

                        $this->sendemail->recuperacao_carrinho($enviar_email, false);
                        $val['estagio1'] = 1;
                        $this->Carrinhos_model->update_carrinho($carrinho['id'], $val);

                        $cobranca = 1;
                        $resto = $dados_loja['limite'] - $cobranca;

                        if ($resto  < 0) {
                            $dados_loja['limite'] = 0;
                            $dados_loja['mcps'] += $resto;
                        } else {
                            $dados_loja['limite'] -= $cobranca;
                        }

                        $this->Loja_model->update_loja($dados_loja['id'], $dados_loja);

                    } else {
                        $this->Carrinhos_model->deleta_carrinho($carrinho['id']);
                        continue;
                    }
                }
            }
        }
    }

    public function recuperar_carrinho_estagio_2()
    {

        $this->load->model("Carrinhos_model", "", true);
        $this->load->model("Loja_model", "", true);
        $this->load->model("Descontos_model", "", true);
        $this->load->library("sendemail");
        $this->load->helper('date');

        $lojas_recuperacao = $this->Carrinhos_model->get_lojas_ativas_recuperacao();

        foreach ($lojas_recuperacao as $loja) {

            if ($loja['estagio_2_ativo'] == 0) {
                continue;
            }

            $cupom = $this->Descontos_model->get_desconto_by_name($loja['loja_id'], $loja['estagio_2_cupom']);
            $dados_loja = $this->Loja_model->get_loja_shopify_by_id($loja['loja_id']);

            $enviar_email['contato_loja'] = $dados_loja['email_contato'];
            $enviar_email['nome_loja'] = $dados_loja['nome_loja'];
            $enviar_email['assunto'] = $loja['estagio_2_assunto'];

            $timer = date('Y-m-d H:i:s', now("UCT") - $loja['estagio_2_timer']);

            $carrinhos_elegiveis = $this->Carrinhos_model->get_carrinhos_elegiveis($loja['loja_id'], 2, $timer);

            if ($carrinhos_elegiveis != false) {
                foreach ($carrinhos_elegiveis as $carrinho) {
                    if (empty($carrinho['email'])) {
                        $this->Carrinhos_model->deleta_carrinho($carrinho['id']);
                        continue;
                    }

                    $cart = json_decode($carrinho['cart_json'], true);

                    if (count($cart['items']) > 0) {

                        $lista_produtos = "<ul>";
                        foreach ($cart['items'] as $items) {
                            $lista_produtos .= "<li>" . $items['title'] . "</li>";
                        }
                        $lista_produtos .= "</ul>";

                        $valor_total = $cart['original_total_price'] / 100;
                        $valor_total =  "R$ " . number_format($valor_total, 2, ',', '.');

                        $valor_cupom = "Sem cupom";
                        if ($cupom != false) {
                            switch ($cupom['tipo']) {
                                case "produtos":
                                    if ($cupom['modalidade'] == "porcentagem") {
                                        $valor_cupom = $cupom['valor'] . "%";
                                    } else {
                                        $valor_cupom = ($cupom['valor'] / 100);
                                        $valor_cupom = "R$ " . number_format($valor_cupom, 2, ',', '.');
                                    }
                                    $valor_cupom .= " off nos produtos";
                                    break;
                                case "frete":
                                    if ($cupom['modalidade'] == "porcentagem") {
                                        $valor_cupom = $cupom['valor'] . "%";
                                    } else {
                                        $valor_cupom = ($cupom['valor'] / 100);
                                        $valor_cupom = "R$ " . number_format($valor_cupom, 2, ',', '.');
                                    }
                                    $valor_cupom .= " off no frete";
                                    break;
                                case "total":
                                    if ($cupom['modalidade'] == "porcentagem") {
                                        $valor_cupom = $cupom['valor'] . "%";
                                    } else {
                                        $valor_cupom = ($cupom['valor'] / 100);
                                        $valor_cupom = "R$ " . number_format($valor_cupom, 2, ',', '.');
                                    }
                                    $valor_cupom .= " off";
                                    break;
                            }
                        }

                        $nome = explode(" ", $carrinho['nome']);
                        $primeiro_nome = $nome[0];
                        $ultimo_nome = end($nome);

                        $link_checkout = "<a target='_blank' href='https://" . $dados_loja['dominio_proprio'] . "/apps/checkout/" . $carrinho['cart_token'] . "/2' >Seu carrinho</a> ";

                        $corpo_email = $loja['estagio_2_email'];
                        $corpo_email = str_ireplace("{{PRODUTOS}}", $lista_produtos, $corpo_email);
                        $corpo_email = str_ireplace("{{VALOR_ATUAL_TOTAL}}", $valor_total, $corpo_email);
                        $corpo_email = str_ireplace("{{VALOR_CUPOM}}", $valor_cupom, $corpo_email);
                        $corpo_email = str_ireplace("{{PRIMEIRO_NOME}}", $primeiro_nome, $corpo_email);
                        $corpo_email = str_ireplace("{{ULTIMO_NOME}}", $ultimo_nome, $corpo_email);
                        $corpo_email = str_ireplace("{{LINK_CHECKOUT}}", $link_checkout, $corpo_email);

                        $enviar_email['corpo'] = $corpo_email;
                        $enviar_email['email_cliente'] = $carrinho['email'];
                        $enviar_email['nome_cliente'] = $primeiro_nome . " " . $ultimo_nome;

                        $this->sendemail->recuperacao_carrinho($enviar_email, false);
                        $val['estagio2'] = 1;
                        $this->Carrinhos_model->update_carrinho($carrinho['id'], $val);

                        $cobranca = 1;
                        $resto = $dados_loja['limite'] - $cobranca;

                        if ($resto  < 0) {
                            $dados_loja['limite'] = 0;
                            $dados_loja['mcps'] += $resto;
                        } else {
                            $dados_loja['limite'] -= $cobranca;
                        }

                        $this->Loja_model->update_loja($dados_loja['id'], $dados_loja);

                    } else {
                        $this->Carrinhos_model->deleta_carrinho($carrinho['id']);
                        continue;
                    }
                }
            }
        }
    }

    public function recuperar_carrinho_estagio_3()
    {

        $this->load->model("Carrinhos_model", "", true);
        $this->load->model("Loja_model", "", true);
        $this->load->model("Descontos_model", "", true);
        $this->load->library("sendemail");
        $this->load->helper('date');

        $lojas_recuperacao = $this->Carrinhos_model->get_lojas_ativas_recuperacao();

        foreach ($lojas_recuperacao as $loja) {

            if ($loja['estagio_3_ativo'] == 0) {
                continue;
            }

            $cupom = $this->Descontos_model->get_desconto_by_name($loja['loja_id'], $loja['estagio_3_cupom']);
            $dados_loja = $this->Loja_model->get_loja_shopify_by_id($loja['loja_id']);

            $enviar_email['contato_loja'] = $dados_loja['email_contato'];
            $enviar_email['nome_loja'] = $dados_loja['nome_loja'];
            $enviar_email['assunto'] = $loja['estagio_3_assunto'];

            $timer = date('Y-m-d H:i:s', now("UCT") - $loja['estagio_3_timer']);

            $carrinhos_elegiveis = $this->Carrinhos_model->get_carrinhos_elegiveis($loja['loja_id'], 3, $timer);

            if ($carrinhos_elegiveis != false) {
                foreach ($carrinhos_elegiveis as $carrinho) {
                    if (empty($carrinho['email'])) {
                        $this->Carrinhos_model->deleta_carrinho($carrinho['id']);
                        continue;
                    }

                    $cart = json_decode($carrinho['cart_json'], true);

                    if (count($cart['items']) > 0) {

                        $lista_produtos = "<ul>";
                        foreach ($cart['items'] as $items) {
                            $lista_produtos .= "<li>" . $items['title'] . "</li>";
                        }
                        $lista_produtos .= "</ul>";

                        $valor_total = $cart['original_total_price'] / 100;
                        $valor_total =  "R$ " . number_format($valor_total, 2, ',', '.');

                        $valor_cupom = "Sem cupom";
                        if ($cupom != false) {
                            switch ($cupom['tipo']) {
                                case "produtos":
                                    if ($cupom['modalidade'] == "porcentagem") {
                                        $valor_cupom = $cupom['valor'] . "%";
                                    } else {
                                        $valor_cupom = ($cupom['valor'] / 100);
                                        $valor_cupom = "R$ " . number_format($valor_cupom, 2, ',', '.');
                                    }
                                    $valor_cupom .= " off nos produtos";
                                    break;
                                case "frete":
                                    if ($cupom['modalidade'] == "porcentagem") {
                                        $valor_cupom = $cupom['valor'] . "%";
                                    } else {
                                        $valor_cupom = ($cupom['valor'] / 100);
                                        $valor_cupom = "R$ " . number_format($valor_cupom, 2, ',', '.');
                                    }
                                    $valor_cupom .= " off no frete";
                                    break;
                                case "total":
                                    if ($cupom['modalidade'] == "porcentagem") {
                                        $valor_cupom = $cupom['valor'] . "%";
                                    } else {
                                        $valor_cupom = ($cupom['valor'] / 100);
                                        $valor_cupom = "R$ " . number_format($valor_cupom, 2, ',', '.');
                                    }
                                    $valor_cupom .= " off";
                                    break;
                            }
                        }

                        $nome = explode(" ", $carrinho['nome']);
                        $primeiro_nome = $nome[0];
                        $ultimo_nome = end($nome);

                        $link_checkout = "<a target='_blank' href='https://" . $dados_loja['dominio_proprio'] . "/apps/checkout/" . $carrinho['cart_token'] . "/3' >Seu carrinho</a> ";

                        $corpo_email = $loja['estagio_3_email'];
                        $corpo_email = str_ireplace("{{PRODUTOS}}", $lista_produtos, $corpo_email);
                        $corpo_email = str_ireplace("{{VALOR_ATUAL_TOTAL}}", $valor_total, $corpo_email);
                        $corpo_email = str_ireplace("{{VALOR_CUPOM}}", $valor_cupom, $corpo_email);
                        $corpo_email = str_ireplace("{{PRIMEIRO_NOME}}", $primeiro_nome, $corpo_email);
                        $corpo_email = str_ireplace("{{ULTIMO_NOME}}", $ultimo_nome, $corpo_email);
                        $corpo_email = str_ireplace("{{LINK_CHECKOUT}}", $link_checkout, $corpo_email);

                        $enviar_email['corpo'] = $corpo_email;
                        $enviar_email['email_cliente'] = $carrinho['email'];
                        $enviar_email['nome_cliente'] = $primeiro_nome . " " . $ultimo_nome;

                        $this->sendemail->recuperacao_carrinho($enviar_email, false);
                        $val['estagio3'] = 1;
                        $this->Carrinhos_model->update_carrinho($carrinho['id'], $val);

                        $cobranca = 1;
                        $resto = $dados_loja['limite'] - $cobranca;

                        if ($resto  < 0) {
                            $dados_loja['limite'] = 0;
                            $dados_loja['mcps'] += $resto;
                        } else {
                            $dados_loja['limite'] -= $cobranca;
                        }

                        $this->Loja_model->update_loja($dados_loja['id'], $dados_loja);
                    } else {
                        $this->Carrinhos_model->deleta_carrinho($carrinho['id']);
                        continue;
                    }
                }
            }
        }
    }

    public function checkar_pagamentos()
    {
        $this->load->library("mercadopago");
        $this->load->library("shopify");
        $this->load->model("Loja_model", "", true);

        $horas = (3600 * 7) - (3600 * 24 * 3);
        $date = date('Y-m-d H:i:s', time() + $horas);
        $resposta = $this->Loja_model->get_auto_fail($date);

        foreach ($resposta as $order) {

            $loja_id = $this->Loja_model->get_loja_id_by_mp_id($order['mp_id']);
            $loja = $this->Loja_model->get_loja_shopify_by_id($loja_id);

            $resposta = $this->mercadopago->acessarPagamento($order['mp_id'], $loja['mp_access_token']);
            $resposta_parsed = json_decode($resposta, true);

            $referencia_externa = $resposta_parsed['external_reference'];
            $update = array(
                "status" => $resposta_parsed['status'],
                "status_detail" => $resposta_parsed['status_detail'],
                "payment_status" => $resposta_parsed['status'] == "approved" ? 1 : 0,
                "mp_id" =>  $order['mp_id']
            );

            $this->Loja_model->update_order($referencia_externa, $update);

            if ($resposta_parsed['status'] == "approved") {
                echo "esse aprovou => " . $order['mp_id'] . "<br />";
                $transactions = json_decode($this->shopify->get_transactions($loja['shopify_nome'], $loja['token'], $referencia_externa), true);
                $ultima_transaction = end($transactions["transactions"]);
                $id_ultima_transaction = $ultima_transaction['id'];

                $transaction = array(
                    "transaction" => array(
                        "parent_id" => $id_ultima_transaction,
                        "kind" => "capture",
                        "gateway" => "mercado_pago",
                        "amount" => $resposta_parsed['transaction_amount'],
                        "status" => "success",
                        "currency" => "BRL"
                    )
                );

                $cobranca = $resposta_parsed['transaction_amount'] * 0.01;
                if ($cobranca > 1) {
                    $cobranca = 100;
                } else {
                    $cobranca *= 100;
                }
                $loja['mcps'] -= $cobranca;

                if ($loja['mcps'] < -10) {
                    $status = $this->shopify->get_checkout_info($loja['shopify_nome'], $loja['token']);
                    $status = json_decode($status, true);

                    //checkout script
                    $existe = false;
                    foreach ($status['script_tags'] as $tag) {
                        if ($tag['id'] == $loja['script_tag']) {
                            $existe = true;
                            break;
                        }
                    }

                    // vacina
                    $existe_vacina = false;
                    foreach ($status['script_tags'] as $tag) {
                        if ($tag['id'] == $loja['vacina_cartx']) {
                            $existe_vacina = true;
                            break;
                        }
                    }

                    if ($existe) {
                        $this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['script_tag']);
                        $loja['script_tag'] = 0;
                    }

                    if ($existe_vacina) {
                        $this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['vacina_cartx']);
                        $loja['vacina_cartx'] = 0;
                    }
                }

                $this->Loja_model->update_loja($loja['id'], $loja);

                $this->shopify->update_order_transaction($loja['shopify_nome'], $loja['token'], $transaction, $referencia_externa);

                $this->load->library("sendemail");

                $order = $this->Loja_model->get_order_by_id($resposta_parsed['external_reference']);

                $dados['nome'] = $order['nome_comprador']; /**/
                $dados['email'] = $order['email_comprador'];/**/
                $dados['pedido'] = $order['order_name']; /**/
                $dados['nome_loja'] = $loja['nome_loja']; /**/
                $dados['order_details_link'] = $order['order_status_url']; /**/

                $this->sendemail->pagamento_aprovado($dados, true);
            }
        }
    }

    public function envia_email()
    {
        $this->load->library("sendemail");

        $client = new Aws\Sqs\SqsClient([
            'region' => $this->config->item('aws_region'),
            'version' => 'latest',
            'credentials' => [
                'key'    => $this->config->item('aws_key_id'),
                'secret' => $this->config->item('aws_secret'),
            ]
        ]);

        try {
            while (true) {
                $result = $client->receiveMessage(array(
                    'AttributeNames' => ['SentTimestamp'],
                    'MaxNumberOfMessages' => 1,
                    'MessageAttributeNames' => ['All'],
                    'QueueUrl' => $this->config->item('sqs_email_queue_url'), // REQUIRED
                    'WaitTimeSeconds' => 0,
                ));

                if ($result->get('Messages') !== null && count($result->get('Messages')) > 0) {
                    $msg = $result->get('Messages')[0];
                    $dados = json_decode($msg['Body'], true);
                    $onde = $msg['MessageAttributes']['Title']["StringValue"];

                    switch ($onde) {
                        case "comprou":
                            $this->sendemail->comprou($dados, false);
                            break;
                        case "pagamento_aprovado":
                            $this->sendemail->pagamento_aprovado($dados, false);
                            break;
                        default:
                            break;
                    }

                    $result = $client->deleteMessage([
                        'QueueUrl' => $this->config->item('sqs_email_queue_url'), // REQUIRED
                        'ReceiptHandle' => $msg['ReceiptHandle'] // REQUIRED
                    ]);
                } else {
                    break;
                }
            }
        } catch (AwsException $e) {
            // output error message if fails
            error_log($e->getMessage());
        }
    }
}
