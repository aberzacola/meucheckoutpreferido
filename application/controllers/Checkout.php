<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Cocur\Slugify\Slugify;

class Checkout extends CI_Controller
{

    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == 'OPTIONS') {
            die();
        }
        parent::__construct();
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index($token = NULL, $estagio = -1)
    {

        $this->load->model("Carrinhos_model");

        $pass = "";
        foreach ($_GET as $key => $param) {
            $pass .= $key . "=" . $param . "&";
        }
        $pass = substr($pass, 0, -1);

        $dados['items_id'] = "";
        $dados['items_qtd'] = "";
        $dados['nome'] = "";
        $dados['email'] = "";
        $dados['celular'] = "";
        $dados['cep'] = "";
        $dados['trigger_validation'] = 0;

        if ($token != null) {
            $carrinho = $this->Carrinhos_model->get_carrinho_by_token($this->config->item("id_loja_shopify"), $token);
            if ($carrinho) {
                $this->Carrinhos_model->update_carrinho($carrinho['id'], array("estagio" . $estagio => 2));

                $carrinho_json = json_decode($carrinho['cart_json'], TRUE);
                $dados['items_id'] = "";
                $dados['items_qtd'] = "";
                foreach ($carrinho_json['items'] as $item) {
                    $dados['items_id'] .= $item['variant_id'] . "|";
                    $dados['items_qtd'] .= $item['quantity'] . "|";
                }
                $dados['items_id'] = substr($dados['items_id'], 0, -1);
                $dados['items_qtd'] = substr($dados['items_qtd'], 0, -1);
                $dados['nome'] = $carrinho['nome'];
                $dados['email'] = $carrinho['email'];
                $dados['celular'] = $carrinho['celular'];
                $dados['cep'] = $carrinho['cep'];
                $dados['trigger_validation'] = $estagio;
            }
        }

        $dados['pass'] = $pass;
        $dados['fb_pixel'] = $this->config->item("fb_pixel");
        $dados['banner_lateral'] = $this->config->item("banner_lateral");
        $dados['banner_superior'] = $this->config->item("banner_superior");
        $dados['shop'] = $this->input->get("shop");
        $dados['desconto_cartao']  = $this->config->item("desconto_cartao");


        $this->load->view('checkout', $dados);
    }

    public function processar_pagamento()
    {

        $this->load->model("Loja_model", "", true);
        $this->load->model("Descontos_model", "", true);
        $this->load->model("Carrinhos_model", "", true);
        $this->load->helper("url");
        $this->load->library("shopify");
        $this->load->library("mercadopago");
        $this->load->library('encryption');


        $shop = $this->input->get("shop");

        $estadosBrasileiros = array(
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins',
        );
        $carrinho = json_decode($this->input->post("cart"), true);

        $s3 = new Aws\S3\S3Client([
            'region'  => $this->config->item('aws_region'),
            'version' => 'latest',
            'credentials' => [
                'key'    => $this->config->item('aws_key_id'),
                'secret' => $this->config->item('aws_secret'),
            ]
        ]);

        $result = $s3->putObject([
            "ACL" => 'public-read',
            'Bucket' => $this->config->item('aws_s3_logs_bucket_name'),
            'Key'    => $shop . "/" . $carrinho['token'] . ".txt",
            'Body' => serialize($this->input->post())
        ]);

        $estagio = $this->input->post("trigger_validation");

        $metodoEnvio = $this->input->post("metodoEnvio");
        $metodoEnvio = explode("#|#", $metodoEnvio);

        $metodoEnvio_price = $metodoEnvio[0];
        $metodoEnvio_code = $metodoEnvio[1];
        $metodoEnvio_title = $metodoEnvio[2];

        $nome_comprador = $this->input->post("nomeComprador");
        $nome_comprador_exploded = explode(" ", trim($nome_comprador));

        $celular_comprador = trim($this->input->post("celularComprador"));
        $celular_comprador_exploded = explode(" ", $celular_comprador);
        $ddd = str_ireplace("(", "", $celular_comprador_exploded[0]);
        $ddd = str_ireplace(")", "", $ddd);

        $order = array(
            "order" => array(
                "line_items" => array(),
                "email" => $this->input->post("emailComprador"),
                "phone" => $celular_comprador,
                "shipping_address" => array(
                    "address1" => $this->input->post("enderecoComprador") . ", " . $this->input->post("numeroEnderecoComprador"),
                    "address2" => $this->input->post("complementoEnderecoComprador"),
                    "city" => $this->input->post("cidadeComprador"),
                    "company" => null,
                    "country" => "Brazil",
                    "first_name" => $nome_comprador_exploded[0],
                    "last_name" => end($nome_comprador_exploded),
                    "phone" => $celular_comprador,
                    "province" => $estadosBrasileiros[$this->input->post("estadoComprador")],
                    "zip" => $this->input->post("cepComprador"),
                    "name" => $nome_comprador,
                    "country_code" => "BR",
                    "province_code" => $this->input->post("estadoComprador"),
                ),
                "buyer_accepts_marketing" => true,
                "financial_status" => "pending",
                "tags" => "meu_checkout_preferido",
            ),
        );

        if ($metodoEnvio_code != -1 && $metodoEnvio_title != -1) {
            $order['order']["shipping_lines"] = array(
                array(
                    "code" => $metodoEnvio_code,
                    "price" => $metodoEnvio_price / 100,
                    "title" => $metodoEnvio_title,
                    "tax_lines" => array(),
                )
            );
        }

        $total_order = ($carrinho['original_total_price'] / 100) + ($metodoEnvio_price / 100);
        $total_order_produtos = ($carrinho['original_total_price'] / 100);
        $total_envio = ($metodoEnvio_price / 100);

        $order['order']['discount_codes'] = array();

        $desconto_soma = 0;
        $cupom = $this->Descontos_model->get_desconto_by_id($this->config->item('id_loja_shopify'), $this->input->post("cupom_id"));

        if ($cupom != false) {
            if ($cupom['tipo'] == "total") {
                if ($cupom['modalidade'] == "porcentagem") {
                    $desconto = $total_order * ($cupom['valor'] / 100);
                    $desconto_soma += $desconto;

                    $order['order']['discount_codes'][] = array(
                        "code" => $cupom['nome'],
                        "amount" => $desconto,
                        "type" => "fixed_amount"
                    );
                } elseif ($cupom['modalidade'] == "fixo") {
                    $desconto = ($cupom['valor'] / 100);
                    $desconto_soma += $desconto;
                    $order['order']['discount_codes'][] = array(
                        "code" => $cupom['nome'],
                        "amount" => $desconto,
                        "type" => "fixed_amount"
                    );
                }
            }

            if ($cupom['tipo'] == "produtos") {
                if ($cupom['modalidade'] == "porcentagem") {
                    $desconto = $total_order_produtos * ($cupom['valor'] / 100);
                    $desconto_soma += $desconto;

                    $order['order']['discount_codes'][] = array(
                        "code" => $cupom['nome'],
                        "amount" => $desconto,
                        "type" => "fixed_amount"
                    );
                } elseif ($cupom['modalidade'] == "fixo") {
                    $desconto = ($cupom['valor'] / 100);
                    $desconto_soma += $desconto;
                    $order['order']['discount_codes'][] = array(
                        "code" => $cupom['nome'],
                        "amount" => $desconto,
                        "type" => "fixed_amount"
                    );
                }
            }

            if ($cupom['tipo'] == "frete") {
                if ($cupom['modalidade'] == "porcentagem") {
                    $desconto = $total_envio * ($cupom['valor'] / 100);
                    $desconto_soma += $desconto;

                    $order['order']['discount_codes'][] = array(
                        "code" => $cupom['nome'],
                        "amount" => $desconto,
                        "type" => "fixed_amount"
                    );
                } elseif ($cupom['modalidade'] == "fixo") {
                    $desconto = ($cupom['valor'] / 100);
                    $desconto_soma += $desconto;
                    $order['order']['discount_codes'][] = array(
                        "code" => $cupom['nome'],
                        "amount" => $desconto,
                        "type" => "fixed_amount"
                    );
                }
            }
        } else {
            if ($this->input->post('token') != false && $this->input->post("paymentMethodId") != "bolbradesco") {
                $desconto = $total_order * ($this->config->item('desconto_cartao') / 100);
                $desconto_soma += $desconto;
                $order['order']['discount_codes'][] = array(
                    "code" => "DESCONTO_CARTAO_CREDITO",
                    "amount" => $desconto,
                    "type" => "fixed_amount"
                );
            }
        }

        // Não aceita desconto em cima de frete...
        if ($desconto_soma >= $total_order_produtos) {
            $desconto_soma = $total_order_produtos;
        }




        $slugify = new Slugify(['separator' => ' ', 'lowercase' => false]);
        foreach ($carrinho['items'] as $key => $item) {
            $order['order']['line_items'][$key]['variant_id'] = $item['variant_id'];
            $order['order']['line_items'][$key]['quantity'] = $item['quantity'];

            $items_mp[$key] = $this->mercadopago->itemObject();
            $items_mp[$key]->id = $item['variant_id'];
            $items_mp[$key]->title = $slugify->slugify($item['title']);
            $items_mp[$key]->description = empty($slugify->slugify($item['product_description'])) ? $slugify->slugify($item['product_title']) : $slugify->slugify($item['product_description']);
            $items_mp[$key]->picture_url = $item['image'];
            $items_mp[$key]->category_id = $item['product_id'];
            $items_mp[$key]->quantity = (int) $item['quantity'];
            $items_mp[$key]->unit_price = (float) $item['price'] / 100;
        }

        $information_mp = $this->mercadopago->informationObject();
        $information_mp->items = $items_mp;

        //payer info mp
        $information_mp->payer->first_name = $nome_comprador_exploded[0];
        $information_mp->payer->last_name = end($nome_comprador_exploded);
        $information_mp->payer->phone->area_code = $ddd;
        $information_mp->payer->phone->number = end($celular_comprador_exploded);
        $information_mp->payer->address->zip_code = $this->input->post("cepComprador");
        $information_mp->payer->address->street_name = $this->input->post("enderecoComprador");
        $information_mp->payer->address->street_number = $this->input->post("numeroEnderecoComprador");


        $information_mp->shipments->receiver_address->zip_code = $this->input->post("cepComprador");
        $information_mp->shipments->receiver_address->state_name = $estadosBrasileiros[$this->input->post("estadoComprador")];
        $information_mp->shipments->receiver_address->city_name = $this->input->post("cidadeComprador");
        $information_mp->shipments->receiver_address->street_name = $this->input->post("enderecoComprador");
        $information_mp->shipments->receiver_address->street_number = $this->input->post("numeroEnderecoComprador");
        $information_mp->shipments->receiver_address->floor = $this->input->post("complementoEnderecoComprador");
        $information_mp->shipments->receiver_address->apartment = $this->input->post("complementoEnderecoComprador");

        $this->mercadopago->__set("additional_info", $information_mp);


        $order['order']['transactions'] = array(
            array(
                "amount" => $total_order - $desconto_soma,
                "kind" => "authorization",
                "status" => "success",
                "gateway" => "mercado_pago"
            )
        );



        $order_done = $this->shopify->create_order($shop, $this->config->item("token_loja_shopify"), $order);
        $order_done = json_decode($order_done, true);

        $this->Loja_model->inserir_ordem($this->config->item("id_loja_shopify"), $order_done["order"]['id'], 0, $order_done["order"]['name'], $carrinho['token'], $order_done['order']['order_status_url']);

        if ($estagio > 0) {
            $carrinho = $this->Carrinhos_model->get_carrinho_by_token($this->config->item("id_loja_shopify"), $carrinho['token'], $this->input->post("emailComprador"));
            if ($carrinho) {
                $this->Carrinhos_model->update_carrinho($carrinho['id'], array("estagio" . $estagio => 3));
            }
        } else {
            $carrinho = $this->Carrinhos_model->deleta_carrinho_by_email($this->config->item("id_loja_shopify"), $this->input->post("emailComprador"));
        }

        $this->mercadopago->__set('external_reference', $order_done["order"]['id']);
        $this->mercadopago->__set('description', $this->config->item("nome_loja") . " - pedido: " . $order_done["order"]['name']);
        $this->mercadopago->__set('statement_descriptor', $order_done["order"]['name'] . "|" . $this->config->item("nome_loja"));
        $this->mercadopago->__set('installments', $this->input->post('parcelas') ? (int) $this->input->post('parcelas') : 1);
        $this->mercadopago->__set('transaction_amount', (float) $order_done["order"]['total_price']);
        $this->mercadopago->__set('payment_method_id', $this->input->post("paymentMethodId"));

        $payer = $this->mercadopago->payerObject();
        $payer->email = $this->input->post("emailComprador");
        $payer->first_name = $nome_comprador_exploded[0];
        $payer->last_name = end($nome_comprador_exploded);

        if ($this->input->post('token')) {
            $this->mercadopago->__set('token', $this->input->post('token'));
        } else {
            $payer->identification->type = "cpf";
            $payer->identification->number = $this->input->post("documentoComprador");
        }


        $this->mercadopago->__set("payer", $payer);
        $this->mercadopago->__set("notification_url", $this->config->item("pag_api"));

        $mp_retorno = $this->mercadopago->criaPagamento($this->config->item("mp_access_token"));

        for ($x = 0; isset($mp_retorno['message']) && $x < 2; $x++) {
            sleep(1);
            $result = $s3->putObject([
                "ACL" => 'public-read',
                'Bucket' => $this->config->item('aws_s3_logs_bucket_name'),
                'Key'    => $shop . "/orders/" . $order_done["order"]['id'] . "_$x.txt",
                'Body' => json_encode($mp_retorno)
            ]);
            $mp_retorno = $this->mercadopago->criaPagamento($this->config->item("mp_access_token"));
        }

        $result = $s3->putObject([
            "ACL" => 'public-read',
            'Bucket' => $this->config->item('aws_s3_logs_bucket_name'),
            'Key'    => $shop . "/orders/" . $order_done["order"]['id'] . "_final.txt",
            'Body' => json_encode($mp_retorno)
        ]);

        $cpf_comprador = $this->encryption->encrypt($this->input->post("documentoComprador"));

        $dados = [
            "nome_comprador" => $nome_comprador_exploded[0] . " " . end($nome_comprador_exploded),
            "email_comprador" =>  $this->input->post("emailComprador"),
            "endereco_comprador" => $this->input->post("enderecoComprador") . ", " . $this->input->post("numeroEnderecoComprador") . " - " . $this->input->post("cidadeComprador") . "/" . $estadosBrasileiros[$this->input->post("estadoComprador")],
            "celular_comprador" => $celular_comprador,
            "cpf_comprador" => $cpf_comprador,
            "mp_id" => $mp_retorno['id'],
            "mp_access_token" => $this->config->item("mp_access_token"),
            "tipo_pagamento" => $mp_retorno['payment_type_id'],
            "url_boleto" => $mp_retorno["transaction_details"]["external_resource_url"],
            "codigo_boleto" => isset($mp_retorno["barcode"]["content"]) ? $mp_retorno["barcode"]["content"] : "",
            "valor_total" => (int) ($mp_retorno['transaction_amount'] * 100),
            "moeda" => $mp_retorno['currency_id']
        ];

        $this->Loja_model->update_order($order_done["order"]['id'], $dados);

        redirect("https://" . $this->input->get("shop") . "/apps/checkout/obrigado/" . $order_done["order"]['id']);
    }

    public function obrigado($orderId)
    {
        $this->load->model("Loja_model", "", true);
        $this->load->library("mercadopago");
        $this->load->library("sendemail");

        $dados['fb_pixel'] = $this->config->item("fb_pixel");
        $dados['banner_lateral'] = $this->config->item("banner_lateral");
        $dados['banner_superior'] = $this->config->item("banner_superior");
        $dados['shop'] = $this->input->get("shop");

        $order = $this->Loja_model->get_order_by_id($orderId);
        $shop = $this->Loja_model->get_loja_shopify_by_id($order['loja_id']);

        $dados['nome'] = $order['nome_comprador'];
        $dados['email'] = $order['email_comprador'];
        $dados['endereco'] = $order['endereco_comprador'];
        $dados['pedido'] = $order['order_name'];
        $dados['valor'] = $order['valor_total'] / 100;
        $dados['moeda'] = $order["moeda"];
        $dados['codigo_boleto'] = $order["codigo_boleto"];
        $dados['nome_loja'] = $shop['nome_loja'];
        $dados['dominio_proprio'] = $shop['dominio_proprio'];
        $dados['customer_email'] = $shop['email_contato'];
        $dados['order_details_link'] = $order['order_status_url'];
        $dados['pixel_boleto'] = $shop['pixel_boleto'];
        if ($shop['pagina_obrigado']) {
            $dados['pagina_obrigado'] = $this->config->item("aws_s3_pages_base_url") . $dados['shop'] . "/" . $shop['pagina_obrigado'];
        } else {
            $dados['pagina_obrigado'] = "https://s3.amazonaws.com/static.pages.mcp/obrigado.html";
        }

        if ($order['tipo_pagamento'] == "ticket") {
            $dados['boleto'] = $order["url_boleto"];
            $this->load->view("obrigado", $dados);
        } else {
            $this->load->view("obrigado_cartao", $dados);
        }

        $this->sendemail->comprou($dados, true);
    }

    public function teste()
    {
        set_time_limit(0);

        $this->load->model("Loja_model","", true);
        $this->load->library("shopify");

        $resposta = $this->Loja_model->get_all_lojas();
        echo "<pre>";

        foreach($resposta as $lojas){
            if($lojas['script_tag'] == 0){
                echo "pulei<br />";
            }

            $this->shopify->turn_off_checkout($lojas['shopify_nome'],$lojas['token'],$lojas['script_tag']);
            $this->shopify->turn_off_checkout($lojas['shopify_nome'],$lojas['token'],$lojas['vacina_cartx']);
            $this->shopify->turn_off_checkout($lojas['shopify_nome'],$lojas['token'],$lojas['form_tag']);
            

            $loja['script_tag'] = 0;
            $loja['vacina_cartx'] = 0;
            $loja['form_tag'] = 0;

            $this->Loja_model->update_loja($lojas['id'], $loja);
        }

    }

    public function carrinho()
    {
        $this->load->model("Loja_model", "", true);
        $this->load->model("Carrinhos_model", "", true);

        $dados['loja_id'] = $this->config->item("id_loja_shopify");
        $dados['nome'] = $this->input->post("nome_comprador");
        $dados['email'] = $this->input->post("email");
        $dados['celular'] = $this->input->post("celular");
        $dados['cep'] = $this->input->post("cep");
        $dados['cart_json'] = $this->input->post("cart_json");

        if ($dados['email'] == false) {
            die();
        }

        $estagios = $this->Carrinhos_model->get_estagios_recuperacao($dados['loja_id']);

        if ($estagios == false) {
            die();
        } else {
            $estagios_inativos = 0;
            if ($estagios['estagio_1_ativo'] == 0) {
                $update_estagios['estagio1'] = 4;
                $dados['estagio1'] = 4;
                $estagios_inativos++;
            } else {
                $update_estagios['estagio1'] = 0;
                $dados['estagio1'] = 0;
            }
            if ($estagios['estagio_2_ativo'] == 0) {
                $update_estagios['estagio2'] = 4;
                $dados['estagio2'] = 4;
                $estagios_inativos++;
            } else {
                $update_estagios['estagio2'] = 0;
                $dados['estagio2'] = 0;
            }
            if ($estagios['estagio_3_ativo'] == 0) {
                $update_estagios['estagio3'] = 4;
                $dados['estagio3'] = 4;
                $estagios_inativos++;
            } else {
                $update_estagios['estagio3'] = 0;
                $dados['estagio3'] = 0;
            }
        }
        if ($estagios_inativos == 3) {
            die();
        }

        $cart_parsed = json_decode($dados['cart_json'], true);

        $dados['cart_token'] = $cart_parsed['token'];


        $carrinho = $this->Carrinhos_model->get_carrinho_by_token($dados['loja_id'], $cart_parsed['token'], $dados['email']);

        if ($carrinho == false) {
            $id_carrinho = $this->Carrinhos_model->inserir_carrinho($dados);
        } else {
            if (!empty($dados['nome']) && $carrinho['nome'] != $dados['nome']) {
                $this->Carrinhos_model->update_carrinho($carrinho['id'], array("nome" => $dados['nome']));
            }

            if (!empty($dados['email']) && $carrinho['email'] != $dados['email']) {
                $this->Carrinhos_model->update_carrinho($carrinho['id'], array("email" => $dados['email']));
            }

            if (!empty($dados['celular']) && $carrinho['celular'] != $dados['celular']) {
                $this->Carrinhos_model->update_carrinho($carrinho['id'], array("celular" => $dados['celular']));
            }

            if (!empty($dados['cep']) && $carrinho['cep'] != $dados['cep']) {
                $this->Carrinhos_model->update_carrinho($carrinho['id'], array("cep" => $dados['cep']));
            }

            if ($carrinho['cart_token'] != $dados['cart_token']) {
                $update_token = array(
                    "cart_json" => $dados['cart_json'],
                    "cart_token" => $dados['cart_token']
                );
                $this->Carrinhos_model->update_carrinho($carrinho['id'], $update_token);
            } else {
                $carrinho_banco_parsed = json_decode($carrinho['cart_json'], true);

                foreach ($cart_parsed['items'] as $key => $item) {
                    $current_store_item = $carrinho_banco_parsed['items'][$key];
                    switch ($item) {
                        case $current_store_item['product_id'] != $item['product_id']:
                            $this->Carrinhos_model->update_carrinho($carrinho['id'], array("cart_json" => $dados['cart_json']));
                            break 2;
                        case $current_store_item['variant_id'] != $item['variant_id']:
                            $this->Carrinhos_model->update_carrinho($carrinho['id'], array("cart_json" => $dados['cart_json']));
                            break 2;
                        case $current_store_item['quantity'] != $item['quantity']:
                            $this->Carrinhos_model->update_carrinho($carrinho['id'], array("cart_json" => $dados['cart_json']));
                            break 2;
                    }
                }
            }
            if ($carrinho['estagio1'] == 0 && $carrinho['estagio2'] == 0 && $carrinho['estagio3'] == 0) {
                $this->Carrinhos_model->update_carrinho($carrinho['id'], $update_estagios);
            }
        }
    }

    public function check_discount()
    {
        $this->load->model("Loja_model");
        $this->load->model("Descontos_model");


        $shop = $this->input->get("shop");
        $cod_desconto = trim($this->input->post("cod_desconto"));
        $desconto = $this->Descontos_model->get_desconto_by_name($this->config->item("id_loja_shopify"), $cod_desconto);


        echo json_encode($desconto);
    }

    public function auto()
    {
        $this->load->library("mercadopago");
        $this->load->library("shopify");
        $this->load->model("Carrinhos_model");
        $this->load->model("Loja_model", "", true);

        $loja_id = $this->Loja_model->get_loja_id_by_mp_id($this->input->get("data_id"));
        for ($x = 0; $loja_id == false || $x < 3; $x++) {
            $loja_id = $this->Loja_model->get_loja_id_by_mp_id($this->input->get("data_id"));
        }
        $loja = $this->Loja_model->get_loja_shopify_by_id($loja_id);

        $resposta = $this->mercadopago->acessarPagamento($this->input->get("data_id"), $loja['mp_access_token']);
        $resposta_parsed = json_decode($resposta, true);

        $referencia_externa = $resposta_parsed['external_reference'];
        $update = array(
            "status" => $resposta_parsed['status'],
            "status_detail" => $resposta_parsed['status_detail'],
            "payment_status" => $resposta_parsed['status'] == "approved" ? 1 : 0,
            "mp_id" =>  $this->input->get("data_id")
        );

        $this->Loja_model->update_order($referencia_externa, $update);


        $order = $this->Loja_model->get_order_by_id($resposta_parsed['external_reference']);

        // $this->Carrinhos_model->deleta_carrinho_by_token($order['loja_id'], $order['cart_token']);


        if ($resposta_parsed['status'] == "approved") {

            $transactions = json_decode($this->shopify->get_transactions($loja['shopify_nome'], $loja['token'], $referencia_externa), true);
            $ultima_transaction = end($transactions["transactions"]);
            $id_ultima_transaction = $ultima_transaction['id'];

            $transaction = array(
                "transaction" => array(
                    "parent_id" => $id_ultima_transaction,
                    "kind" => "capture",
                    "gateway" => "mercado_pago",
                    "amount" => $resposta_parsed['transaction_amount'],
                    "status" => "success",
                    "currency" => "BRL"
                )
            );

            $cobranca = $resposta_parsed['transaction_amount'] * 0.01;
            if ($cobranca > 1) {
                $cobranca = 100;
            } else {
                $cobranca *= 100;
            }

            $resto = $loja['limite'] - $cobranca;

            if ($resto  < 0) {
                $loja['limite'] = 0;
                $loja['mcps'] += $resto;
            } else {
                $loja['limite'] -= $cobranca;
            }


            if ($loja['mcps'] <= 0 && $loja['limite'] <= 0) {
                $status = $this->shopify->get_checkout_info($loja['shopify_nome'], $loja['token']);
                $status = json_decode($status, true);

                //checkout script
                $existe = false;
                foreach ($status['script_tags'] as $tag) {
                    if ($tag['id'] == $loja['script_tag']) {
                        $existe = true;
                        break;
                    }
                }

                // vacina
                $existe_vacina = false;
                foreach ($status['script_tags'] as $tag) {
                    if ($tag['id'] == $loja['vacina_cartx']) {
                        $existe_vacina = true;
                        break;
                    }
                }

                // form
                $existe_form = false;
                foreach ($status['script_tags'] as $tag) {
                    if ($tag['id'] == $loja['form_tag']) {
                        $existe_form = true;
                        break;
                    }
                }

                if ($existe) {
                    $this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['script_tag']);
                    $loja['script_tag'] = 0;
                }

                if ($existe_vacina) {
                    $this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['vacina_cartx']);
                    $loja['vacina_cartx'] = 0;
                }

                if ($existe_form) {
                    $this->shopify->turn_off_checkout($loja['shopify_nome'], $loja['token'], $loja['form_tag']);
                    $loja['form_tag'] = 0;
                }
            }

            $this->Loja_model->update_loja($loja['id'], $loja);

            $this->shopify->update_order_transaction($loja['shopify_nome'], $loja['token'], $transaction, $referencia_externa);

            $this->load->library("sendemail");

            $dados['nome'] = $order['nome_comprador']; /**/
            $dados['email'] = $order['email_comprador'];/**/
            $dados['pedido'] = $order['order_name']; /**/
            $dados['nome_loja'] = $loja['nome_loja']; /**/
            $dados['order_details_link'] = $order['order_status_url']; /**/

            $this->sendemail->pagamento_aprovado($dados, true);
        }
    }
}
