<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Cocur\Slugify\Slugify;

class Front extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('front/index');
	}

	public function verifica_afiliado($cod_afiliado)
	{
		$this->load->model("Loja_model", "", true);

		$existe = $this->Loja_model->get_loja_by_cod_afiliado($cod_afiliado);

		$resposta['exists'] = false;
		if($existe){
			$resposta['exists'] = true;
		}

		echo json_encode($resposta);
	}

	public function envia_email()
	{
		$dados['email_contato'] = $this->input->post("email");
		$dados['nome'] = $this->input->post("name");
		$dados['msg'] = "Contato do site:<br />" .
			"Telefone: " . $this->input->post("phone") . "<br />" .
			"Plano: " . $this->input->post("select") . "<br />";
		$dados['msg'] .= "MSG: " . $this->input->post("message");

		$this->load->library("sendemail");

		$this->sendemail->contato($dados);

		echo "success";
	}
}
