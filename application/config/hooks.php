<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['post_controller_constructor'][] = array(
    'class'    => '',
    'function' => 'isFromShopify',
    'filename' => 'seguranca.php',
    'filepath' => 'hooks',
    'params' => array(  // controle e metodo de bypass
        "checkout" => array(
            "auto" => true,
            "teste" => true,
            "teste2" => true
        ),
        "installer" => array(
            "index" => true,
            "redirect" => true,
            "teste" => true
        ),
        "front" => array(
            "index" => true,
            "verifica_afiliado" => true,
            "envia_email" => true
        ),
        "crons" => array(
            "envia_email" => true,
            "checkar_pagamentos" => true,
            "processar_mp" => true,
            "recuperar_carrinho_estagio_1" => true,
            "recuperar_carrinho_estagio_2" => true,
            "recuperar_carrinho_estagio_3" => true
        ),
        "dashboard" => array(
            "gerar_script_output" => true,
            "auto" => true
        )
    )
);

$hook['post_controller_constructor'][] = array(
    'class'    => '',
    'function' => 'configLoader',
    'filename' => 'config_loader.php',
    'filepath' => 'hooks',
    'params' => array(  // controle e metodo de bypass
        "dashboard" => array(
            "update_loja_config" => true,
            "gerar_script_output" => true,
            "configuracoes" => true,
            "suporte" => true,
            "loja" => true,
            "processar_pagamento" => true,
            "auto" => true,
            "reemitir_boleto" => true,
            "index" => true,
        ),
        "checkout" => array(
            "auto" => true,
            "teste" => true,
            "teste2" => true
        ),
        "installer" => array(
            "index" => true,
            "page" => true,
            "generate_token" => true,
            "redirect" => true,
            "teste" => true
        ),
        "front" => array(
            "index" => true,
            "verifica_afiliado" => true,
            "envia_email" => true
        ),
        "crons" => array(
            "envia_email" => true,
            "checkar_pagamentos" => true,
            "processar_mp" => true,
            "recuperar_carrinho_estagio_1" => true,
            "recuperar_carrinho_estagio_2" => true,
            "recuperar_carrinho_estagio_3" => true
        )
    )
);
