<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mercadopago
{
    private $token;
    private $installments;
    private $transaction_amount;
    private $description = "MCP";
    private $payment_method_id;
    private $payer;
    private $notification_url;
    private $sponsor_id = null;
    private $binary_mode = false;
    private $external_reference;
    private $statement_descriptor = "MCP";
    private $additional_info;

    public function __construct()
    { }
    public function consultar_transacao()
    { }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    public function itemObject()
    { 
        $object = new stdClass();
        $object->id = 0;
        $object->title = "";
        $object->description = "";
        $object->picture_url = "";
        $object->category_id = "";
        $object->quantity = 0;
        $object->unit_price = 0.0;

        return $object;

    }

    public function informationObject()
    {

        $object = new stdClass();
        $object->items = array();
        $object->payer = new stdClass();
        $object->payer->first_name = "";
        $object->payer->last_name = "";
        $object->payer->phone = new stdClass();
        $object->payer->phone->area_code = null;
        $object->payer->phone->number = null;
        $object->payer->address = new stdClass();
        $object->payer->address->zip_code = "";
        $object->payer->address->street_name = "";
        $object->payer->address->street_number = 0;
        $object->payer->registration_date = "";
        $object->shipments = new stdClass();
        $object->shipments->receiver_address = new stdClass();
        $object->shipments->receiver_address->zip_code = "";
        $object->shipments->receiver_address->state_name = "";
        $object->shipments->receiver_address->city_name = "";
        $object->shipments->receiver_address->street_name = "";
        $object->shipments->receiver_address->street_number = 0;
        $object->shipments->receiver_address->floor = "";
        $object->shipments->receiver_address->apartment = "";
        // $object->barcode = new stdClass();
        // $object->barcode->type = "";
        // $object->barcode->content = "";
        // $object->barcode->width = 0;
        // $object->barcode->height = 0;

        return $object;
    }

    public function payerObject()
    {
        $object = new stdClass();
        $object->email = 'hit@gmail.com';
        $object->first_name = 'John';
        $object->last_name = 'Foo';
        $object->identification = new StdClass();
        $object->identification->type = false;
        $object->identification->number = false;

        return $object;
    }

    public function json()
    {
        $vars = get_object_vars($this);

        return json_encode($vars);
    }

    public function acessarPagamento($id_mp, $access_token)
    {
        $CI = &get_instance();
        $url = $CI->config->item('mercado_pago_payments_url') . "/$id_mp?access_token=" . $access_token;
        $status = file_get_contents($url);
        return $status;
    }

    public function criaPagamento($access_token)
    {
        $CI = &get_instance();
        $url = $CI->config->item('mercado_pago_payments_url') . "?access_token=" . $access_token;

        // Configure curl client and execute request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json());
        $result = curl_exec($ch);
        curl_close($ch);

        // Store the access token
        $result = json_decode($result, true);

        return $result;
    }
}
