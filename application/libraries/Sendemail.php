<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Sendemail
{

    private $destinatario;

    public function __construct()
    { }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    public function pega_boleto($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return $response;
        }
    }

    public function recuperacao_carrinho($dados, $sqs = true)
    {

        $CI = &get_instance();

        if($sqs){
            $client = new Aws\Sqs\SqsClient([
                'region' => $CI->config->item('aws_region'),
                'version' => 'latest',
                'credentials' => [
                    'key'    => $CI->config->item('aws_key_id'),
                    'secret' => $CI->config->item('aws_secret'),
                ]
            ]);

            $params = [
                'MessageAttributes' => [
                    "Title" => [
                        'DataType' => "String",
                        'StringValue' => "recuperacao_carrinho"
                    ],
                ],
                'DelaySeconds' => 0,
                'MessageBody' => json_encode($dados),
                'QueueUrl' => $CI->config->item('sqs_email_queue_url')
            ];

            $client->sendMessage($params);
            return true;
        }

        $mail = new PHPMailer(true);

        try {
            //Server settings                                  // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'email-smtp.us-east-1.amazonaws.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   =  $CI->config->item("aws_ses_smtp_user");                // SMTP username
            $mail->Password   = $CI->config->item("aws_ses_smtp_pass");                               // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom("noreply@meucheckoutpreferido.com", $dados['nome_loja']);
            $mail->addReplyTo($dados['contato_loja'], $dados['nome_loja']);
            $mail->addAddress($dados['email_cliente'] , $dados['nome_cliente']);       // Name is optional

            // Content
            $mail->isHTML(true); 
            $mail->CharSet = 'UTF-8';                                 // Set email format to HTML
            $mail->Subject = $dados['assunto'];
            $mail->Body    = $dados['corpo'];

            $mail->send();
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

    public function contato($dados){

        $CI = &get_instance();

        $mail = new PHPMailer(true);

        try {
            //Server settings                                  // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'email-smtp.us-east-1.amazonaws.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   =  $CI->config->item("aws_ses_smtp_user");                // SMTP username
            $mail->Password   = $CI->config->item("aws_ses_smtp_pass");                               // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('contato@meucheckoutpreferido.com' , "contato site");       // Name is optional
            $mail->addAddress('contato@meucheckoutpreferido.com' , "contato site");       // Name is optional
            $mail->addReplyTo($dados['email_contato'], "Contato do site");

            // Content
            $mail->isHTML(true);         
            $mail->CharSet = 'UTF-8';                           // Set email format to HTML
            $mail->Subject = 'Contato do site';
            $mail->Body    = $dados['msg'];

            $mail->send();
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

    public function pagamento_aprovado($dados, $sqs = true)
    {

        $CI = &get_instance();

        $dados['nome_comprador'] = $dados['nome']; /**/
        $dados['order_name'] = $dados['pedido']; /**/
        $dados['nome_loja'] = $dados['nome_loja'];/**/
        $dados['email_cliente'] = $dados['email']; /**/
        $dados['order_details_link'] = $dados['order_details_link']; /**/
        $email = $CI->load->view("emails/pagamento_recebido", $dados, true);

        if($sqs){
            $client = new Aws\Sqs\SqsClient([
                'region' => $CI->config->item('aws_region'),
                'version' => 'latest',
                'credentials' => [
                    'key'    => $CI->config->item('aws_key_id'),
                    'secret' => $CI->config->item('aws_secret'),
                ]
            ]);

            $params = [
                'MessageAttributes' => [
                    "Title" => [
                        'DataType' => "String",
                        'StringValue' => "pagamento_aprovado"
                    ],
                ],
                'DelaySeconds' => 0,
                'MessageBody' => json_encode($dados),
                'QueueUrl' => $CI->config->item('sqs_email_queue_url')
            ];

            $client->sendMessage($params);
            return true;
        }

        $mail = new PHPMailer(true);

        try {
            //Server settings                                  // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'email-smtp.us-east-1.amazonaws.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   =  $CI->config->item("aws_ses_smtp_user");                // SMTP username
            $mail->Password   = $CI->config->item("aws_ses_smtp_pass");                               // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('noreply@meucheckoutpreferido.com', $dados['nome_loja']);
            $mail->addAddress($dados['email_cliente'] , $dados['nome_comprador']);       // Name is optional

            // Content
            $mail->isHTML(true);      
            $mail->CharSet = 'UTF-8';                              // Set email format to HTML
            $mail->Subject = 'Seu pagamento foi confirmado';
            $mail->Body    = $email;

            $mail->send();
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

    public function comprou($dados, $sqs = true)
    {

        $CI = &get_instance();

        $dados['nome_comprador'] = $dados['nome'];
        $dados['order_name'] = $dados['pedido'];
        $dados['nome_loja'] = $dados['nome_loja'];
        $dados['email_contato'] = $dados['customer_email'];
        $dados['email_cliente'] = $dados['email'];
        $dados['order_details_link'] = $dados['order_details_link'];

        if($sqs){
            $client = new Aws\Sqs\SqsClient([
                'region' => $CI->config->item('aws_region'),
                'version' => 'latest',
                'credentials' => [
                    'key'    => $CI->config->item('aws_key_id'),
                    'secret' => $CI->config->item('aws_secret'),
                ]
            ]);

            $params = [
                'MessageAttributes' => [
                    "Title" => [
                        'DataType' => "String",
                        'StringValue' => "comprou"
                    ],
                ],
                'DelaySeconds' => 0,
                'MessageBody' => json_encode($dados),
                'QueueUrl' => $CI->config->item('sqs_email_queue_url')
            ];

            $client->sendMessage($params);
            return true;
        }

        if (isset($dados['boleto'])) {
            $link_boleto = $this->pega_boleto($dados['boleto']);
            $dados['dados_boleto'] = $link_boleto;
        }
        
        $email = $CI->load->view("emails/comprou", $dados, true);

        $mail = new PHPMailer(true);

        try {
            //Server settings                                  // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'email-smtp.us-east-1.amazonaws.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   =  $CI->config->item("aws_ses_smtp_user");                // SMTP username
            $mail->Password   = $CI->config->item("aws_ses_smtp_pass");                               // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('noreply@meucheckoutpreferido.com', $dados['nome_loja']);
            $mail->addAddress($dados['email_cliente'] , $dados['nome_comprador']);       // Name is optional
            $mail->addReplyTo($dados['email_contato'], 'SAC');

            // Attachments
            if (isset($dados['boleto'])) {
                $dados_boleto = $dados['dados_boleto'];
                if($dados_boleto != false){
                    $mail->addStringAttachment($dados_boleto, "boleto.pdf");         // Add attachments
                }
            }

            // Content
            $mail->isHTML(true);         
            $mail->CharSet = 'UTF-8';                           // Set email format to HTML
            $mail->Subject = 'Seu pedido foi confirmado';
            $mail->Body    = $email;

            $mail->send();
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
}
