<?php


function hashSignature($hmac, $params, $shared_secret)
{

    $params = array_diff_key($params, array('signature' => '')); // Remove hmac from params

    ksort($params); // Sort params lexographically
    $computed_hmac = hash_hmac('sha256', urldecode(http_build_query($params, '', '')), $shared_secret);

    return hash_equals($hmac, $computed_hmac);
}

function hashHMAC($hmac, $params, $shared_secret)
{
    $params = array_diff_key($params, array('hmac' => '')); // Remove hmac from params

    ksort($params); // Sort params lexographically
    $computed_hmac = hash_hmac('sha256', http_build_query($params), $shared_secret);

    return hash_equals($hmac, $computed_hmac);
}

function isFromShopify($bypass)
{
    $CI = &get_instance();
    $isShopify = false;

    $shopify_safe =  $CI->config->item('shopify_safe');
    $controller = $CI->router->fetch_class();
    $method = $CI->router->fetch_method();

    if(isset($bypass[$controller][$method]) && $bypass[$controller][$method] === true){
        return;
    }

    if ($shopify_safe == false) {
        return;
    }

    $params = $CI->input->get(); // Retrieve all request parameters
    $hmac = $CI->input->get('hmac');
    $shared_secret =  $CI->config->item('shopify_shared_secret');

    if ($hmac) {
        $isShopify = hashHMAC($hmac, $params, $shared_secret);
    } else {
        $hmac = $CI->input->get('signature');
        $isShopify = hashSignature($hmac, $params, $shared_secret);
    }

    if (!$isShopify) {
        die("Is not from shopify");
    }
}
