<?php

function configLoader($bypass)
{    
    $CI = &get_instance();

    $controller = $CI->router->fetch_class();
    $method = $CI->router->fetch_method();

    if(isset($bypass[$controller][$method]) && $bypass[$controller][$method] === true){
        return;
    }

    $shop = $CI->input->get("shop");

    $CI->load->model("Loja_model", "", true);
    // $shop = 'dropapps.myshopify.com';
    $info_loja = $CI->Loja_model->get_loja_shopify_by_name($shop);

    if ($info_loja['mp_access_token'] == NULL && $controller == "checkout") {
        die("erro no access token mercado livre, corrigir no painel de configuracao");
    }
    if ($info_loja['mp_public_key'] == NULL && $controller == "checkout") {
        die("erro na public key mercado livre, corrigir no painel de configuracao");
    }
    if ($info_loja['token'] == NULL) {
        die("erro na token da loja, reinstale o checkout");
    }

    $fb_pixel = $info_loja['fb_pixel'] != NULL ? $info_loja['fb_pixel'] : false;
    $banner_lateral = $info_loja['banner_lateral'] != NULL ? $info_loja['banner_lateral'] : false;
    $banner_superior = $info_loja['banner_superior'] != NULL ? $info_loja['banner_superior'] : false;
    $desconto_cartao = $info_loja['desconto_cartao'] != NULL ? $info_loja['desconto_cartao'] : 0;
    $nome_loja = $info_loja['nome_loja'] != NULL ? $info_loja['nome_loja'] : $info_loja['shopify_nome'];
    $escassez = $info_loja['escassez'] != NULL ? $info_loja['escassez'] : 0;
    $texto_escassez = $info_loja['texto_escassez'];


    $CI->config->set_item("mp_access_token", $info_loja['mp_access_token']);
    $CI->config->set_item("mp_public_key", $info_loja['mp_public_key']);
    $CI->config->set_item("fb_pixel", $fb_pixel);
    $CI->config->set_item("token_loja_shopify", $info_loja['token']);
    $CI->config->set_item("id_loja_shopify", $info_loja['id']);
    $CI->config->set_item("banner_lateral", $banner_lateral);
    $CI->config->set_item("banner_superior", $banner_superior);
    $CI->config->set_item("desconto_cartao", $desconto_cartao);
    $CI->config->set_item("nome_loja", $nome_loja);
    $CI->config->set_item("escassez", $escassez);
    $CI->config->set_item("texto_escassez", $texto_escassez);
}
