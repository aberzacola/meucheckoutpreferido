<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147970193-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-147970193-1');
    </script>

    <meta charset="UTF-8">
    <title>Checkout Seguro</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="https://s3-sa-east-1.amazonaws.com/assets.sp/css/mc.css">
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', <?= $fb_pixel ?>);
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?= $fb_pixel ?>&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
    <link rel="icon" href="https://s3-sa-east-1.amazonaws.com/assets.sp/imgs/favicon_checkout.ico">
</head>

<body>
    <div class="container">
        <div class="row outerLoader  m-0">
            <div class="col-12" style="left: -25px;">
                <div class="row">
                    <div class="col-12">
                        <div class="pad-out d-flex justify-content-center">
                            <img class="pad" src="https://checkoutfast-images.s3.amazonaws.com/shield.png" />
                            <div class="loader"></div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12 ">
                        <div class="d-flex justify-content-center">
                            <p>Carregando ambiente seguro</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top <?= $this->config->item("escassez") == "0" ? "d-none" : "" ?>">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-xl-7">
                        <p><?= $this->config->item("texto_escassez") ?></p>
                    </div>
                    <div class="col-12 col-sm-6 col-xl-2 tempo ">
                        <div class="row justify-content-center">
                            <div id="etapas">
                                <div class="numero">0</div>
                                <div class="numero">0</div>
                                <div class="numero">0</div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div id="legenda">
                                <div>Horas</div>
                                <div>Minutos</div>
                                <div>Segundos</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xl-3 comprar justify-content-center">
                        <a href="#" id="btn-comprar"><button type="button" class="btn btn-success btn-comprar efeito">Comprar Agora <i class="fa fa-arrow-down" aria-hidden="true"></i></button></a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">

        <?php if ($banner_superior) : ?>
            <div class="<?= $banner_superior ? "" : "d-none"; ?> row">
                <div class="col-md-4 mt-3 col-12">
                    <div class="row">
                        <div class='col-12'>
                            <img class="banners" src='<?= $this->config->item("aws_s3_bucket_url") ?><?= $shop ?>/<?= $banner_superior ?>' />
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="row ">
            <div class='<?= $banner_lateral ? "col-md-8 " : "col-md-12"; ?> col-12'>
                <div class="row carrinho fundo_branco">
                    <div class="col-12 border pb-3 pt-3">
                        <h4 class="mb-4">Finalizar Pedido</h4>
                        <div class="row mb-1">
                            <div class="col-12">
                                <div id="prdcts_model" class="row d-none mb-1 prdcts_cart">
                                    <div class="col-12 box">
                                        <span class="badge badge-light notify-badge">x</span>
                                        <div class="row">
                                            <div class="col-6 col-lg-3 prdct-thumb">
                                                <img class="produto" src="https://checkoutfast-images.s3.amazonaws.com/foto_produto.png">
                                            </div>
                                            <div class="col-6 col-lg-4 prdct-name">
                                                <ul>
                                                    <li><strong>Aguarde...</strong></li>
                                                </ul>
                                            </div>
                                            <div class="col-6 col-lg-3 qtd text-right mt-3">
                                                <p class="text-left"><b>Quantidade:</b></p>
                                                <input type="number" style="text-align: center;" class="form-control prdct_qty" name="quantity" placeholder="Quantity" autocomplete="off" value="1">
                                            </div>
                                            <div class="col-6 col-lg-2 mt-3 text-right">
                                                <p class="text-left"><b>Valor:</b></p>
                                                <span class="prdct-price" >R$ 0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prdcts_list">

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <ul class="list-group list-group-flush mt-3">
                                    <li class="list-group-item">Subtotal <span id="subtotal" class="float-right" data-value="0">R$ 0</span></li>
                                    <li class="list-group-item">Frete <span id="entrega" class="float-right" data-value="0">Preencha os dados de entrega</span></li>
                                    <li class="list-group-item cupom d-none">Cupom <span id="cupom" class="float-right" data-value="0"></span></li>
                                    <li class="list-group-item d-none">Desconto Cartão de Crédito <span id="desconto" class="float-right" data-value="0">R$ 0</span></li>
                                    <li class="list-group-item active">Total <span id="total" class="float-right" data-value="0">R$ 0</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row fundo_branco pt-3 mt-3 border">
                    <div class="col-12">
                        <h4 class="mb-3">Cupom de desconto</h4>
                        <div class="row area_cupom">
                            <div class="col-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="cupomDesconto" name='cupomDesconto' placeholder="Digite seu cupom" required autocomplete="off">
                                    <small class="form-text text-muted <?= $desconto_cartao > 0 ? "" : "d-none" ?>">Ao utilizar um cupom, o desconto automático no cartão será removido</small>
                                </div>
                            </div>
                            <div class="col-4">
                                <button id="aplicar_desconto" type="button" style="width:100%" class="btn btn-primary">Aplicar</button>
                            </div>
                        </div>
                        <div class="cupom_resposta row d-none">
                            <div class="col-12">
                                <div class="alert alert-primary" role="alert">
                                    This is a primary alert—check it out!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form class="" action="/apps/checkout/processar_pagamento" method="post" id="pay" name="pay" autocomplete="off">
                    <input type="text" autocomplete="off" name="lastname_74758209201093747" class="d-none">
                    <div class="row mt-3 ">
                        <div class="col-12 border fundo_branco pt-3 pb-3">
                            <h4>Informações pessoais</h4>
                            <div class="form-group">
                                <label for="nomeComprador">Nome Completo</label>
                                <input type="text" value="<?=$nome?>" class="form-control" id="nomeComprador" name='nomeComprador' placeholder="Digite seu nome completo" required autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="emailComprador">E-mail</label>
                                <input type="email" value="<?=$email?>" class="form-control" id="emailComprador" name='emailComprador' placeholder="Digite seu melhor e-mail" required>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="celularComprador">Celular com DDD</label>
                                        <input type="tel" value="<?=$celular?>" class="form-control" id="celularComprador" name='celularComprador' placeholder="Digite seu celular com DDD" required>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="documentoComprador">CPF</label>
                                        <input type="tel" class="form-control" id="documentoComprador" name='documentoComprador' placeholder="Digite seu CPF" required>
                                    </div>
                                </div>
                            </div>
                            <h4>Dados de entrega</h4>
                            <div class="form-group">
                                <label for="cepComprador">CEP</label>
                                <input type="tel" value="<?=$cep?>" class="form-control" id="cepComprador" name='cepComprador' placeholder="Digite seu CEP" aria-describedby="cepHelpBlock" required>
                                <small id="cepHelpBlock" class="form-text text-muted">
                                    Digite seu cep para continuar.
                                </small>
                            </div>
                            <div class='enderecoCompleto' style="display: none">
                                <div class="form-group">
                                    <label for="enderecoComprador">Endereço</label>
                                    <input type="text" class="form-control" id="enderecoComprador" name='enderecoComprador' placeholder="Digite seu Endereço" required>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="numeroEnderecoComprador">Número</label>
                                            <input type="tel" class="form-control" id="numeroEnderecoComprador" name='numeroEnderecoComprador' placeholder="Numero da sua casa" required>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="complementoEnderecoComprador">Complemento</label>
                                            <input type="text" class="form-control" id="complementoEnderecoComprador" name='complementoEnderecoComprador' placeholder="Complemento do seu endereço">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bairroComprador">Bairro</label>
                                    <input type="text" class="form-control" id="bairroComprador" name='bairroComprador' placeholder="Digite seu Bairro" required>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="cidadeComprador">Cidade</label>
                                            <input type="text" class="form-control" id="cidadeComprador" name='cidadeComprador' placeholder="Digite sua Cidade" required>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="estadoComprador">Estado</label>
                                            <select id="estadoComprador" name='estadoComprador' class="custom-select" required>
                                                <option value="--"> -- </option>
                                                <option value="AC"> AC </option>
                                                <option value="AL"> AL </option>
                                                <option value="AM"> AM </option>
                                                <option value="AP"> AP </option>
                                                <option value="BA"> BA </option>
                                                <option value="CE"> CE </option>
                                                <option value="DF"> DF </option>
                                                <option value="ES"> ES </option>
                                                <option value="GO"> GO </option>
                                                <option value="MA"> MA </option>
                                                <option value="MG"> MG </option>
                                                <option value="MS"> MS </option>
                                                <option value="MT"> MT </option>
                                                <option value="PA"> PA </option>
                                                <option value="PB"> PB </option>
                                                <option value="PE"> PE </option>
                                                <option value="PI"> PI </option>
                                                <option value="PR"> PR </option>
                                                <option value="RJ"> RJ </option>
                                                <option value="RN"> RN </option>
                                                <option value="RO"> RO </option>
                                                <option value="RR"> RR </option>
                                                <option value="RS"> RS </option>
                                                <option value="SC"> SC </option>
                                                <option value="SE"> SE </option>
                                                <option value="SP"> SP </option>
                                                <option value="TO"> TO </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h4>Método de envio</h4>
                                    <div class="alert alert-primary alertaEndereco" role="alert">
                                        Primeiro informe os dados de entrega acima.
                                    </div>
                                    <div class="metodoEnvio" style="display: none" required>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h4>Método de pagamento</h4>
                                </div>
                            </div>
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                                        Cartão
                                        <?php
                                        if ($desconto_cartao > 0) {
                                            echo " <b class='desconto_cartao_alerta'>" . $desconto_cartao . "% OFF</b>";
                                        }
                                        ?>
                                    </a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Boleto</a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="row mt-3 desconto_cartao_alerta <?= $desconto_cartao > 0 ? "" : "d-none" ?>">
                                        <div class="col-12">
                                            <div class="alert alert-success " role="alert">
                                                Pagando com cartão você tem <a href="#" class="alert-link"><?= $desconto_cartao ?>% de desconto</a>.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <div class='card-wrapper'></div>
                                            <div class="form-group">
                                                <label for="cardNumber">Número do cartão</label>
                                                <input type="tel" data-checkout="cardNumber" class="form-control" id="cardNumber" placeholder="Numero do seu cartão" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="cardholderName">Nome que está escrito no cartão</label>
                                                <input type="text" class="form-control" id="cardholderName" data-checkout="cardholderName" placeholder="Nome escrito no cartão de crédito" required>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-md-6">
                                                    <div class="form-group">
                                                        <label for="expiry">Validade</label>
                                                        <input type="tel" class="form-control" id="expiry" name='expiry' placeholder="Validade do seu cartão Ex: 06/23" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    <div class="form-group">
                                                        <label for="securityCode">Código de segurança (CVC)</label>
                                                        <input type="tel" class="form-control" data-checkout="securityCode" id="securityCode" placeholder="Código de segurança" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="parcelas">Parcelas </label>
                                                <div class="alert alert-primary alertParcelas" role="alert">
                                                    Primeiro informe os dados do cartão acima.
                                                </div>
                                                <select name="parcelas" id="parcelas" class="custom-select d-none" disabled>
                                                    <!-- <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option> -->
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <div class="text-container boleto_data">
                                        <p>Clique em "Finalizar Compra" para gerar o seu boleto.</p>
                                        <p>Informações sobre o pagamento via boleto:</p>
                                        <ul>
                                            <li>Valor à vista <b class="checkoutTotal" style="">R$65,00</b></li>
                                            <li>Não pode ser parcelado! Use cartão de crédito para parcelar sua compra;</li>
                                            <li>Prazo de até 2 dias úteis para compensar.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3 mt-3">
                                <div class="col-12">
                                    <button type="submit" class="finalizar-pagamento btn btn-success btn-lg btn-block">Finalizar compra</button>
                                </div>
                                <div class="col-6 offset-3 col-md-4 offset-md-4">
                                    <img src="https://s3-sa-east-1.amazonaws.com/assets.sp/imgs/cadeado.jpg">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                        <p class="text-center">Copyright © Meu Checkout Preferido</p>
                        </div>
                    </div>
                    <select id="docType" data-checkout="docType" class="invisible">
                        <option value="CPF" selected>CPF</option>
                    </select>
                    <input type="hidden" id="docNumber" data-checkout="docNumber" />
                    <input type="hidden" id="paymentMethodId" name="paymentMethodId" />
                    <input type="hidden" id="cart" name="cart" />
                    <input type="hidden" id="desconto_cartao" name="desconto_cartao" value="<?= $desconto_cartao ?>" />
                    <input type="hidden" id="cupom_val" name="cupom_val" />
                    <input type="hidden" id="cupom_metodo" name="cupom_metodo" />
                    <input type="hidden" id="cupom_id" name="cupom_id" value="-1" />
                    <input type="hidden" id="cupom_tipo" name="cupom_tipo" />
                    <input type="hidden" id="escassez" value="<?= $this->config->item("escassez") ?>" />
                    <input type="hidden" id="shop_url" value="<?= $shop ?>" />
                    <input type="hidden" name="trigger_validation" id="trigger_validation" value="<?= $trigger_validation ?>" />
                    <input type="hidden" id="items_id" value="<?= $items_id ?>" />
                    <input type="hidden" id="items_qtd" value="<?= $items_qtd ?>" />
                    <input type="hidden" id="mp_public_key" value="<?= $this->config->item('mp_public_key') ?>" />
                    <input type="hidden" id="cardExpirationYear" data-checkout="cardExpirationYear" onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off />
                    <input type="hidden" id="cardExpirationMonth" data-checkout="cardExpirationMonth" onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off />
                </form>
            </div>
            <?php if ($banner_lateral) : ?>
                <div class='<?= $banner_lateral ? "" : "d-none"; ?> col-md-4 col-0 mt-3'>
                    <img class="banners" src="<?= $this->config->item("aws_s3_bucket_url") ?><?= $shop ?>/<?= $banner_lateral ?>" />
                </div>
            <?php endif; ?>
        </div>
    </div>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://s3-sa-east-1.amazonaws.com/assets.sp/js/jquery.mask.min.js"></script>
<script src="https://s3-sa-east-1.amazonaws.com/assets.sp/js/card.js"></script>
<script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
<script src="https://s3-sa-east-1.amazonaws.com/assets.sp/js/bootstrap-input-spinner.js"></script>
<script src="https://s3-sa-east-1.amazonaws.com/assets.sp/js/mc.js"></script>


</html>