<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MCP</title>
</head>
<body>

<h1>Nosso futuro a partir do dia 20/11/2019</h1>

<p>Olá todos os usuários do MCP, como a maioria de vocês já sabem, o MCP foi suspenso da Shopify e até o dado momento não conseguimos nenhum tipo de acordo com 
    a platafomra Shopify para a regularização do nosso funcionamento, apesar de todos os esforços que o nosso time fez para que chegassemos a um acordo,
    ainda não estamos vendo uma luz no fim do túnel.
</p>
<p>Então gostaria de comunicar que, até dia 20, todos os usuários que já utilizam poderão utilizar normalmente, após esse dia, nossas credênciais serão 
    suspensas por um período indeterminado e acredito que não haverá reversão, tendo isso em vista, após o dia 20, entraremos em contato com cada um dos usuários que ainda 
    tem alguma pendência ou valor dentro do MCP para que possamos deixar tudo resolvido.
</p>
<p>
    Dentro de alguns dias após a suspensão, para que esse trabalho de meses de desenvolvimento feito exclusivamente por mim não seja perdido, disponibilizaremos nosso código fonte 
    em modo open source ( O LINK DO GITHUB ESTARÁ AQUI ), para que a comunidade possa encontar alguma utilidade para esse sistema no ambiente tão crescente que é o e-commerce no brasil.
</p>
<p>
    Att Angelo;
</p>

</body>
</html>
