<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147970193-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-147970193-1');
    </script>

    <meta charset="UTF-8">
    <title>Checkout Seguro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="<?= $this->config->item("base_url") ?>/assets/images/favicon_checkout.ico">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        @media (max-width: 575.98px) {
            .footer {
                position: fixed;
                height: 180px;
                left: 0;
                bottom: 0;
                width: 100%;
                background-color: #97C932;
                color: white;
            }

            a {
                width: 100%
            }

            .texto {
                margin-left: 10px;
                margin-top: 5px;
                font-size: 25px;
            }

            button {
                width: 100%;
                margin-bottom: 10px;
            }
        }

        @media (min-width: 576px) {
            .footer {
                position: fixed;
                height: 60px;
                left: 0;
                bottom: 0;
                width: 100%;
                background-color: #97C932;
                color: white;
            }

            .texto {
                margin-left: 10px;
                margin-top: 5px;
                font-size: 32px;
            }
        }

        input {
            position: absolute;
            left: -9999px;
        }
    </style>

    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', <?= $fb_pixel ?>);
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?= $fb_pixel ?>&ev=PageView&noscript=1" /></noscript>
</head>

<body>
    <iframe src="<?= $pagina_obrigado ?>" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden;"></iframe>
    <div class="footer">
        <div class="container-fluid">
            <div class="row d-none d-sm-block">
                <div class="col-12 col-sm-6">
                    <p class="texto text-left">Obrigado! <a target="_blank" href="<?= $boleto ?>"><button type="button" class="click_boleto btn btn-primary">Baixar Boleto</button> </a><button type="button" onclick="myFunction()" class="btn btn-primary">Copiar Código do Boleto</button><br /></p>
                </div>
            </div>
            <div class="row d-block d-sm-none">
                <div class="col-12">
                    <div class="texto text-center">Obrigado!</div><br />
                    <button type="button" onclick="myFunction()" class="click_boleto btn btn-primary">Copiar Código do Boleto</button><br />

                    <a href="<?= $boleto ?>"><button type="button" class="click_boleto btn btn-primary">Baixar Boleto</button></a>
                </div>
            </div>
        </div>
    </div>
    <input id="codigo_boleto" type="text" value="<?= $codigo_boleto ?>" />
    <input id="pixel_boleto" type="hidden" value="<?= $pixel_boleto ?>" />

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    var pixel_boleto = $("#pixel_boleto").val();
    if (pixel_boleto == "sempre") {
        fbq('track', 'Purchase', {
            value: <?= $valor ?>,
            currency: '<?= $moeda ?>'
        });
    } else if (pixel_boleto == "baixar") {
        $(".click_boleto").on("click", function() {
            fbq('track', 'Purchase', {
                value: <?= $valor ?>,
                currency: '<?= $moeda ?>'
            });
        })
    }


    function myFunction() {
        var copyText = document.getElementById("codigo_boleto");
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        alert("Copiado");
    }
</script>

</html>