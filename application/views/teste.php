<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Checkout Fast</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        img {
            height: 96px;
            width: 96px;
            display: block;
        }

        table {
            width: 100%;
            height: 100%;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row cart">
            <div class="col-12">
                <ul>
                    <li><b>Nome Comprador:</b> <span id="order-nome"></span></li>
                    <li><b>Edereço:</b> <span id="order-endereco"></span></li>
                    <li><b>Celular:</b> <span id="order-celular"></span> <a href="#" target="_blank" id="order-celular_zap">WhatsApp</a></li>
                    <li><b>Email:</b> <span id="order-email"></span></li>
                    <li><b>Ultima Atualizacao:</b> <span id="order-ultima_alteracao"></span></li>
                    <li><b>Tipo Pagamento:</b> <span id="order-tipo_pagamento"></span></li>
                    <li><b>Status:</b> <span id="order-status_pagamento"></span></li>
                    <li><b>Detalhes Pagamento:</b> <span id="order-detalhes_status_pagamento"></span></li>
                    <li><b>Url Boleto:</b> <a href="#" id="order-url_boleto"></a></li>
                    <li><b>Cod. Boleto:</b> <span id="order-cod_boleto"></span></li>
                    <li><b>Valor:</b><span id="order-valor"></span></li>
                    <li>
                        <b>Produtos:</b>
                        <span id="order-produtos">
                            <ul>1
                                <li><b>Nome:</b></li>
                                <li><b>Variante:</b></li>
                            </ul>
                        </span>
                    </li>
                    <li><b>Desconto:</b><span id="order-total_descontos"></span></li>
                    <li><button type="button" class="btn btn-primary">Gerar Boleto</button></li>
                </ul>
            </div>
        </div>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>

</script>
<!-- Facebook Pixel Code -->

</html>