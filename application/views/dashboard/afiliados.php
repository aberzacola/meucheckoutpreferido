<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | MCP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="https://unpkg.com/tabulator-tables@4.4.3/dist/css/tabulator.min.css" rel="stylesheet">
  <style>
    #gravando {
      height: 32px;
    }
  </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?= $this->load->view("dashboard/main_header", "", true) ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $this->load->view("dashboard/main_sidebar", array("loja", $loja), true) ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Afiliados
          <small>Gerenciador de afiliacoes</small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <div id="success_alert" class="alert alert-success hidden" role="alert">
              Registro gravado
            </div>
            <div class="box box-primary">
              <div class="box-header">
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <label for="status_checkout">Código de Afiliação:</label>
                    <input id="cod_afiliado" class="<?= (!$loja['cod_afiliado']) ? "hidden" : "" ?>" type="text" value="<?= $loja['cod_afiliado'] ?>" disabled />
                    <button type="button" id="gerar_codigo" class="btn btn-success <?= (!$loja['cod_afiliado']) ? "" : "hidden" ?>">Gerar meu código</button>
                    <img id="gravando" class="hidden" src="<?= $this->config->item("base_url") ?>/assets/images/spin.gif" />
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <ul>
                      <li>
                        <h4>A cada recarga de MCP que seu afiliado faz, você ganha 10%!</h4>
                      </li>
                      <li>
                        <h4>Acima de 100 MCPs, você pode solicitar a retirada com nosso suporte</h4>
                      </li>
                      <li>
                        <h4>Taxas:</h4>
                        <ul>
                          <li>Até 250 MCPs taxa de 30%</li>
                          <li>Acima de 250 MCPs taxa de 25%</li>
                          <li>Acima de 500 MCPs taxa de 20%</li>
                        </ul>
                      </li>
                      <li>
                      <h4>MCPs de afiliados serão creditados após 30 dias de confirmado o pagamento.</h4>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <div id="recargas"></div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">

              </div>
            </div>
          </div>
        </div>
        <!--------------------------
        | Your Page Content Here |
        -------------------------->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="pull-right hidden-xs">
        Feito pensando no lojista
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; <?= date("Y") ?> <a target="_blank" href="<?= $this->config->item("base_url") ?>">Meu Checkout Prefeirdo</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/js/adminlte.min.js"></script>

  <script src="<?= $this->config->item('base_url'); ?>assets/js/jquery.mask.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/pt.js"></script>
  <script type="text/javascript" src="https://unpkg.com/tabulator-tables@4.4.3/dist/js/tabulator.min.js"></script>

  <script>
    var url_get = window.location.href.split("?");
    var table = new Tabulator("#recargas", {
      height: 400, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
      layout: "fitColumns", //fit columns to width of table (optional)
      ajaxURL: "/dashboard/get_all_pacotes_compras/?" + url_get[1],
      columns: [ //Define Table Columns
        {
          title: "Id",
          field: "id",
          visible: false
        },
        {
          title: "Pacote Comprado",
          field: "pacote_comprado",
          headerFilter: true
        },
        {
          title: "Data",
          field: "date_updated",
          sorter: "date",
          sorterParams: {
            format: "DD/MM/YYYY HH:mm"
          }
        },
        {
          title: "Creditado",
          field: "creditado",
          headerFilter: true
        },
        {
          title: "Valor",
          field: "valor"
        }
      ],
      rowFormatter: function(row) {
        //row - row component

        var data = row.getData();


        var moment_vencimento = moment(data.date_updated, "DD/MM/YYYY HH:mm").add(30, 'days');
        if (moment(moment_vencimento).isBefore(moment())) {
          row.getElement().style.backgroundColor = "#8ae37b";
        }
      }
    });


    $("#gerar_codigo").on("click", function() {
      $("#gravando").removeClass("hidden");
      $("#gerar_codigo").addClass("hidden");
      var url_get = window.location.href.split("?");
      $.ajax({

        type: "POST",
        url: "<?= $this->config->item("base_url") ?>dashboard/gerar_codigo_afiliado?" + url_get[1],
        success: function(e) {
          e = JSON.parse(e);
          $("#cod_afiliado").val(e.cod_afiliado);
          $("#cod_afiliado").removeClass("hidden");
          $('html, body').animate({
            scrollTop: $("#success_alert").offset().top
          }, 500);
          $("#success_alert").removeClass("hidden");
          setTimeout(function() {
            $("#success_alert").addClass("hidden");
          }, 3000);
        }
      }).done(function() {
        $("#gravando").addClass("hidden");
        $("#gerar_codigo").addClass("hidden");
      });
    })
  </script>

  <!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>

</html>