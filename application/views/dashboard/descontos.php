<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | MCP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    #gravando {
      height: 32px;
    }
  </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?= $this->load->view("dashboard/main_header", "", true) ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $this->load->view("dashboard/main_sidebar", array("loja", $loja), true) ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Cupons
          <small>Crie e gerencie cupons</small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <div id="success_alert" class="alert alert-success hidden" role="alert">
              Registro gravado
            </div>
            <div id="cupom_existe" class="alert alert-danger hidden" role="alert">
              Este cupom já existe
            </div>
            <div class="box box-primary">
              <div class="box-header ui-sortable-handle" style="cursor: move;">
                <h3 class="box-title">Criar Cupom</h3>
              </div>

              <form role="form" id="descontos_form">
                <div class="box-body">
                  <div class="form-group">
                    <label for="desconto_nome">Nome:</label>
                    <input value="" type="text" class="form-control" name="desconto_nome" id="desconto_nome" placeholder="Ex: SUPER_VERAO">
                  </div>
                  <div class="form-group">
                    <label for="tipo_desconto">Tipo:</label>
                    <select id="tipo_desconto" name="tipo_desconto" class="custom-select form-control">
                      <option value="total" selected>Total</option>
                      <option value="produtos">Produtos</option>
                      <option value="frete">Frete</option>
                    </select>
                    <small class="form-text text-muted">
                      <ul>
                        <li>Frete: Desconto somente na taxa de frete</li>
                        <li>Total: Desconto sob o total da compra produtos+frete</li>
                        <li>Produto: Desconto somente no preço total dos produtos</li>
                      </ul>
                    </small>
                  </div>
                  <div class="form-group">
                    <label for="modalidade_desconto">Modalidade:</label>
                    <select id="modalidade_desconto" name="modalidade_desconto" class="custom-select form-control">
                      <option value="porcentagem" selected>Porcentagem</option>
                      <option value="fixo">Valor fixo</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="desconto_valor">Valor:</label>
                    <input value="" type="text" class="form-control" name="desconto_valor" id="desconto_valor">
                    <input value="" type="hidden" class="form-control" name="desconto_valor_puro" id="desconto_valor_puro">
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button id="gravar" type="submit" class="btn btn-primary">Criar</button>
                  <img id="gravando" class="hidden" src="<?= $this->config->item("base_url") ?>/assets/images/spin.gif" />
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Cupons Existentes:</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  <tr>
                    <th>Nome</th>
                    <th>Tipo</th>
                    <th>Modalidade</th>
                    <th>Valor</th>
                    <th>Deletar</th>
                  </tr>
                  <?php
                  if ($descontos) {
                    foreach ($descontos as $desconto) {
                      echo "<tr id='{$desconto['id']}' >";
                      echo "<td>{$desconto['nome']}</td>";
                      echo "<td>{$desconto['tipo']}</td>";
                      echo "<td>{$desconto['modalidade']}</td>";
                      $valor = ($desconto['modalidade'] == "fixo") ? "R$ " . number_format($desconto['valor'] / 100, 2, ',', '.') : $desconto['valor'] . "%";
                      echo "<td>$valor</td>";
                      echo "<td><a class='deletar' style='cursor:pointer' ><i class='fa  fa-dollar'></i> <span>Deletar</span></a></td>";
                    }
                  }
                  ?>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
        <!--------------------------
        | Your Page Content Here |
        -------------------------->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="pull-right hidden-xs">
        Feito pensando no lojista
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; <?= date("Y") ?> <a target="_blank" href="<?= $this->config->item("base_url") ?>">Meu Checkout Prefeirdo</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/js/adminlte.min.js"></script>

  <script src="<?= $this->config->item('base_url'); ?>assets/js/jquery.mask.min.js"></script>
  <script src="<?= $this->config->item('base_url'); ?>assets/js/jquery.maskMoney.min.js"></script>

  <script>
    function muda_mascara(modalidade) {
      var desconto_valor = $("#desconto_valor");
      if (modalidade == "porcentagem") {
        desconto_valor.maskMoney('destroy')
        desconto_valor.val("");
        desconto_valor.mask('#0%', {
          reverse: true
        });
      } else {
        desconto_valor.unmask();
        desconto_valor.val("");
        desconto_valor.maskMoney({
          prefix: "R$ ",
          decimal: ",",
          thousands: "."
        });
      }
    }

    function set_valor_puro(modalidade) {
      var desconto_valor = $("#desconto_valor");
      var desconto_valor_puro = $("#desconto_valor_puro");
      if (modalidade == "porcentagem") {
        desconto_valor_puro.val(desconto_valor.cleanVal())
      } else {
        desconto_valor_puro.val(desconto_valor.maskMoney('unmasked')[0] * 100);
      }
    }


    $(document).ready(function() {
      var url_get = window.location.href.split("?");
      var modalidade_desconto = $("#modalidade_desconto");

      muda_mascara(modalidade_desconto.val());

      modalidade_desconto.on("change", function() {
        muda_mascara($(this).val());
      });

      $("#descontos_form").on("submit", function(e) {
        e.preventDefault();
        set_valor_puro(modalidade_desconto.val());

        $("#gravando").removeClass("hidden");
        $("#gravar").addClass("hidden");
        var form = this;

        $.ajax({
          type: "POST",
          processData: false,
          contentType: false,
          dataType: "json",
          cache: false,
          url: "<?= $this->config->item("base_url") ?>dashboard/salva_desconto?" + url_get[1],
          data: new FormData(form),
          success: function(resposta) {
            if (resposta.resposta == true) {
              $('html, body').animate({
                scrollTop: $("#success_alert").offset().top
              }, 500);
              $("#success_alert").removeClass("hidden");
              setTimeout(function() {
                $("#success_alert").addClass("hidden");
              }, 3000);
            } else {
              $('html, body').animate({
                scrollTop: $("#cupom_existe").offset().top
              }, 500);
              $("#cupom_existe").removeClass("hidden");
              setTimeout(function() {
                $("#cupom_existe").addClass("hidden");
              }, 3000);
            }

          }
        }).done(function() {
          $("#gravando").addClass("hidden");
          $("#gravar").removeClass("hidden");
        });

      });

      $(".deletar").on("click", function(e) {
        e.preventDefault();
        var parent = $($(this).parents("tr")[0]);
        var id_desconto = parent.attr('id');

        $.ajax({
          type: "POST",
          url: "<?= $this->config->item("base_url") ?>dashboard/deletar_desconto?" + url_get[1],
          data: {
            id_desconto: id_desconto
          },
          success: function() {
            parent.hide();
            $('html, body').animate({
              scrollTop: $("#success_alert").offset().top
            }, 500);
            $("#success_alert").removeClass("hidden");
            setTimeout(function() {
              $("#success_alert").addClass("hidden");
            }, 3000);
          }
        }).done(function() {
          $("#gravando").addClass("hidden");
          $("#gravar").removeClass("hidden");
        });

      })


    })
  </script>

  <!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>

</html>