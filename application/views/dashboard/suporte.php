<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | MCP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?= $this->load->view("dashboard/main_header", "", true) ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $this->load->view("dashboard/main_sidebar", array("loja", $loja), true) ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Painel Principal
          <small>Dados Sobre seus pedidos</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
          <li class="active">Here</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">

        <div class="box box-primary">
          <div class="box-header">
            <div class="row">
              <div class="col-12 text-center mt-5">
                <h2>Suporte Whatsapp: <a href="https://wa.me/553198288-0686" target="_blank">31 9 98288-0686</a></h2>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="container ">
              <div class="panel-group" id="faqAccordion">
                <div class="panel panel-default ">
                  <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question0">
                    <h4 class="panel-title">
                      <a href="#" class="ing">Como serei cobrado?</a>
                    </h4>

                  </div>
                  <div id="question0" class="panel-collapse collapse" style="height: 0px;">
                    <div class="panel-body">
                      <h5><span class="label label-primary">Resposta</span></h5>
                      <p>
                        Após uma venda ser realizada e concluída com sucesso, nós calculamos a tarifa ( R$ 1,00 ou 1% ) e abatemos no seu saldo de Bonus/MCPs
                      </p>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default ">
                  <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question1">
                    <h4 class="panel-title">
                      <a href="#" class="ing">Oque são MCPs</a>
                    </h4>
                  </div>
                  <div id="question1" class="panel-collapse collapse" style="height: 0px;">
                    <div class="panel-body">
                      <h5><span class="label label-primary">Resposta</span></h5>
                      <p>MCPs são os créditos equivalentes a reais que você possui, são deles que são abatidos os valores da tarifa única após os bonus acabarem</p>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default ">
                  <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#bonus">
                    <h4 class="panel-title">
                      <a href="#" class="ing">Oque são Bonus?</a>
                    </h4>
                  </div>
                  <div id="bonus" class="panel-collapse collapse" style="height: 0px;">
                    <div class="panel-body">
                      <h5><span class="label label-primary">Resposta</span></h5>
                      <p>Os bonus tem a mesma funcionalidade dos MCPs, mas não podem ser sacados, você ganha bonus ao entrar pela primeira vez e ao efetuar recargas</p>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default ">
                  <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question2">
                    <h4 class="panel-title">
                      <a href="#" class="ing">Como conseguir MCPs</a>
                    </h4>

                  </div>
                  <div id="question2" class="panel-collapse collapse" style="height: 0px;">
                    <div class="panel-body">
                      <h5><span class="label label-primary">Answer</span></h5>
                      <p>MCPs são créditos, que você pode comrpar no link "loja" que está do lado esquerdo.</p>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default ">
                  <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question3">
                    <h4 class="panel-title">
                      <a href="#" class="ing">Recuperação de Vendas ( Boleto e Cartão )</a>
                    </h4>

                  </div>
                  <div id="question3" class="panel-collapse collapse" style="height: 0px;">
                    <div class="panel-body">
                      <h5><span class="label label-primary">Resposta</span></h5>
                      <p>
                        No Dashboard, existe uma tabela de ordens, lá estão todas as ordens que foram processadas pelo nosso checkout, ao clicar nela você verá um resumo da ordem.
                        Nessa tela, você pode gerar um boleto pra essa ordem. Mas atenção, a ordem original, será deletada e subistituída por outra identica, mas sem os cupons ou descontos.
                        Caso queira aplicar algum desconto, use o campo de cupom antes de gerar a ordem.
                      </p>
                    </div>
                  </div>
                </div>

              </div>
              <!--/panel-group-->
            </div>
          </div>
        </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="pull-right hidden-xs">
        Feito pensando no lojista
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; <?= date("Y") ?> <a target="_blank" href="<?= $this->config->item("base_url") ?>">Meu Checkout Prefeirdo</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/js/adminlte.min.js"></script>

  <!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>

</html>