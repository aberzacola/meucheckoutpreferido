<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | MCP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    #gravando {
      height: 32px;
    }
  </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?= $this->load->view("dashboard/main_header", "", true) ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $this->load->view("dashboard/main_sidebar", array("loja", $loja), true) ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Loja
          <small>Reabasteça seus MCPs aqui</small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <?php
            if ($comprou == 1) {
              echo '<div class="alert alert-success" role="alert">Tudo certo, assim que o pagamento for confirmado, seus MCPs serão creditados</div>';
            }
            ?>
            <div class="box box-primary">
              <!-- /.box-header -->
              <!-- form start -->
              <form class="" action="/dashboard/processar_pagamento/?<?=$_SERVER['QUERY_STRING']?>" method="post" id="pay" name="pay" autocomplete="off">
                <div class="box-body">
                  <div class="form-group">
                    <label for="id_pacote">Pacote:</label>
                    <select id="id_pacote" name="id_pacote" class="custom-select form-control">
                      <?php
                      foreach ($pacotes as $pacote) {
                        echo "<option value='{$pacote['id']}' >{$pacote['nome']}</option>";
                      }
                      ?>
                    </select>
                    <h3>Informações pessoais</h3>
                    <div class="form-group">
                      <label for="nomeComprador">Nome Completo</label>
                      <input type="text" class="form-control" id="nomeComprador" name='nomeComprador' placeholder="Digite seu nome completo" required autocomplete="off">
                    </div>
                    <div class="form-group">
                      <label for="emailComprador">Email</label>
                      <input type="text" class="form-control" id="emailComprador" name='emailComprador' placeholder="Digite seu e-mail" required autocomplete="off">
                    </div>
                    <div class="form-group">
                      <label for="documentoComprador">CPF</label>
                      <input type="text" class="form-control" id="documentoComprador" name='documentoComprador' placeholder="Digite seu CPF" required autocomplete="off">
                    </div>
                    <h3>Método de pagamento</h3>
                    <label for="metodo_pagamento">Método Pagemento:</label>
                    <select id="metodo_pagamento" name="metodo_pagamento" class="custom-select form-control" style="margin-bottom:20px">
                      <option value=''>Selecione</option>
                      <option value='boleto'>Boleto</option>
                      <option value='cartao'>Cartão</option>
                    </select>
                    <div class="campo_cartao hidden">
                      <div class='card-wrapper mt-3'></div>
                      <div class="form-group">
                        <label for="cardNumber">Número do cartão</label>
                        <input type="tel" data-checkout="cardNumber" class="form-control" id="cardNumber" placeholder="Numero do seu cartão" required>
                      </div>
                      <div class="form-group">
                        <label for="cardholderName">Nome que está escrito no cartão</label>
                        <input type="text" class="form-control" id="cardholderName" data-checkout="cardholderName" placeholder="Nome escrito no cartão de crédito" required>
                      </div>
                      <div class="row">
                        <div class="col-12 col-md-6">
                          <div class="form-group">
                            <label for="expiry">Validade</label>
                            <input type="tel" class="form-control" id="expiry" name='expiry' placeholder="Validade do seu cartão Ex: 06/23" required>
                          </div>
                        </div>
                        <div class="col-12 col-md-6">
                          <div class="form-group">
                            <label for="securityCode">Código de segurança (CVC)</label>
                            <input type="tel" class="form-control" data-checkout="securityCode" id="securityCode" placeholder="Código de segurança" required>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="campo_boleto hidden">
                      <div class="text-container boleto_data">
                        <p>Clique em "Finalizar Compra" para gerar o seu boleto.</p>
                        <p>Informações sobre o pagamento via boleto:</p>
                        <ul>
                          <li>Prazo de até 2 dias úteis para compensar.</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
                <select id="docType" data-checkout="docType" class="invisible" name="docType">
                  <option value="CPF" selected>CPF</option>
                </select>
                <input type="hidden" id="docNumber" data-checkout="docNumber" />
                <input type="hidden" id="paymentMethodId" name="paymentMethodId" />
                <input type="hidden" id="cardExpirationYear" data-checkout="cardExpirationYear" onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off />
                <input type="hidden" id="cardExpirationMonth" data-checkout="cardExpirationMonth" onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off />
                <div class="box-footer">
                  <button id="gravar" type="submit" class="btn btn-primary">Salvar</button>
                  <img id="gravando" class="hidden" src="<?= $this->config->item("base_url") ?>/assets/images/spin.gif" />
                </div>
              </form>
            </div>
          </div>
        </div>
        <!--------------------------
        | Your Page Content Here |
        -------------------------->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="pull-right hidden-xs">
        Feito pensando no lojista
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; <?= date("Y") ?> <a target="_blank" href="<?= $this->config->item("base_url") ?>">Meu Checkout Prefeirdo</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/js/adminlte.min.js"></script>

  <script src="<?= $this->config->item('base_url'); ?>assets/js/jquery.mask.min.js"></script>
  <script src="<?= $this->config->item('base_url'); ?>assets/js/card-master/dist/card.js"></script>
  <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>

  <script>
    var ticket = false;
    var doSubmit = false;
    window.Mercadopago.setPublishableKey("<?= $this->config->item('mcp_mp_public_key') ?>");

    function getBin() {
      const cardnumber = document.getElementById("cardNumber").value;
      return cardnumber.replace(/ /g, '').substring(0, 6);
    }

    function guessingPaymentMethod(event) {
      var bin = getBin();
      if (event.type == "keyup") {
        if (bin.length >= 6) {
          window.Mercadopago.getPaymentMethod({
            "bin": bin
          }, setPaymentMethodInfo);
        }
      } else {
        setTimeout(function() {
          if (bin.length >= 6) {
            window.Mercadopago.getPaymentMethod({
              "bin": bin
            }, setPaymentMethodInfo);
          }
        }, 100);
      }
    };

    function setPaymentMethodInfo(status, response) {
      var form = document.querySelector('#pay');
      if (status == 200) {
        const paymentMethodElement = document.querySelector('input[name=paymentMethodId]');
        if (paymentMethodElement) {
          paymentMethodElement.value = response[0].id;
        } else {
          const input = document.createElement('input');
          input.setattribute('name', 'paymentMethodId');
          input.setAttribute('type', 'hidden');
          input.setAttribute('value', response[0].id);

          form.appendChild(input);
        }
      } else {
        alert(`payment method info error: ${response}`);
      }
    };

    function sdkResponseHandler(status, response) {
      console.log(status, response);
      if (status != 200 && status != 201) {
        alert("Confira os dados do cartão");
        $('html, body').animate({
          scrollTop: $(".card-wrapper").offset().top
        }, 500);
      } else {
        var form = document.querySelector('#pay');
        var card = document.createElement('input');
        card.setAttribute('name', 'token');
        card.setAttribute('type', 'hidden');
        card.setAttribute('value', response.id);
        form.appendChild(card);
        doSubmit = true;
        form.submit();
      }
    };

    function numberToReal(numero) {
      var numero = numero.toFixed(2).split('.');
      numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
      return numero.join(',');
    }

    function validarCPF(cpf) {
      cpf = cpf.replace(/[^\d]+/g, '');
      if (cpf == '') {
        return false;
      }

      // Elimina CPFs invalidos conhecidos
      if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999") {
        return false;
      }
      // Valida 1o digito
      add = 0;
      for (i = 0; i < 9; i++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
      rev = 11 - (add % 11);
      if (rev == 10 || rev == 11)
        rev = 0;
      if (rev != parseInt(cpf.charAt(9))) {
        return false;
      }
      // Valida 2o digito
      add = 0;
      for (i = 0; i < 10; i++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
      rev = 11 - (add % 11);
      if (rev == 10 || rev == 11)
        rev = 0;
      if (rev != parseInt(cpf.charAt(10))) {
        return false;
      } else {
        return true;
      }
    }

    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    $("#pay").on("submit", function(e) {

      $("#gravando").removeClass("hidden");
      $("#gravar").addClass("hidden");

      e.preventDefault();

      //Validando CPF
      if (validarCPF($("#documentoComprador").cleanVal()) === false) {
        $("#documentoComprador").parent().addClass('has-error');
        $('html, body').animate({
          scrollTop: $("#documentoComprador").offset().top
        }, 500);
        return false;
      } else {
        $("#documentoComprador").parent().removeClass('has-error');
      }

      if (!/\w+\s+\w+/.test($("#nomeComprador").val())) {
        $("#nomeComprador").parent().addClass('has-error');
        alert("Digite seu nome completo");
        $('html, body').animate({
          scrollTop: $("#nomeComprador").offset().top
        }, 500);
        return false;
      } else {
        $("#nomeComprador").parent().removeClass('has-error');
      }

      if (!validateEmail($("#emailComprador").val())) {
        $("#emailComprador").parent().addClass('has-error');
        alert("Digite seu nome completo");
        $('html, body').animate({
          scrollTop: $("#emailComprador").offset().top
        }, 500);
        return false;
      } else {
        $("#emailComprador").parent().removeClass('has-error');
      }

      $("#docNumber").val($("#documentoComprador").cleanVal());

      if (!doSubmit && !ticket) {
        if (jQuery("input[id='cardNumber']").is(':visible')) {
          if (jQuery("#expiry").length) {
            var cardExpiryDate = jQuery("#expiry").val().replace(/ /g, '');
            if (typeof cardExpiryDate != "undefined" && cardExpiryDate != "" && cardExpiryDate != null) {
              var cardExpiryDateArray = cardExpiryDate.split('/');

              if (typeof cardExpiryDateArray != "undefined" && cardExpiryDateArray != "" && cardExpiryDateArray != null && cardExpiryDateArray.length == 2) {
                if (jQuery("#cardExpirationMonth").length) {
                  jQuery("#cardExpirationMonth").attr('value', cardExpiryDateArray[0]);
                  jQuery("#cardExpirationMonth").val(cardExpiryDateArray[0]);
                }
                if (jQuery("#cardExpirationYear").length) {
                  jQuery("#cardExpirationYear").attr('value', "20" + cardExpiryDateArray[1]);
                  jQuery("#cardExpirationYear").val("20" + cardExpiryDateArray[1]);
                }
              }
            }
          }
          guessingPaymentMethod(e);
          if (!doSubmit) {
            var $form = document.querySelector('#pay');
            Mercadopago.createToken($form, sdkResponseHandler); // The function "sdkResponseHandler" is defined below
            return false;
          }
        }
      } else {
        var form = document.querySelector('#pay');
        doSubmit = true;
        form.submit();
      }
    })

    // mascara cpf
    $("#documentoComprador").mask("000.000.000-00");

    $("#metodo_pagamento").on("change", function() {
      var metodo = $("#metodo_pagamento").val();
      var camposCartao = $(".campo_cartao input");

      if (metodo == "cartao") {
        for (var i = 0; i < camposCartao.length; i++) {
          $(camposCartao[i]).attr("required", "required");
        }
        ticket = false;
        $(".campo_cartao").removeClass("hidden");
        $(".campo_boleto").addClass("hidden");
      } else {
        for (var i = 0; i < camposCartao.length; i++) {
          $(camposCartao[i]).removeAttr("required");
        }
        const paymentMethodElement = document.querySelector('input[name=paymentMethodId]');
        paymentMethodElement.value = 'bolbradesco';
        ticket = true;
        $(".campo_cartao").addClass("hidden");
        $(".campo_boleto").removeClass("hidden");
      }
    })

    $('#expiry').mask('00 / 00');

    var card = new Card({
      form: 'form',
      container: '.card-wrapper',
      formSelectors: {
        numberInput: 'input#cardNumber', // optional — default input[name="number"]
        expiryInput: 'input#expiry', // optional — default input[name="expiry"]
        cvcInput: 'input#securityCode', // optional — default input[name="cvc"]
        nameInput: 'input#cardholderName' // optional - defaults input[name="name"]
      },
      placeholders: {
        name: 'Nome completo'
      },
      messages: {
        validDate: 'expire\ndate',
        monthYear: 'mm/yy'
      }
    })
  </script>

  <!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>

</html>