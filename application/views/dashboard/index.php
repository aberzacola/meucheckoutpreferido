<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | MCP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="https://unpkg.com/tabulator-tables@4.4.3/dist/css/tabulator.min.css" rel="stylesheet">
  <style>
    .gerar_boleto {
      width: 100%;
    }
  </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?= $this->load->view("dashboard/main_header", "", true) ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $this->load->view("dashboard/main_sidebar", array("loja", $loja), true) ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Painel Principal
          <small>Dados Sobre seus pedidos nos útilos <b>60 dias</b></small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="alert alert-success" role="alert">
          Ulitima funcionalidade integrada: <b>Recuperação de Carrinhos ( BETA )</b>
        </div>
        <div class="row">
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3><?= $orders_total ?></h3>

                <p>Ordens</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3><?= $orders_pagas ?></h3>

                <p>Pagas</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3><?= $orders_nao_pagas ?></h3>

                <p>Não Pagas</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3></h3>

                <p>Carrinhos Abandonados</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-cart-outline"></i>
              </div>
              <a href="#" class="small-box-footer">Estágio BETA<i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Ordens:</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div id="orders"></div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      </section>
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="nome_order">Detalhes da ordem</h4>
            </div>
            <div class="modal-body">
              <div class="row aguarde">
                <div class="col-xs-12">
                  <div class="alert alert-info" role="alert">
                    Aguarde
                  </div>
                </div>
              </div>
              <div class="row hidden">
                <div class="col-12">
                  <ul>
                    <li><b>Nome Comprador:</b> <span id="order-nome"></span></li>
                    <li><b>Edereço:</b> <span id="order-endereco"></span></li>
                    <li><b>Celular:</b> <span id="order-celular"></span> <a href="#" target="_blank" id="order-celular_zap">WhatsApp</a></li>
                    <li><b>Email:</b> <span id="order-email"></span></li>
                    <li><b>CPF:</b> <span id="order-cpf"></span></li>
                    <li><b>Ultima Atualizacao:</b> <span id="order-ultima_alteracao"></span></li>
                    <li><b>Tipo Pagamento:</b> <span id="order-tipo_pagamento"></span></li>
                    <li><b>Status:</b> <span id="order-status_pagamento"></span></li>
                    <li><b>Detalhes Pagamento:</b> <span id="order-detalhes_status_pagamento"></span></li>
                    <li><b>Url Boleto:</b> <a target="_blank" href="#" id="order-url_boleto"></a></li>
                    <li><b>Cod. Boleto:</b> <span id="order-cod_boleto"></span></li>
                    <li><b>Valor:</b><span id="order-valor"></span></li>
                    <li><b>Valor sem desconto:</b><span id="order-valor_sem_desconto"></span></li>
                    <li>
                      <b>Produtos:</b>
                      <span id="order-produtos">

                      </span>
                    </li>
                    <li><b>Desconto:</b><span id="order-total_descontos"></span></li>
                  </ul>
                </div>
              </div>
              <div class="row pt-3 mt-3 hidden">
                <div class="col-xs-12">
                  <h4 class="mb-3">Cupom de desconto</h4>
                  <div class="row area_cupom">
                    <div class="col-xs-8">
                      <div class="form-group">
                        <input type="text" class="form-control" id="cupomDesconto" name='cupomDesconto' placeholder="Digite seu cupom" autocomplete="off">
                        <input type="hidden" class="form-control" id="cupom_id" name='cupom_id' value="-1">
                        <input type="hidden" class="form-control" id="order_id" name='order_id' value="-1">
                        <small class="form-text text-muted">desconto a ser aplicado no novo boleto</small>
                      </div>
                    </div>
                    <div class="col-xs-4">
                      <button id="aplicar_desconto" type="button" style="width:100%" class="btn btn-primary">Aplicar</button>
                    </div>
                  </div>
                  <div class="cupom_resposta row hidden">
                    <div class="col-xs-12">
                      <div class="alert alert-info" role="alert">
                        This is a primary alert—check it out!
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row hidden">
                <div class="valor_nova_order col-xs-12">
                  <h4>Valor da nova ordem:</h4>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="alert alert-success" role="alert">

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row hidden">
                <div class="col-xs-12">
                  <h4>CPF do comprador:</h4>
                  <input type="text" class="form-control" id="cpf_comprador" name='cpf_comprador' placeholder="CPF do comprador" autocomplete="off">
                  <small class="form-text text-muted">Gerar um novo boleto, exige o CPF do comprador</small>
                </div>
              </div>
              <div class="row hidden">
                <div class="col-xs-12">
                  <p style="color:red">Ao clicar em gerar boleto, esta ordem será DELETADA do shopify, e uma nova ordem idêntica será criada para ser paga no boleto</p>
                  <input type="checkbox" id="estou_ciente" value="1"> Estou ciente<br>
                </div>
              </div>
              <div class="row hidden">
                <div class="col-xs-12">
                  <button type="button" class="btn btn-primary gerar_boleto">Gerar Boleto</button>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="pull-right hidden-xs">
        Feito pensando no lojista
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; <?= date("Y") ?> <a target="_blank" href="<?= $this->config->item("base_url") ?>">Meu Checkout Prefeirdo</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/js/adminlte.min.js"></script>
  <script src="<?= $this->config->item('base_url'); ?>assets/js/jquery.mask.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/pt.js"></script>
  <script type="text/javascript" src="https://unpkg.com/tabulator-tables@4.4.3/dist/js/tabulator.min.js"></script>

  <!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
<script>
  var url_get = window.location.href.split("?");
  var order_details;

  $("#cpf_comprador").mask("000.000.000-00");

  var table = new Tabulator("#orders", {
    height: 400, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
    layout: "fitColumns", //fit columns to width of table (optional)
    ajaxURL: "/dashboard/get_all_orders/?" + url_get[1],
    columns: [ //Define Table Columns
      {
        title: "Id",
        field: "id",
        visible: false
      },
      {
        title: "Ordem",
        field: "order_name",
        headerFilter: true
      },
      {
        title: "Data Criada",
        field: "date_created",
        sorter: "date",
        sorterParams: {
          format: "DD/MM/YYYY HH:mm"
        }
      },
      {
        title: "Data Atualizada",
        field: "date_updated",
        sorter: "date",
        sorterParams: {
          format: "DD/MM/YYYY HH:mm"
        }
      },
      {
        title: "Excluida",
        field: "excluida",
        headerFilter: true
      },
      {
        title: "Tipo Pagamento",
        field: "tipo_pagamento",
        headerFilter: true
      },
      {
        title: "Pagamento",
        field: "status",
        headerFilter: true
      },
      {
        title: "Detalhes Pagamento",
        field: "status_detail",
        headerFilter: true
      },
      {
        title: "Valor",
        field: "valor_total"
      }
    ],
    rowFormatter: function(row) {
      //row - row component

      var data = row.getData();


      var moment_vencimento = moment(data.date_created, "DD/MM/YYYY HH:mm").add(3, 'days');
      if (moment(moment_vencimento).isBefore(moment()) && data.tipo_pagamento == "Boleto" && data.excluida == "Não") {
        row.getElement().style.backgroundColor = "#ffff80";
      }
    },
    rowClick: function(e, row) { //trigger an alert message when the row is clicked
      var order_name = row.getData().order_name;
      var order_id = row.getData().id;
      $("#nome_order").html(order_name);
      $("#order_id").val(order_id);
      $('#myModal').modal('toggle');
      $.ajax({
        type: "POST",
        url: "<?= $this->config->item("base_url") ?>dashboard/order_detail?" + url_get[1],
        data: {
          order_id: order_id
        },
        success: function(resposta) {
          resposta = JSON.parse(resposta);
          if (resposta.excluida == 1) {
            $(".aguarde .alert-info").html("Ordem substituida pela: " + resposta.nova_order_name);
            return false;
          }
          order_details = resposta;
          $("#order-nome").html(resposta.nome_comprador);
          $("#order-endereco").html(resposta.endereco_comprador);
          $("#order-celular").html(resposta.celular_comprador);
          $("#order-celular_zap").attr("href", "https://wa.me/55" + resposta.celular_comprador.replace(/[^0-9]/g, ''));
          $("#order-email").html(resposta.email_comprador);
          $("#order-cpf").html(resposta.cpf_comprador);
          $("#order-ultima_alteracao").html(resposta.date_updated);
          $("#order-tipo_pagamento").html(resposta.tipo_pagamento);
          if (resposta.status == "Pago") {
            $(".gerar_boleto").attr("disabled", true);
          } else {
            $(".gerar_boleto").attr("disabled", false);
          }
          $("#order-status_pagamento").html(resposta.status);
          $("#order-detalhes_status_pagamento").html(resposta.status_detail);
          if (resposta.url_boleto) {
            $("#order-url_boleto").attr("href", resposta.url_boleto);
            $("#order-url_boleto").html("Abrir");
          } else {
            $("#order-url_boleto").html("Não se aplica");
          }

          if (resposta.codigo_boleto) {
            $("#order-cod_boleto").html(resposta.codigo_boleto);
          } else {
            $("#order-cod_boleto").html("Não se aplica");
          }

          $("#order-valor").html(resposta.valor_total);
          $("#order-valor_sem_desconto").html(numberToReal(parseFloat(order_details.order_detail_shopify.total_line_items_price) + parseFloat(order_details.order_detail_shopify.total_shipping_price_set.shop_money.amount)));

          var order_produtos = $("#order-produtos");
          order_produtos.html("");
          for (var i = 0; i < resposta.order_detail_shopify.line_items.length; i++) {
            var element = "<ul> " + (i + 1);
            element += "<li><b>Nome: </b>" + resposta.order_detail_shopify.line_items[i].title + "</li>";
            element += "<li><b>Variante: </b>" + resposta.order_detail_shopify.line_items[i].variant_title + "</li>";
            element += "</ul>";
            order_produtos.append(element);
          }

          $("#order-total_descontos").html(resposta.order_detail_shopify.total_discounts_formated);

          $(".valor_nova_order").removeClass("hidden");
          $(".valor_nova_order .alert-success").html("Valor da nova ordem: " + numberToReal(parseFloat(order_details.order_detail_shopify.total_line_items_price) + parseFloat(order_details.order_detail_shopify.total_shipping_price_set.shop_money.amount)));

          $("#cpf_comprador").val("");

          if (resposta.order_detail_shopify.total_discounts > 0) {
            $(".area_cupom").removeClass("hidden");
            $("#cupomDesconto").val(resposta.order_detail_shopify.discount_codes[0].code);
          } else {
            $("#cupomDesconto").val("");
            $(".area_cupom").removeClass("hidden");
          }
          mostrar();

        }
      });

    },
  });

  $('#myModal').on('hide.bs.modal', function(e) {
    esconder();
  })

  function mostrar() {
    var mostar = $(".modal-body").find(".row");
    for (var i = 0; i < mostar.length; i++) {
      if ($(mostar[i]).hasClass("cupom_resposta")) {
        continue;
      }
      if ($(mostar[i]).hasClass("aguarde")) {
        $(mostar[i]).addClass("hidden");
      } else {
        $(mostar[i]).removeClass("hidden");
      }
    }
  }

  function esconder() {
    var mostar = $(".modal-body").find(".row");
    for (var i = 0; i < mostar.length; i++) {
      if ($(mostar[i]).hasClass("aguarde")) {
        $(".aguarde .alert-info").html("Aguarde");
        $(mostar[i]).removeClass("hidden");
      } else {
        $(mostar[i]).addClass("hidden");
      }
    }
  }

  $(".gerar_boleto").on("click", function() {
    if (!$('#estou_ciente').is(':checked')) {
      alert("Para re-emitir um boleto, marque o botão 'estou ciente'");
      return false;
    }

    //Validando CPF
    if (validarCPF($("#cpf_comprador").cleanVal()) === false) {
        $("#cpf_comprador").parent().addClass('has-error');
        $('html, body').animate({
          scrollTop: $("#cpf_comprador").offset().top
        }, 500);
        alert("CPF inválido");
        return false;
      } else {
        $("#cpf_comprador").parent().removeClass('has-error');
      }

    esconder();
    $.ajax({
      type: "POST",
      url: "<?= $this->config->item("base_url") ?>dashboard/reemitir_boleto?" + url_get[1],
      data: {
        order_id: $("#order_id").val(),
        cupom_id: $("#cupom_id").val(),
        cpf_comprador: $("#cpf_comprador").val()
      },
      success: function(resposta) {
        $('#myModal').modal('hide');
        table.setData();
      }
    })
  })

  $("#aplicar_desconto").on("click", function() {
    $.ajax({
      type: "POST",
      url: "<?= $this->config->item("base_url") ?>checkout/check_discount/?" + url_get[1],
      cache: false,
      dataType: 'json',
      data: {
        cod_desconto: $("#cupomDesconto").val()
      },
      success: function(e) {
        $(".cupom_resposta").removeClass("hidden");
        if (e == false) {
          $(".cupom_resposta .alert-info").html("Cumpom inválido");
          $(".valor_nova_order").removeClass("hidden");
          $("#cupom_id").val(-1);
        } else {
          $(".area_cupom").addClass("hidden");
          $(".valor_nova_order").addClass("hidden");
          if (e.tipo == "frete") { // FRETE
            var frete = order_details.order_detail_shopify.total_shipping_price_set.shop_money.amount;
            if (e.modalidade == "fixo") { // fixo
              frete -= e.valor / 100;
              valor = numberToReal(e.valor / 100);
              if (frete < 0) {
                frete = 0;
              }
            } else { // porcentagem
              frete -= (frete * e.valor / 100);
              valor = e.valor + "%";
              if (frete < 0) {
                frete = 0;
              }
            }
            frete += parseFloat(order_details.order_detail_shopify.total_line_items_price);
            $(".cupom_resposta .alert-info").html("Cumpom aplicado - " + e.nome + " - Valor da nova ordem: " + numberToReal(frete));
          } else if (e.tipo == "produtos") { // PRODUTO
            var produtos = order_details.order_detail_shopify.total_line_items_price;
            if (e.modalidade == "fixo") { // fixo
              produtos -= e.valor / 100;
              valor = numberToReal(e.valor / 100);
              if (produtos < 0) {
                produtos = 0;
              }
            } else { // porcentagem
              produtos -= (produtos * e.valor / 100);

              valor = e.valor + "%";
              if (produtos < 0) {
                produtos = 0;
              }
            }
            produtos += parseFloat(order_details.order_detail_shopify.total_shipping_price_set.shop_money.amount);
            $(".cupom_resposta .alert-info").html("Cumpom aplicado - " + e.nome + " - Valor da nova ordem: " + numberToReal(produtos));
          } else { // TOTAL
            var total = order_details.order_detail_shopify.total_line_items_price + order_details.order_detail_shopify.total_shipping_price_set.shop_money.amount;
            if (e.modalidade == "fixo") { // fixo
              total -= e.valor / 100;
              valor = numberToReal(e.valor / 100);
              if (total < 0) {
                total = 0;
              }
            } else { // porcentagem
              total -= (total * e.valor / 100);
              valor = e.valor + "%";
              if (total < 0) {
                total = 0;
              }
            }
            $(".cupom_resposta .alert-info").html("Cumpom aplicado - " + e.nome + " - Valor da nova ordem: " + numberToReal(total));
          }
          $("#cupom_id").val(e.id);
        }


      }
    });
  })

  function numberToReal(numero) {
    var numero = numero.toFixed(2).split('.');
    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
  }

  function validarCPF(cpf) {
      cpf = cpf.replace(/[^\d]+/g, '');
      if (cpf == '') {
        return false;
      }

      // Elimina CPFs invalidos conhecidos
      if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999") {
        return false;
      }
      // Valida 1o digito
      add = 0;
      for (i = 0; i < 9; i++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
      rev = 11 - (add % 11);
      if (rev == 10 || rev == 11)
        rev = 0;
      if (rev != parseInt(cpf.charAt(9))) {
        return false;
      }
      // Valida 2o digito
      add = 0;
      for (i = 0; i < 10; i++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
      rev = 11 - (add % 11);
      if (rev == 10 || rev == 11)
        rev = 0;
      if (rev != parseInt(cpf.charAt(10))) {
        return false;
      } else {
        return true;
      }
    }
</script>

</html>