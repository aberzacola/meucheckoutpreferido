<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | MCP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    #gravando {
      height: 32px;
    }
  </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?= $this->load->view("dashboard/main_header", "", true) ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $this->load->view("dashboard/main_sidebar", array("loja", $loja), true) ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Configurações
          <small>Configurações do seu checkout</small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <div id="success_alert" class="alert alert-success hidden" role="alert">
              Registro gravado
            </div>
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Preview checkout: <a href='https://<?= $loja['dominio_proprio'] . "/apps/checkout" ?>' target="_blank"><?= $loja['dominio_proprio'] . "/apps/checkout" ?></a></h3>
              </div>
              <!-- form start -->
              <form role="form" id="loja_settings">
                <div class="box-body">
                  <div class="form-group">
                    <label for="status_checkout">Checkout:</label>
                    <select id="status_checkout" name="status_checkout" class="custom-select form-control">
                      <option value="1" <?= $loja['script_tag'] ? "selected" : "" ?>>Ligado</option>
                      <option value="0" <?= !$loja['script_tag']   ? "selected" : "" ?>>Desligado</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="texto_escassez">Texto de Escassez: ( texto que vai no banner de escassez )</label>
                    <input value="<?= $loja['texto_escassez'] ?>" type="tel" class="form-control" name="texto_escassez" id="texto_escassez" aria-describedby="texto_escassez_help" placeholder="">
                    <small id="texto_escassez_help" class="form-text text-muted">Padrão: Aproveite nossa oferta é por tempo limitado!</small>
                  </div>
                  <div class="form-group">
                    <label for="escassez">Cronometro de Escassez: ( Horas:Minutos:Segundos )</label>
                    <input value="<?= $loja['escassez'] ?>" type="tel" class="form-control" name="escassez" id="escassez" aria-describedby="escassez_help" placeholder="Ex: HH:MM:SS">
                    <small id="escassez_help" class="form-text text-muted">Cronometro no 00:00:00 escassez desligada</small>
                  </div>
                  <div class="form-group">
                    <label for="mp_public_key">Mercado Pago Public Key:</label>
                    <input value="<?= $loja['mp_public_key'] ?>" type="text" class="form-control" name="mp_public_key" id="mp_public_key" aria-describedby="mp_public_key_help" placeholder="Ex:  APP_USR-xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx">
                    <small id="mp_public_key_help" class="form-text text-muted">Pega aqui: <a href="https://www.mercadopago.com/mlb/account/credentials/" target="_blank">Mercado Pago Credentials</a></small>
                  </div>
                  <div class="form-group">
                    <label for="mp_access_token">Mercado Pago Access token:</label>
                    <input value="<?= $loja['mp_access_token'] ?>" type="text" class="form-control" name="mp_access_token" id="mp_access_token" aria-describedby="mp_access_token_help" placeholder="Ex: APP_USR-xxxxxxxxxxxxxxxx-xxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-xxxxxxxx">
                    <small id="mp_access_token_help" class="form-text text-muted">Pega aqui: <a href="https://www.mercadopago.com/mlb/account/credentials/" target="_blank">Mercado Pago Credentials</a></small>
                  </div>
                  <div class="form-group">
                    <label for="fb_pixel">Facebook Pixel:</label>
                    <input value="<?= $loja['fb_pixel'] ?>" type="text" class="form-control" name="fb_pixel" id="fb_pixel" aria-describedby="fb_pixel_help" placeholder="Ex: 175698547882356">
                  </div>
                  <div class="form-group">
                    <label for="pixel_boleto">Marcação Pixel Boleto:</label>
                    <select id="pixel_boleto" name="pixel_boleto" class="custom-select form-control">
                      <option value="desligado" <?= $loja['pixel_boleto'] == "desligado" ? "selected" : "" ?>>Desligado</option>
                      <option value="baixar" <?= $loja['pixel_boleto'] == "baixar"   ? "selected" : "" ?>>Ao Baixar</option>
                      <option value="sempre" <?= $loja['pixel_boleto'] == "sempre"   ? "selected" : "" ?>>Sempre</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="banner_lateral">Baner Lateral:</label>
                    <input type="file" name="banner_lateral" id="banner_lateral" accept="image/*">
                    <?php if ($loja['banner_lateral']) : ?>
                      <p class="help-block"><a target="_blank" href="<?= $this->config->item('aws_s3_bucket_url') . $loja['shopify_nome'] . "/" . $loja['banner_lateral'] ?>">Ver o Atual</a></p>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="remover_banner_lateral"> Remover
                        </label>
                      </div>
                    <?php else : ?>
                      <p class="help-block">Não cadastrado</p>
                    <?php endif ?>
                  </div>
                  <div class="form-group">
                    <label for="banner_superior">Baner Superior:</label>
                    <input type="file" name="banner_superior" id="banner_superior" accept="image/*">
                    <?php if ($loja['banner_superior']) : ?>
                      <p class="help-block"><a target="_blank" href="<?= $this->config->item('aws_s3_bucket_url') . $loja['shopify_nome'] . "/" . $loja['banner_superior'] ?>">Ver o Atual</a></p>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="remover_banner_superior"> Remover
                        </label>
                      </div>
                    <?php else : ?>
                      <p class="help-block">Não cadastrado</p>
                    <?php endif ?>
                  </div>
                  <div class="form-group">
                    <label for="pagina_obrigado">Pagina de Obrigado:</label>
                    <input type="file" name="pagina_obrigado" id="pagina_obrigado" accept=".htm*">
                    <?php if ($loja['pagina_obrigado']) : ?>
                      <p class="help-block"><a target="_blank" href="<?= $this->config->item('aws_s3_pages_base_url') . $loja['shopify_nome'] . "/" . $loja['pagina_obrigado'] ?>">Ver a Atual</a></p>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="remover_pagina_obrigado"> Remover
                        </label>
                      </div>
                    <?php else : ?>
                      <p class="help-block"><a target="_blank" href="<?= $this->config->item('aws_s3_pages_base_url') ?>obrigado.html">Ver a padrão</a></p>
                    <?php endif ?>
                  </div>
                  <div class="form-group">
                    <label for="desconto_cartao">Desconto para cartão:</label>
                    <input value="<?= $loja['desconto_cartao'] ?>" type="text" class="form-control" name="desconto_cartao" id="desconto_cartao" aria-describedby="desconto_cartao_help" placeholder="Insira o valor">
                    <small id="desconto_cartao_help" class="form-text text-muted">Porcentagem de desconto cartão</small>
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button id="gravar" type="submit" class="btn btn-primary">Salvar</button>
                  <img id="gravando" class="hidden" src="<?= $this->config->item("base_url") ?>/assets/images/spin.gif" />
                </div>
              </form>
            </div>
          </div>
        </div>
        <!--------------------------
        | Your Page Content Here |
        -------------------------->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="pull-right hidden-xs">
        Feito pensando no lojista
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; <?= date("Y") ?> <a target="_blank" href="<?= $this->config->item("base_url") ?>">Meu Checkout Prefeirdo</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/js/adminlte.min.js"></script>

  <script src="<?= $this->config->item('base_url'); ?>assets/js/jquery.mask.min.js"></script>

  <script>
    $(document).ready(function() {

      $('#desconto_cartao').mask('#0%', {
        reverse: true
      });

      var escassez = $("#escassez");
      escassez.val(secondsToHms((escassez.val())));
      escassez.mask('00:00:00');

      $("#loja_settings").on("submit", function(e) {

        if ($("#status_checkout").val() == 1 && ($("#mp_public_key").val().length < 41 || $("#mp_access_token").val().length < 70)) {
          alert("Crendenciais do mercado pago não preenchidas ou incorretas");
          e.preventDefault();
          return false;
        }

        if ($("#status_checkout").val() == 1 && ($("#mp_public_key").val().search("TEST-") > -1 || $("#mp_access_token").val().search("TEST-") > -1)) {
          alert("Crendenciais do mercado pago tem que ser de produção");
          e.preventDefault();
          return false;
        }

        if (escassez.val().length < 8) {
          alert("Cronômetro de escassez incompleto");
          e.preventDefault();
          return false;
        }

        var desconto = parseInt($('#desconto_cartao').cleanVal());
        if (desconto >= 50) {
          alert("descontos acima de 50% não são possíveis");
        }
        $('#desconto_cartao').val(desconto);
        e.preventDefault();
        form = this;
        var url_get = window.location.href.split("?");


        $("#gravando").removeClass("hidden");
        $("#gravar").addClass("hidden");

        $.ajax({
          type: "POST",
          processData: false,
          contentType: false,
          cache: false,
          url: "<?= $this->config->item("base_url") ?>dashboard/update_loja_config?" + url_get[1],
          data: new FormData(form),
          success: function() {
            $('html, body').animate({
              scrollTop: $("#success_alert").offset().top
            }, 500);
            $("#success_alert").removeClass("hidden");
            setTimeout(function() {
              $("#success_alert").addClass("hidden");
            }, 3000);
          }
        }).done(function() {
          $("#gravando").addClass("hidden");
          $("#gravar").removeClass("hidden");
        });
      })
    })

    function secondsToHms(d) {
      d = Number(d);
      var h = Math.floor(d / 3600);
      var m = Math.floor(d % 3600 / 60);
      var s = Math.floor(d % 3600 % 60);

      var hDisplay = zeroFill(h, 2);
      var mDisplay = zeroFill(m, 2);
      var sDisplay = zeroFill(s, 2);
      return hDisplay + mDisplay + sDisplay;
    }

    function zeroFill(number, width) {
      width -= number.toString().length;
      if (width > 0) {
        return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
      }
      return number + ""; // always return a string
    }
  </script>

  <!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>

</html>