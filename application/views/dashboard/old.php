<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147970193-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-147970193-1');
    </script>

    <meta charset="UTF-8">
    <title>Checkout Fast</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        img {
            width: 100%;
            /* object-fit: cover; */
            height: auto;
        }

        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 text-center mt-5">
                <h2>Suporte Whatsapp: <a href="https://wa.me/5531992639127" target="_blank">31 9 9263-9127</a></h2>
            </div>
            <div class="col-12 text-center mt-5">
                <h2>Painel beta:</h2>
            </div>
            <div class="col-12 text-center mt-5">
                <div class="alert alert-success d-none" id="success_alert" role="alert">
                    Registro gravado
                </div>
                <form id="loja_settings">
                    <div class="form-group">
                        <label for="status_checkout">Checkout:</label>
                        <select id="status_checkout" name="status_checkout" class="custom-select">
                            <option value="1" <?= $script_tag ? "selected" : "" ?>>Ligado</option>
                            <option value="0" <?= !$script_tag ? "selected" : "" ?>>Desligado</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="mp_public_key">Mercado Pago Public Key:</label>
                        <input value="<?= $mp_public_key ?>" type="text" class="form-control" name="mp_public_key" id="mp_public_key" aria-describedby="mp_public_key_help" placeholder="Ex:  APP_USR-xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx">
                        <small id="mp_public_key_help" class="form-text text-muted">Pega aqui: <a href="https://www.mercadopago.com/mlb/account/credentials/" target="_blank">Mercado Pago Credentials</a></small>
                    </div>
                    <div class="form-group">
                        <label for="mp_access_token">Mercado Pago Access token:</label>
                        <input value="<?= $mp_access_token ?>" type="text" class="form-control" name="mp_access_token" id="mp_access_token" aria-describedby="mp_access_token_help" placeholder="Ex: APP_USR-xxxxxxxxxxxxxxxx-xxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-xxxxxxxx">
                        <small id="mp_access_token_help" class="form-text text-muted">Pega aqui: <a href="https://www.mercadopago.com/mlb/account/credentials/" target="_blank">Mercado Pago Credentials</a></small>
                    </div>
                    <div class="form-group">
                        <label for="fb_pixel">Facebook Pixel:</label>
                        <input value="<?= $fb_pixel ?>" type="text" class="form-control" name="fb_pixel" id="fb_pixel" aria-describedby="fb_pixel_help" placeholder="Ex: 175698547882356">
                    </div>
                    <div class="form-group">
                        <label>Baner Lateral:</label>
                        <div class="custom-file">
                            <input type="file" name="banner_lateral" class="custom-file-input" id="banner_lateral" accept="image/*">
                            <label class="custom-file-label" for="banner_lateral"><?= $banner_lateral ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Baner Superior:</label>
                        <div class="custom-file">
                            <input type="file" name="banner_superior" class="custom-file-input" id="banner_superior" accept="image/*">
                            <label class="custom-file-label" for="banner_superior"><?= $banner_superior ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Página de Obrigado:</label>
                        <div class="custom-file">
                            <input type="file" name="pagina_obrigado" class="custom-file-input" id="pagina_obrigado" accept=".htm*">
                            <label class="custom-file-label" for="pagina_obrigado"><?= $pagina_obrigado ?></label>
                            <small id="pagina_obrigado_help" class="form-text text-muted">Pagina que será exibida ao completar a compra</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desconto_cartao">Desconto para cartão:</label>
                        <input value="<?= $desconto_cartao ?>" type="text" class="form-control" name="desconto_cartao" id="desconto_cartao" aria-describedby="desconto_cartao_help" placeholder="Insira o valor">
                        <small id="desconto_cartao_help" class="form-text text-muted">Porcentagem de desconto cartão</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="<?= $this->config->item('base_url'); ?>assets/js/jquery.mask.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
<script>
    $(document).ready(function() {
        bsCustomFileInput.init()

        $('#desconto_cartao').mask('#0%', {
            reverse: true
        });



        $("#loja_settings").on("submit", function(e) {

            if ($("#status_checkout").val() == 1 && ($("#mp_public_key").val().length < 41 || $("#mp_access_token").val().length < 70)) {
                alert("Crendenciais do mercado pago não preenchidas ou incorretas");
                e.preventDefault();
                return false;
            }

            var desconto = parseInt($('#desconto_cartao').cleanVal());
            if (desconto >= 50) {
                alert("descontos acima de 50% não são possíveis");
            }
            $('#desconto_cartao').val(desconto);
            e.preventDefault();
            form = this;
            var url_get = window.location.href.split("?");
            $.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                url: "<?= $this->config->item("base_url") ?>dashboard/update_loja_config?" + url_get[1],
                data: new FormData(form),
                success: function() {
                    $('html, body').animate({
                        scrollTop: $("#success_alert").offset().top
                    }, 500);
                    $("#success_alert").removeClass("d-none");
                    setTimeout(function() {
                        $("#success_alert").addClass("d-none");
                    }, 3000);
                }
            });
            console.log(form);
        })
    })
</script>

</html>