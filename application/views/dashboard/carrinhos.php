<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | MCP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="<?= $this->config->item("base_url") ?>assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <style>
    #gravando {
      height: 32px;
    }
  </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?= $this->load->view("dashboard/main_header", "", true) ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?= $this->load->view("dashboard/main_sidebar", array("loja", $loja), true) ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Carrinhos Abandonados
          <small>Carrinhos abandonados no checkout</small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <div id="success_alert" class="alert alert-success hidden" role="alert">
              Registro gravado
            </div>
            <div class="box box-success">
              <div class="box-header">
                <h4>Painel Geral</h4>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <table style="width:100%" class="table table-bordered">
                      <tr>
                        <th>Estágios</th>
                        <th>Aguardando</th>
                        <th>Enviados</th>
                        <th>Visitaram</th>
                        <th>Convertidos</th>
                      </tr>
                      <tr>
                        <td>Estágio 1</td>
                        <td><?=$estagio1['aguardando']?></td>
                        <td><?=$estagio1['enviado']?></td>
                        <td><?=$estagio1['voltaram']?></td>
                        <td><?=$estagio1['convertido']?></td> 
                      </tr>
                      <tr>
                        <td>Estágio 2</td>
                        <td><?=$estagio2['aguardando']?></td>
                        <td><?=$estagio2['enviado']?></td>
                        <td><?=$estagio2['voltaram']?></td>
                        <td><?=$estagio2['convertido']?></td> 
                      </tr>
                      <tr>
                        <td>Estágio 3</td>
                        <td><?=$estagio3['aguardando']?></td>
                        <td><?=$estagio3['enviado']?></td>
                        <td><?=$estagio3['voltaram']?></td>
                        <td><?=$estagio3['convertido']?></td> 
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <p>Somente será gravado o carrinho, caso exista pelo menos 1 estágio ativo</p>
                    <p>O Checkout é dado como abandonado somente se o cliente preencher o obrigatoriamente ao menos o nome e email</p>
                    <p>Será cobrado 0.01 MCP por e-mail enviado</p>
                    <p>Configure abaixo os três estágios da recuperação de carrinho</p>
                    <p>Variáveis
                      <ul>
                        <li>{{PRODUTOS}} -> Será subistituida por uma lista com os nomes dos produtos, ex:
                          <ul>
                            <li>Nome Produto 1</li>
                            <li>Nome Produto 2</li>
                          </ul>
                        </li>
                        <li>{{VALOR_ATUAL_TOTAL}} -> Será subistituido pelo valor total do carrinho</li>
                        <li>{{VALOR_CUPOM}} -> Valor do cupom, exemplo: 10% off nos produtos, R$25,00 no pedido</li>
                        <li>{{CUPOM}} -> Será subistituido pelo cupom, caso o campo seja preenchido</li>
                        <li>{{PRIMEIRO_NOME}} -> Será subistituida pelo primeiro nome do cliente</li>
                        <li>{{ULTIMO_NOME}} -> Será subistituida pelo ultimo nome do cliente</li>
                        <li>{{LINK_CHECKOUT}} -> Será subistituida pelo link com o checkout abandonado</li>
                      </ul>
                    </p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <div class="row">
                  <div class="col-xs-12">
                    <button type="button" id="savlar_config" class="btn btn-success">SALVAR TODOS ESTÁGIOS E CONFIGURAÇÕES</button>
                    <img id="gravando" class="hidden" src="<?= $this->config->item("base_url") ?>/assets/images/spin.gif" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <form id="estagios_carrinho">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h4>Primeiro estágio</h4>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="tempo_estagio_1">Tempo de espera</label>
                        <input type="tel" class="form-control tempo_estagio" name="tempo_estagio_1" id="tempo_estagio_1" value="<?= $rec_carrinho['estagio_1_timer'] ?>" aria-describedby="tempo_estagio_1_help" placeholder="Ex: HH:MM:SS">
                        <p class="help-block">Tempo a esperar depois que o carrinho foi abandonado para enviar o e-mail</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="cumpon_estagio_1">Cupom</label>
                        <input type="text" class="form-control cupom " name="cumpon_estagio_1" id="cumpon_estagio_1" value="<?= $rec_carrinho['estagio_1_cupom'] ?>" aria-describedby="cumpon_estagio_1_help" placeholder="Cupom existente">
                        <p class="help-block">Este cupom será responsável em aplicar o desconto no valor total da ordem</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="estagio_1_assunto">Assunto E-mail</label>
                        <input type="text" class="form-control" name="estagio_1_assunto" id="estagio_1_assunto" value="<?= $rec_carrinho['estagio_1_assunto'] ?>" aria-describedby="estagio_1_assunto_help" placeholder="Assunto e-mail">
                        <p class="help-block">Este cupom será responsável em aplicar o desconto no valor total da ordem</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="corpo_email_1">Corpo do e-mail</label>
                        <textarea class="textarea" id="corpo_email_1" name="email_estagio_1" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                        <?= $rec_carrinho['estagio_1_email'] ?>
                        </textarea>
                      </div>
                    </div>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-check">
                        <input type="checkbox" name="ativo_estagio_1" class="form-check-input" id="ativo_estagio_1" value="1" <?= $rec_carrinho['estagio_1_ativo'] == 1 ? "checked" : "" ?>>
                        <label class="form-check-label" for="exampleCheck1">Ativo</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h4>Segundo estágio</h4>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="tempo_estagio_2">Tempo de espera</label>
                        <input type="tel" value="<?= $rec_carrinho['estagio_2_timer'] ?>" class="form-control tempo_estagio" name="tempo_estagio_2" id="tempo_estagio_2" aria-describedby="tempo_estagio_2_help" placeholder="Ex: HH:MM:SS">
                        <p class="help-block">Tempo a esperar depois que o carrinho foi abandonado para enviar o e-mail</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="cumpon_estagio_2">Cupom</label>
                        <input type="text" value="<?= $rec_carrinho['estagio_2_cupom'] ?>" class="form-control cupom" name="cumpon_estagio_2" id="cumpon_estagio_2" aria-describedby="cumpon_estagio_2_help" placeholder="Cupom existente">
                        <p class="help-block">Este cupom será responsável em aplicar o desconto no valor total da ordem</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="estagio_2_assunto">Assunto E-mail</label>
                        <input type="text" class="form-control" name="estagio_2_assunto" id="estagio_2_assunto" value="<?= $rec_carrinho['estagio_2_assunto'] ?>" aria-describedby="estagio_2_assunto_help" placeholder="Assunto e-mail">
                        <p class="help-block">Este cupom será responsável em aplicar o desconto no valor total da ordem</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="corpo_email_2">Corpo do e-mail</label>
                        <textarea class="textarea" id="corpo_email_2" name="email_estagio_2" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                        <?= $rec_carrinho['estagio_2_email'] ?>
                      </textarea>
                      </div>
                    </div>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="ativo_estagio_2" id="ativo_estagio_2" value="1" <?= $rec_carrinho['estagio_2_ativo'] == 1 ? "checked" : "" ?>>
                        <label class="form-check-label" for="exampleCheck1">Ativo</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-danger">
                <div class="box-header">
                  <h4>Terceiro estágio</h4>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="tempo_estagio_3">Tempo de espera</label>
                        <input type="tel" value="<?= $rec_carrinho['estagio_3_timer'] ?>" class="form-control tempo_estagio" name="tempo_estagio_3" id="tempo_estagio_3" aria-describedby="tempo_estagio_3_help" placeholder="Ex: HH:MM:SS">
                        <p class="help-block">Tempo a esperar depois que o carrinho foi abandonado para enviar o e-mail</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="cumpon_estagio_3">Cupom</label>
                        <input type="text" value="<?= $rec_carrinho['estagio_3_cupom'] ?>" class="form-control cupom" name="cumpon_estagio_3" id="cumpon_estagio_3" aria-describedby="cumpon_estagio_3_help" placeholder="Cupom existente">
                        <p class="help-block">Este cupom será responsável em aplicar o desconto no valor total da ordem</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="estagio_3_assunto">Assunto E-mail</label>
                        <input type="text" class="form-control" name="estagio_3_assunto" id="estagio_3_assunto" value="<?= $rec_carrinho['estagio_3_assunto'] ?>" aria-describedby="estagio_3_assunto_help" placeholder="Assunto e-mail">
                        <p class="help-block">Este cupom será responsável em aplicar o desconto no valor total da ordem</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label for="corpo_email_2">Corpo do e-mail</label>
                        <textarea class="textarea" id="corpo_email_3" name="email_estagio_3" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                        <?= $rec_carrinho['estagio_3_email'] ?>
                      </textarea>
                      </div>
                    </div>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="ativo_estagio_3" id="ativo_estagio_3" value="1" <?= $rec_carrinho['estagio_3_ativo'] == 1 ? "checked" : "" ?>>
                        <label class="form-check-label" for="exampleCheck1">Ativo</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>

        <!--------------------------
        | Your Page Content Here |
        -------------------------->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="pull-right hidden-xs">
        Feito pensando no lojista
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; <?= date("Y") ?> <a target="_blank" href="<?= $this->config->item("base_url") ?>">Meu Checkout Prefeirdo</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/js/adminlte.min.js"></script>

  <script src="<?= $this->config->item('base_url'); ?>assets/js/jquery.mask.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/pt.js"></script>
  <script type="text/javascript" src="https://unpkg.com/tabulator-tables@4.4.3/dist/js/tabulator.min.js"></script>
  <script src="<?= $this->config->item("base_url") ?>assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

  <!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
<script>
  $(document).ready(function() {
    var url_get = window.location.href.split("?");
    var timeout = null;
    var tempo_estagio = $(".tempo_estagio");
    tempo_estagio.val(secondsToHms((tempo_estagio.val())));
    tempo_estagio.mask('00:00:00');


    $('.textarea').wysihtml5();

    $("#savlar_config").on("click", function(e) {
      $("#estagios_carrinho").trigger("submit");
    })

    $("#estagios_carrinho").on("submit", function(e) {
      $("#gravando").removeClass("hidden");
      $("#savlar_config").addClass("hidden");
      e.preventDefault();
      form = this;
      $.ajax({
        type: "POST",
        processData: false,
        contentType: false,
        cache: false,
        url: "<?= $this->config->item("base_url") ?>dashboard/salvar_conf_carrinho?" + url_get[1],
        data: new FormData(form),
        success: function(e) {
          $('html, body').animate({
            scrollTop: $("#success_alert").offset().top
          }, 500);
          $("#success_alert").removeClass("hidden");
          setTimeout(function() {
            $("#success_alert").addClass("hidden");
          }, 3000);
        }
      }).done(function() {
        $("#gravando").addClass("hidden");
        $("#savlar_config").removeClass("hidden");
      });
    });

    $(".cupom").on("focusout", function() {
      var cupom = $(this);
      if (cupom.length > 0) {
        $.ajax({
          type: "POST",
          url: "<?= $this->config->item("base_url") ?>checkout/check_discount/?" + url_get[1],
          cache: false,
          dataType: 'json',
          data: {
            cod_desconto: cupom.val()
          },
          success: function(e) {
            if (e == false) {
              cupom.parent("div").addClass("has-error");
              cupom.parent("div").removeClass("has-success");
            } else {
              cupom.parent("div").removeClass("has-error");
              cupom.parent("div").addClass("has-success");
            }
          }
        });
      }

    })

  })

  function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = zeroFill(h, 2);
    var mDisplay = zeroFill(m, 2);
    var sDisplay = zeroFill(s, 2);
    return hDisplay + mDisplay + sDisplay;
  }

  function zeroFill(number, width) {
    width -= number.toString().length;
    if (width > 0) {
      return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
    }
    return number + ""; // always return a string
  }
</script>

</html>