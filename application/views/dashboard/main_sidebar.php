<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= $this->config->item("base_url") ?>assets/admin_lte/dist/img/user.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?= $loja['nome_loja'] ?></p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i>MCPs: <?= $loja['mcps'] / 100 ?></a>
        <a href="#"><i class="fa fa-circle text-success"></i>Bonus: <?= $loja['limite'] / 100 ?></a>
      </div>
    </div>

    <!-- search form (Optional) -->
    <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form> -->
    <!-- /.search form -->

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">Menu</li>
      <!-- Optionally, you can add icons to the links -->
      <li><a href="/dashboard/?<?= $_SERVER['QUERY_STRING'] ?>"><i class="fa  fa-bar-chart"></i> <span>Dashboard</span></a></li>
      <li><a href="/dashboard/afiliados/?<?= $_SERVER['QUERY_STRING'] ?>"><i class="fa  fa-users"></i> <span>Afiliados</span></a></li>
      <li><a href="/dashboard/carrinho/?<?= $_SERVER['QUERY_STRING'] ?>"><i class="fa  fa-shopping-cart"></i> <span>Carrinho Abandonados</span></a></li>
      <li><a href="/dashboard/descontos/?<?= $_SERVER['QUERY_STRING'] ?>"><i class="fa  fa-dollar"></i> <span>Cupons</span></a></li>
      <!-- <li><a href="/dashboard/loja/?<?= $_SERVER['QUERY_STRING'] ?>"><i class="fa fa-shopping-cart"></i> <span>Loja</span></a></li> -->
      <li><a href="/dashboard/configuracoes/?<?= $_SERVER['QUERY_STRING'] ?>"><i class="fa fa-cogs"></i> <span>Configurações</span></a></li>
      <li><a href="/dashboard/suporte/?<?= $_SERVER['QUERY_STRING'] ?>"><i class="fa fa-support"></i> <span>Suporte</span></a></li>
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>